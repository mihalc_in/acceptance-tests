package com.caparzo.acceptance.tests.apt.jbehave.unittest;

/**
 * Package private utility type class to store/collect some context used in velocity template processing.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class TestMethodMetaInfoEntry {

    String testMethodName;

    String storyFilePath;

    public String getTestMethodName() {
        return testMethodName;
    }

    public void setTestMethodName(String testMethodName) {
        this.testMethodName = testMethodName;
    }

    public String getStoryFilePath() {
        return storyFilePath;
    }

    public void setStoryFilePath(String storyFilePath) {
        this.storyFilePath = storyFilePath;
    }
}
