package com.caparzo.acceptance.tests.apt.jbehave.stepdoc;

import java.util.List;

/**
 * Model to hold info about tabular parameter.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public class TabularFieldModel {

    protected String name;

    protected String type;

    /**
     * Useful for enums especially.
     */
    protected List<String> supportedValues;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getSupportedValues() {
        return supportedValues;
    }

    public void setSupportedValues(List<String> supportedValues) {
        this.supportedValues = supportedValues;
    }

    @Override
    public String toString() {
        return "TabularFieldModel{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", supportedValues=" + supportedValues +
                '}';
    }
}
