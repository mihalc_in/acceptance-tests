package com.caparzo.acceptance.tests.apt.jbehave.stepdoc;

/**
 * Annotation used to mark JBehave step methods which should be ignored by the StepDoc apt based generator.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public @interface StepDocIgnore {
}
