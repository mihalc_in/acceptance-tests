package com.caparzo.acceptance.tests.apt.javafieldinfo;

/**
 * Package private utility type class to store/collect some context used in velocity template processing.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class JavaFieldInfoEntry {

    String fieldName;

    String fieldType;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @Override
    public String toString() {
        return "MetaEntry{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldType='" + fieldType + '\'' +
                '}';
    }
}
