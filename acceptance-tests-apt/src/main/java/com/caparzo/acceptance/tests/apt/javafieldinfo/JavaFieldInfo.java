package com.caparzo.acceptance.tests.apt.javafieldinfo;

/**
 * Utility type interface to provide meta level type information about java object fields.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public interface JavaFieldInfo<T> {

    Class<T> getType();

    String getName();

    T getFrom(Object instance);

    void setOn(Object instance, T value);
}
