package com.caparzo.acceptance.tests.apt.jbehave.stepdoc;

import java.util.List;

/**
 * Model for step method parameter inforamtion.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public class StepParameterModel {

    protected String name;

    protected String type;

    protected boolean tabular;

    protected List<TabularFieldModel> tabularParameterInfo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isTabular() {
        return tabular;
    }

    public void setTabular(boolean tabular) {
        this.tabular = tabular;
    }

    public List<TabularFieldModel> getTabularParameterInfo() {
        return tabularParameterInfo;
    }

    public void setTabularParameterInfo(List<TabularFieldModel> tabularParameterInfo) {
        this.tabularParameterInfo = tabularParameterInfo;
    }

    @Override
    public String toString() {
        return "StepParameterModel{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", tabular=" + tabular +
                ", tabularParameterInfo=" + tabularParameterInfo +
                '}';
    }
}
