package com.caparzo.acceptance.tests.apt.jbehave.stepdoc;

import java.util.List;

/**
 * Contains step documentation model gathered by the annotation processor and which will be passed to the velocity
 * rendering template engine.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public class StepDocModel {

    protected String stepDefintion;

    protected String javaDoc;

    protected List<StepParameterModel> parameterInfos;

    protected String className;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStepDefintion() {
        return stepDefintion;
    }

    public void setStepDefintion(String stepDefintion) {
        this.stepDefintion = stepDefintion;
    }

    public String getJavaDoc() {
        return javaDoc;
    }

    public void setJavaDoc(String javaDoc) {
        this.javaDoc = javaDoc;
    }

    public List<StepParameterModel> getParameterInfos() {
        return parameterInfos;
    }

    public void setParameterInfos(List<StepParameterModel> parameterInfos) {
        this.parameterInfos = parameterInfos;
    }

    @Override
    public String toString() {
        return "StepDocModel{" +
                "stepDefintion='" + stepDefintion + '\'' +
                ", javaDoc='" + javaDoc + '\'' +
                ", parameterInfos=" + parameterInfos +
                '}';
    }
}
