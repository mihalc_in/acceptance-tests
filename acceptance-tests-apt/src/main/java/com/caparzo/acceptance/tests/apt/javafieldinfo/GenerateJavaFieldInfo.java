package com.caparzo.acceptance.tests.apt.javafieldinfo;

import java.lang.annotation.*;

/**
 * Intended to be used on classes for which java meta level information is to be generated.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface GenerateJavaFieldInfo {

}
