package com.caparzo.acceptance.tests.apt.javafieldinfo;

import org.apache.commons.lang.Validate;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * User: stasyukd
 * Date: 26/09/13
 * Time: 17:19
 */
@SupportedAnnotationTypes("com.caparzo.acceptance.tests.apt.javafieldinfo.GenerateJavaFieldInfo")
public class JavaFieldInfoGenerator extends AbstractProcessor {


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (roundEnv.processingOver()) {
            return false;
        }

        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(GenerateJavaFieldInfo.class);

        for (Element e : annotatedElements) {

            if (e.getKind() == ElementKind.CLASS) {

                VelocityContext vc = new VelocityContext();

                // extract context for velocity template
                TypeElement classElement = (TypeElement) e;
                PackageElement packageElement = (PackageElement) classElement.getEnclosingElement();

                log("processing annotated class: " + classElement.getQualifiedName(), e);

                String fqClassName = classElement.getQualifiedName().toString();
                String className = classElement.getSimpleName().toString();
                String packageName = packageElement.getQualifiedName().toString();
                vc.put("className", className);
                vc.put("packageName", packageName);
                vc.put("generatorName", this.getClass().getName());

                List<JavaFieldInfoEntry> metaEntries = gatherMetaInfos(classElement);

                // extract info from super class if not directly extending from Object - NOTE works only on one super class and does not
                // follow full class hierarchy
                DeclaredType superclass = (DeclaredType) classElement.getSuperclass();
                String superType = superclass.toString();
                if (!superType.equals(Object.class.getName())) {
                    Element superElement = superclass.asElement();
                    List<JavaFieldInfoEntry> superFieldInfos = gatherMetaInfos(superElement);
                    metaEntries.addAll(superFieldInfos);
                }

                Validate.notEmpty(metaEntries, "Classes annotated with " + GenerateJavaFieldInfo.class.getSimpleName() + " must " +
                        "also have at least one field defined");
                vc.put("metaEntries", metaEntries);

                // process velocity template
                Properties props = new Properties();
                URL url = this.getClass().getClassLoader().getResource("velocity.properties");
                try {
                    props.load(url.openStream());
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                VelocityEngine ve = new VelocityEngine(props);
                ve.init();

                Template vt = ve.getTemplate("templates/java-field-info.vm");

                JavaFileObject jfo = null;
                try {
                    String suffix = "_JavaFieldInfo";
                    jfo = processingEnv.getFiler().createSourceFile(fqClassName + suffix);
                    vc.put("generatedClassName", className + suffix);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }

                log("creating source file: " + jfo.toUri(), e);

                Writer writer = null;
                try {
                    writer = jfo.openWriter();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }

                log("applying velocity template: " + vt.getName(), e);
                vt.merge(vc, writer);

                try {
                    writer.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }

            } else {
                throw new RuntimeException(GenerateJavaFieldInfo.class.getName() + " annotation is supported only on types.");
            }
        }

        return false;
    }

    private List<JavaFieldInfoEntry> gatherMetaInfos(Element element) {

        List<JavaFieldInfoEntry> metaEntries = new ArrayList<JavaFieldInfoEntry>();

        List<? extends Element> innerElements = element.getEnclosedElements();

        for (Element innerElement : innerElements) {

            ElementKind kind = innerElement.getKind();

            if (kind == ElementKind.FIELD) {

                Set<Modifier> modifiers = innerElement.getModifiers();
                if (modifiers.contains(Modifier.STATIC)) {
                    // skip static fields
                } else {
                    Name fieldName = innerElement.getSimpleName();
                    JavaFieldInfoEntry metaEntry = processField(innerElement, fieldName.toString());
                    metaEntries.add(metaEntry);
                }
            }

        }

        return metaEntries;
    }

    private void log(String msg, Element e) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg, e);
    }

    private JavaFieldInfoEntry processField(Element innerElement, String javaFieldName) {

        JavaFieldInfoEntry metaEntry = new JavaFieldInfoEntry();
        metaEntry.fieldName = javaFieldName;

        // unless overriden for typed elements below
        TypeMirror elementType = innerElement.asType();
        String typeAsString = elementType.toString();
        if (typeAsString.equals("boolean")) {
            typeAsString = "Boolean";
        }
        metaEntry.fieldType = typeAsString;

        if (elementType instanceof DeclaredType) {
            DeclaredType dt = (DeclaredType) elementType;
            if (!dt.getTypeArguments().isEmpty()) {
                Element dtElement = dt.asElement();
                if (dtElement instanceof TypeElement) {
                    TypeElement te = (TypeElement) dtElement;
                    DeclaredType withoutType = processingEnv.getTypeUtils().getDeclaredType(te);
                    metaEntry.fieldType = withoutType.toString();
                }
            }
        }

        return metaEntry;
    }


}
