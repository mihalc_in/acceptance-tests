package com.caparzo.acceptance.tests.apt.jbehave.unittest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.maven.shared.utils.io.DirectoryScanner;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.test.annotation.IfProfileValue;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 * Used to generate JUnit tests for each story file.
 * User: stasyukd
 * Date: 26/09/13
 * Time: 17:19
 */
@SupportedAnnotationTypes("com.caparzo.acceptance.tests.apt.jbehave.unittest.GenerateStoryTestMethods")
public class GenerateStoryTestMethodsProcessor extends AbstractProcessor {


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (roundEnv.processingOver()) {
            return false;
        }

        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(GenerateStoryTestMethods.class);

        for (Element element : annotatedElements) {
            processAnnotatedElement(element);
        }

        return false;
    }

    private void processAnnotatedElement(Element element) {

        ElementKind kind = element.getKind();
        Validate.isTrue(kind.equals(ElementKind.CLASS), "Annotation - " + GenerateStoryTestMethods.class.getSimpleName() + " is only supported on types");
        TypeElement classElement = (TypeElement) element;

        String fqClassName = classElement.getQualifiedName().toString();
        log("processing annotated element - " + fqClassName, element);
        String className = classElement.getSimpleName().toString();
        PackageElement packageElement = (PackageElement) classElement.getEnclosingElement();
        String packageName = packageElement.getQualifiedName().toString();

        VelocityContext vc = new VelocityContext();
        vc.put("className", className);
        vc.put("packageName", packageName);
        vc.put("generatorName", this.getClass().getName());
        String suffix = "Test";
        vc.put("generatedClassName", className + suffix);

        List<TestMethodMetaInfoEntry> metaEntries = gatherMetaInfos(element);

        if (metaEntries.isEmpty()) {
            log("Failed to find any story files matching file mask specified in annotated element - " + fqClassName
                    + ". No unit test class will be generated for this element.", element);
            return;
        }

        vc.put("metaEntries", metaEntries);
        processVelocityTemplate(vc, element, fqClassName, suffix);

        List<String> profiles = gatherProfiles(element);

        for (String profile : profiles) {
            suffix = "Test";
            String subfolderName = "";

            String[] activeProfiles = null;

            if (profile.contains("-")) {
                String[] strings = profile.split("-");

                for (String string : strings) {
                    suffix += StringUtils.capitalize(string);
                }
                subfolderName = profile.replaceAll("-", "_");
                activeProfiles = new String[]{profile};

            } else if (profile.contains(",")) {
                // e.g. 'embedded,ci' case
                activeProfiles = profile.split(",");
                for (int i = 0; i < activeProfiles.length; i++) {
                    String value = activeProfiles[i];
                    suffix += StringUtils.capitalize(value);
                    subfolderName += value;
                    if (i < activeProfiles.length - 1) {
                        subfolderName += "_";
                    }
                }
            } else {
                suffix += StringUtils.capitalize(profile);
                subfolderName = profile;
                activeProfiles = new String[]{profile};
            }

            vc.put("activeProfiles", activeProfiles);
            vc.put("generatedClassName", className + suffix);
            vc.put("packageName", packageName);
            vc.put("subfolderName", subfolderName);
            fqClassName = packageName + "." + subfolderName + "." + className;
            processVelocityTemplate(vc, element, fqClassName, suffix);
        }
    }

    private void processVelocityTemplate(VelocityContext vc, Element element, String fqClassName, String suffix) {
        // process velocity template
        Properties props = new Properties();
        URL url = this.getClass().getClassLoader().getResource("velocity.properties");
        try {
            props.load(url.openStream());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        VelocityEngine ve = new VelocityEngine(props);
        ve.init();

        Template vt = ve.getTemplate("templates/jbehave-unittest.vm");

        JavaFileObject jfo = null;
        try {
            jfo = processingEnv.getFiler().createSourceFile(fqClassName + suffix);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        log("creating source file: " + jfo.toUri(), element);

        Writer writer = null;
        try {
            writer = jfo.openWriter();
            log("applying velocity template: " + vt.getName(), element);
            vt.merge(vc, writer);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private List<TestMethodMetaInfoEntry> gatherMetaInfos(Element element) {

        GenerateStoryTestMethods annotation = element.getAnnotation(GenerateStoryTestMethods.class);
        String searchDirectory = annotation.value();
        if (searchDirectory.endsWith("/")) {
            throw new RuntimeException("Directory path specified in " + GenerateStoryTestMethods.class.getSimpleName() + " should not end with '/'");
        }

        if (searchDirectory.startsWith("classpath*:")) {
            return gatherMetaInfosFromStoriesInJar(element, searchDirectory);
        } else {
            return gatherMetaInfosFromStoriesInDirectory(element, searchDirectory);
        }

    }

    private List<TestMethodMetaInfoEntry> gatherMetaInfosFromStoriesInJar(Element element, String searchDirectory) {

        String storyFileMask = searchDirectory + "/**/*.story";

        URLClassLoader classLoader = (URLClassLoader) getClass().getClassLoader();
        PathMatchingResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver(classLoader);
        log("searching for story paths on the classpath using mask - " + storyFileMask, element);
        Resource[] foundStoryResources;
        try {
            foundStoryResources = pathResolver.getResources(storyFileMask);
        } catch (IOException e) {
            throw new RuntimeException("An error occurred while trying to generate story test methods for stories inside the Jar file", e);
        }
        log("total of - " + foundStoryResources.length + " stories were found on the classpath:", element);

        String[] foundFiles = new String[foundStoryResources.length];
        for (int i = 0; i < foundStoryResources.length; i++) {
            Resource foundStoryResource = foundStoryResources[i];
//            foundStoryResource = foundStoryResource.replace('\\', '/');
            URL storyFileUrl = null;
            try {
                storyFileUrl = foundStoryResource.getURL();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            String fullStoryFilePath = storyFileUrl.getFile();
            int fileSeparator = fullStoryFilePath.lastIndexOf('!');
            String filePath;
            if (fileSeparator != -1) {
                filePath = fullStoryFilePath.substring(fileSeparator + 1);
            } else {
                filePath = fullStoryFilePath;
            }
            foundFiles[i] = filePath;
            log(fullStoryFilePath, element);
        }

        List<TestMethodMetaInfoEntry> metaInfoEntries = new ArrayList<>(foundFiles.length);

        for (String foundFile : foundFiles) {
            TestMethodMetaInfoEntry metaInfoEntry = new TestMethodMetaInfoEntry();
            metaInfoEntry.setStoryFilePath(foundFile);
            String fileNameWithExt = new File(foundFile).getName();
            String fileNameOnly = fileNameWithExt.substring(0, fileNameWithExt.lastIndexOf('.'));
            metaInfoEntry.setTestMethodName(fileNameOnly);
            metaInfoEntries.add(metaInfoEntry);
        }

        return metaInfoEntries;
    }

    private List<TestMethodMetaInfoEntry> gatherMetaInfosFromStoriesInDirectory(Element element, String searchDirectory) {

        String storyFileMask = "**/src/test/resources/" + searchDirectory + "/**/*.story";

        DirectoryScanner ds = new DirectoryScanner();

        ds.setCaseSensitive(false);
        ds.setFollowSymlinks(true);

        String baseDir = new File("").getAbsolutePath();
        if (baseDir.endsWith("parent")) {
            // work around for running fil/tes-parent build from the parent's directory and not from the java-apps directory one level up
            baseDir = new File(baseDir).getParent();
        }
        log("searching for story paths using mask - " + storyFileMask + " in base directory - " + baseDir, element);
        ds.setBasedir(baseDir);
        ds.setIncludes(storyFileMask);

        ds.scan();
        String[] foundFiles = ds.getIncludedFiles();

//        log("found story paths before stripping 'src/test/resources' are:", element);

        for (int i = 0; i < foundFiles.length; i++) {
            String foundFile = foundFiles[i];
            foundFile = foundFile.replace('\\', '/');
            foundFiles[i] = foundFile;
            log(foundFile, element);
        }

        // trim the 'src/test/resources' which was appended above to make sure the search is executed in the test
        // resources directory
        for (int i = 0; i < foundFiles.length; i++) {
            String foundFile = foundFiles[i];
            String resPrefix = "src/test/resources/";
            int subStringStartAt = foundFile.indexOf(resPrefix) + resPrefix.length();
            foundFile = foundFile.substring(subStringStartAt);
            foundFiles[i] = foundFile;
        }

        log("found a total of - " + foundFiles.length + " story files, using mask - " + storyFileMask, element);
        log("found story paths after stripping 'src/test/resources' are:", element);
        for (String storyPath : foundFiles) {
            log(storyPath, element);
        }


        List<TestMethodMetaInfoEntry> metaInfoEntries = new ArrayList<>(foundFiles.length);

        for (String foundFile : foundFiles) {
            TestMethodMetaInfoEntry metaInfoEntry = new TestMethodMetaInfoEntry();
            metaInfoEntry.setStoryFilePath(foundFile);
            String fileNameWithExt = new File(foundFile).getName();
            String fileNameOnly = fileNameWithExt.substring(0, fileNameWithExt.lastIndexOf('.'));
            metaInfoEntry.setTestMethodName(fileNameOnly);
            metaInfoEntries.add(metaInfoEntry);
        }

        return metaInfoEntries;
    }

    private List<String> gatherProfiles(Element element) {
        IfProfileValue annotation = element.getAnnotation(IfProfileValue.class);
        String[] profiles = annotation.values();
        return Arrays.asList(profiles);
    }

    private void log(String msg, Element e) {
        System.out.println("[" + this.getClass().getSimpleName() + "] " + msg);
        processingEnv.getMessager().printMessage(Diagnostic.Kind.OTHER, msg, e);
    }


}
