package com.caparzo.acceptance.tests.apt.jbehave.stepdoc;

import org.apache.commons.collections.FastTreeMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.util.ReflectionUtils;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.*;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used to generate documentation for story step methods.
 * User: stasyukd
 * Date: 26/09/13
 * Time: 17:19
 */
@SupportedAnnotationTypes("org.jbehave.core.annotations.*")
public class GenerateStepMethodsDocProcessor extends AbstractProcessor {


    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (roundEnv.processingOver()) {
            return false;
        }

        List<StepDocModel> allStepDocs = new ArrayList<>();

        Set<? extends Element> annotatedElements;

        allStepDocs.addAll(gatherForAnnotation(roundEnv, Given.class));
        allStepDocs.addAll(gatherForAnnotation(roundEnv, When.class));
        allStepDocs.addAll(gatherForAnnotation(roundEnv, Then.class));

        // sort by enclosing class
        Map<String, List<StepDocModel>> groupedByClass = groupByClass(allStepDocs);

        if (!groupedByClass.isEmpty()) {
            processTemplate(groupedByClass, "jbehave-stepdoc.vm", "stepdoc.html");
            processTemplate(groupedByClass, "jbehave-stepdoc-story-prototype.vm", "stepdoc_with_story_prototype.html");
        }

        return false;
    }

    private List<StepDocModel> gatherForAnnotation(RoundEnvironment roundEnv, Class annotationClass) {

        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotationClass);
        filterIgnored(annotatedElements);
        List<StepDocModel> stepDocs = processAnnotatedElements(annotatedElements, annotationClass);
        return stepDocs;
    }

    private Map<String, List<StepDocModel>> groupByClass(List<StepDocModel> stepDocs) {

        Map<String, String> options = this.processingEnv.getOptions();
        String stripPackagePrefix = options.get("stripPackagePrefix");

        SortedMap<String, List<StepDocModel>> unorderedMap = new FastTreeMap();

        for (StepDocModel stepDoc : stepDocs) {
            String className = stepDoc.className;
            if (stripPackagePrefix != null && className.startsWith(stripPackagePrefix)) {
                className = className.substring(stripPackagePrefix.length());
            }
            List<StepDocModel> forClass = unorderedMap.get(className);
            if (forClass == null) {
                forClass = new ArrayList<>();
                unorderedMap.put(className, forClass);
            }
            forClass.add(stepDoc);
        }

        return unorderedMap;
    }

    private void processTemplate(Map<String, List<StepDocModel>> allStepDocs, String templateName, String createdFileName) {

        VelocityContext vc = new VelocityContext();
        vc.put("stepDocsByClass", allStepDocs);

        // process velocity template
        Properties props = new Properties();
        URL url = this.getClass().getClassLoader().getResource("velocity.properties");
        try {
            props.load(url.openStream());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        VelocityEngine ve = new VelocityEngine(props);
        ve.init();

        Template vt = ve.getTemplate("templates/" + templateName);

        FileObject resource = null;
        try {
            JavaFileManager.Location location = StandardLocation.SOURCE_OUTPUT;
            String pkg = "stepdocs";
            resource = processingEnv.getFiler().createResource(location, pkg, createdFileName);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        log("creating source file: " + resource.toUri(), null);

        Writer writer = null;
        try {
            writer = resource.openWriter();
            log("applying velocity template: " + vt.getName(), null);
            vt.merge(vc, writer);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private void filterIgnored(Set<? extends Element> elements) {
        List<Element> toRemove = new ArrayList<>();
        for (Element element : elements) {
            StepDocIgnore ignoreAnn = element.getAnnotation(StepDocIgnore.class);
            if (ignoreAnn != null) {
                toRemove.add(element);
            }
        }
        elements.removeAll(toRemove);
    }

    private List<StepDocModel> processAnnotatedElements(Collection<? extends Element> elements, Class annotationType) {
        List<StepDocModel> allStepDocs = new ArrayList<>();
        for (Element element : elements) {
            StepDocModel stepDoc = processAnnotatedElement(element, annotationType);
            allStepDocs.add(stepDoc);
        }
        return allStepDocs;
    }

    private StepDocModel processAnnotatedElement(Element element, Class annotationClass) {

        StepDocModel stepDoc = new StepDocModel();

        ElementKind kind = element.getKind();
//        TypeElement classElement = (TypeElement) element;

        TypeElement enclosingElement = (TypeElement) element.getEnclosingElement();
        Name className = enclosingElement.getQualifiedName();
        stepDoc.className = className.toString();

        Annotation annotation = element.getAnnotation(annotationClass);
        Method valueMethod = ReflectionUtils.findMethod(annotationClass, "value");
        if (valueMethod != null) { // some of the step annotations don't have value, e.g. BeforeStory
            String value = null;
            try {
                value = (String) valueMethod.invoke(annotation);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
            log("------------------------------------", element);
            String stepDefinition = annotationClass.getSimpleName() + " " + value;
            stepDoc.stepDefintion = spanStepKeywords(stepDefinition);
            log(stepDefinition, element);
        }

        ExecutableElement em = (ExecutableElement) element;
        List<? extends VariableElement> parameters = em.getParameters();
        String javadoc = processingEnv.getElementUtils().getDocComment(element);
        log("JavaDoc:\n " + javadoc, element);
        stepDoc.javaDoc = javadoc;

        List<StepParameterModel> parameterInfos = new ArrayList<>(parameters.size());
        stepDoc.parameterInfos = parameterInfos;

        for (VariableElement parameter : parameters) {

            StepParameterModel pm = new StepParameterModel();
            parameterInfos.add(pm);

            Name simpleName = parameter.getSimpleName();
            log("parameter - " + simpleName + ":", element);
            pm.name = simpleName.toString();

            TypeMirror vtype = parameter.asType();
            log("vtype = " + vtype, element);

            if (vtype instanceof DeclaredType) {
                DeclaredType dt = (DeclaredType) vtype;
                List<? extends TypeMirror> typeArguments = dt.getTypeArguments();
                log("typeArguments: " + typeArguments, element);
                if (typeArguments.isEmpty()) {
                    // check if its a class from packages under com.caparzo.acceptance
                    Element parameterElemment = dt.asElement();
                    if (parameterElemment instanceof TypeElement) {
                        TypeElement te = (TypeElement) parameterElemment;
                        Name qualifiedName = te.getQualifiedName();
                        log("qualifiedName = " + qualifiedName, parameterElemment);

                        ElementKind paramElemKind = parameterElemment.getKind();
                        // special use case for enums
                        if (paramElemKind.equals(ElementKind.ENUM)) {
                            log("parameter is an Enum kind", parameterElemment);
                            List<? extends Element> enumConstants = parameterElemment.getEnclosedElements();
                            List<VariableElement> variableElements = ElementFilter.fieldsIn(enumConstants);
                            pm.type = "one of: " + variableElements;
                        } else if (qualifiedName.toString().startsWith("com.caparzo.acceptance")) {
                            List<TabularFieldModel> tabularInfos = getFieldsFromTypeElemment(te);
                            log("supportedField: \n" + tabularInfos, parameterElemment);
                            pm.tabular = true;
                            pm.tabularParameterInfo = tabularInfos;
                            pm.type = te.getSimpleName().toString();
                        } else {
                            pm.type = te.getSimpleName().toString();
                        }
                    }

                } else if (typeArguments.size() > 1) {
                    throw new RuntimeException("Only one type parameter is supported in parameters of JBhehave step methods");
                } else {
                    TypeMirror ta = typeArguments.get(0);
                    if (ta instanceof DeclaredType) {
                        DeclaredType dta = (DeclaredType) ta;
                        Element de = dta.asElement();
                        // we only process types, i.e. primitive parameters are ignored
                        if (de instanceof TypeElement) {
                            TypeElement typeElement = (TypeElement) de;
                            List<TabularFieldModel> supportedFields = getFieldsFromTypeElemment(typeElement);
                            log("supported fields: \n" + supportedFields, element);

                            TypeElement te = (TypeElement) de;
                            Name qualifiedName = te.getQualifiedName();
                            log("qualifiedName = " + qualifiedName, de);
                            if (qualifiedName.toString().startsWith("com.caparzo.acceptance")) {
                                List<TabularFieldModel> tabularInfos = getFieldsFromTypeElemment(te);
                                log("supportedField: \n" + tabularInfos, de);
                                pm.tabular = true;
                                pm.tabularParameterInfo = tabularInfos;
                                pm.type = te.getSimpleName().toString();
                            } else {
                                // handle lists of primitive wrapper types
                                // TODO - needs improvement to be less of a hack
                                if (vtype.toString().startsWith("java.util.List")) {
                                    String wrapperType = qualifiedName.toString();
                                    switch (wrapperType) {
                                        case "java.lang.String":
                                            pm.type = "list of Strings";
                                            break;
                                        case "java.lang.Boolean":
                                            pm.type = "list of Boolean";
                                            break;
                                        case "java.lang.Integer":
                                            pm.type = "list of Integer";
                                            break;
                                        case "java.lang.Long":
                                            pm.type = "list of Long";
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                pm.type = vtype.toString();
            }


        }

        return stepDoc;
    }

    /**
     * Might be difficult to do regexp required to find paramters in Javascript so doing it in Java here.
     *
     * @param stepDefinition
     * @return
     */
    private String spanStepKeywords(String stepDefinition) {

        // span first word with 'step-type' class
        Pattern pattern = Pattern.compile("([^\\s]*)(.*)");
        Matcher matcher = pattern.matcher(stepDefinition);
        Validate.isTrue(matcher.matches(), "Mandatory regular expression pattern for step definition spanning failed");
        String firstWord = matcher.group(1);
        String rest = matcher.group(2);
        stepDefinition = "<span class='step-type'>" + firstWord + "</span>" + rest;

        // span each argument in 'step-param' class
        stepDefinition = stepDefinition.replaceAll("(\\$[^\\s]*)", "<span class='step-param'>$1</span>");

        return stepDefinition;
    }

    private List<TabularFieldModel> getFieldsFromTypeElemment(TypeElement typeElement) {

        Elements elementUtils = processingEnv.getElementUtils();

        List<? extends Element> enclosedElements = typeElement.getEnclosedElements();
        List<VariableElement> fields = ElementFilter.fieldsIn(enclosedElements);

        List<TabularFieldModel> supportedFields = new ArrayList<>(fields.size());

        TypeMirror superclass = typeElement.getSuperclass();
        TypeElement objectType = elementUtils.getTypeElement(Object.class.getName());
        String superTypeString = superclass.toString();
        TypeElement superType = elementUtils.getTypeElement(superTypeString);

        if (superType == null || superType.equals(objectType)) {
            // nothing else to do as there is no inheritance with enums
            // or in case where super type is NoType i.e. Object
        } else {
            List<TabularFieldModel> fromSuperClass = this.getFieldsFromTypeElemment(superType);
            supportedFields.addAll(fromSuperClass);
        }

        for (VariableElement field : fields) {
            Name fieldName = field.getSimpleName();
            TabularFieldModel info = new TabularFieldModel();
            info.name = fieldName.toString();
            TypeMirror type = field.asType();
            String fullQualifiedName = type.toString();
            String simpleName;
            int lastDotIndex = fullQualifiedName.lastIndexOf(".");
            if (lastDotIndex == -1) {
                // case of primitive types
                simpleName = StringUtils.capitalize(fullQualifiedName);
            } else {
                simpleName = fullQualifiedName.substring(lastDotIndex + 1);
            }
            info.type = simpleName;
            supportedFields.add(info);
        }
        return supportedFields;
    }

    private void log(String msg, Element e) {
        System.out.println("[" + this.getClass().getSimpleName() + "] " + msg);
        if (e != null) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg, e);
        } else {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }


}
