package com.caparzo.acceptance.tests.apt.jbehave.unittest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to mark tests classes which should be extended by the annotation processing facility with classes that will
 * contain auto generated methods for each story file in the story directory or subdirectory specified in the value
 * attribute.
 * User: stasyukd
 * Date: 26/09/13
 * Time: 17:01
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GenerateStoryTestMethods {

    /**
     * Directory starting from which the annotation processor will start scanning for *.story files.
     *
     * @return
     */
    String value();
}
