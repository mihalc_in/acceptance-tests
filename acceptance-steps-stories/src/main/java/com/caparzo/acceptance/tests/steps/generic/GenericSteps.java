package com.caparzo.acceptance.tests.steps.generic;

import com.caparzo.acceptance.tests.core.jbehave.ScenarioExecutionContext;
import com.caparzo.acceptance.tests.core.jbehave.ScenarioParamsContext;
import com.caparzo.acceptance.tests.core.jbehave.annotations.Steps;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Contains reusable generic story steps.
 * User: mihalcip
 * Date: 16/05/14
 * Time: 11:23
 */
@Steps
public class GenericSteps {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeScenario
    public void resetScenarioContexts() {
        // create and put context
        ScenarioExecutionContext.CONTEXT.set(new ScenarioExecutionContext());
        ScenarioParamsContext.CONTEXT.set(new ScenarioParamsContext());
    }

    @Given("time to wait: $seconds s")
    public void wait(int seconds) {
        try {
            logger.info("Pausing for " + seconds + " seconds...");
            Thread.sleep(TimeUnit.SECONDS.toMillis(seconds));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Given("scenario parameter - $paramName = $paramValue")
    public void addScenarioParam(String paramName, String paramValue) {
        logger.debug("Saving scenario parameter '" + paramName + "=" + paramValue + "' into context...");
        ScenarioParamsContext.addScenarioParam(paramName, paramValue);
    }

    @AfterScenario
    public void resetScenarioParams() {
        // create and put context
        ScenarioParamsContext scenarioParamsContext = ScenarioParamsContext.CONTEXT.get();

        if (scenarioParamsContext != null) {
            Map<String, String> scenarioParams = scenarioParamsContext.getScenarioParams();
            scenarioParams.clear();
        }
    }


}
