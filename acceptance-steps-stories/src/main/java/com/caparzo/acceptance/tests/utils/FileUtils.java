package com.caparzo.acceptance.tests.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by mihalcip on 14/05/2014.
 */
public class FileUtils {

    public static File createFile(String fileName, String fileContent) {
        fileContent = fileContent.trim();

        File file = new File(fileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write(fileContent);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return file;
    }

}
