package com.caparzo.acceptance.tests.context.properties;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MethodInvokingFactoryBean which takes care of setting properties from property file to CONSTANTs located in TimeoutUtils class
 *
 * @author mihalcip, created on 01/07/2014 17:04
 */
@Configuration
public class TesAcceptanceTestsMethodInvokingFactoryBean {

    @Value("${acceptance.tests.overall.timeout:10000}")
    private Long accTestsOverallTimeoutMs;

    @Value("${acceptance.tests.timeout:1000}")
    private Long accTestsTimeoutMs;

    @Bean
    public MethodInvokingFactoryBean tesAccTestsMethodInvokingFactoryBean() {
        MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();

        methodInvokingFactoryBean.setStaticMethod("com.caparzo.acceptance.tests.utils.TimeoutUtils.setTimeoutConstants");
        Long[] timeouts = {accTestsOverallTimeoutMs, accTestsTimeoutMs};
        methodInvokingFactoryBean.setArguments(timeouts);

        try {
            methodInvokingFactoryBean.afterPropertiesSet();
        } catch (Exception e) {
            throw new AcceptanceTestException("Problem occured when invoking static setter 'setTimeoutConstants' on class TimeoutUtils", e);
        }

        return methodInvokingFactoryBean;
    }

}
