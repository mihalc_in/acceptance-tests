package com.caparzo.acceptance.tests.context.sftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.file.remote.FileInfo;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Component
public class SftpRemoteFilesLister {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ServiceActivator
    public List<FileInfo> process(Message<?> message) {
        logger.debug("Received SI message: {}", message);

        Object payload = message.getPayload();
        logger.trace("Message's payload: {}", payload);

        logger.trace("Message's headers:");
        MessageHeaders headers = message.getHeaders();
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            logger.trace("[{}={}]", entry.getKey(), entry.getValue());
        }

        List<FileInfo> remoteFiles = (List<FileInfo>) message.getPayload();
        logger.debug("{} remote file(s): {}", remoteFiles.size(), remoteFiles);

        return remoteFiles;
    }
}

