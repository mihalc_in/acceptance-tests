package com.caparzo.acceptance.tests.context.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Spring properties configuration for specified profile(s).
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@Configuration
public class TesAcceptanceTestsPropertiesForProfile {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Bean
    public PropertyPlaceholderConfigurer tesAccTestsPropertyPlaceholderConfigurerForProfile() {

        String[] activeProfiles;
        String activeProfilesProp = System.getProperty("spring.profiles.active");
        if (activeProfilesProp != null && !activeProfilesProp.isEmpty()) {
            activeProfiles = activeProfilesProp.split(",");
        } else {
            activeProfiles = new String[0];
        }

        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new CustomPropertyPlaceholderConfigurer();

        if (activeProfiles.length != 0) {

            Resource[] resources = new Resource[activeProfiles.length + 1];

            for (int i = 0; i < activeProfiles.length; i++) {
                String activeProfile = activeProfiles[i];
                String propFile = "acceptance-tests-" + activeProfile + ".properties";
                resources[i] = new ClassPathResource(propFile);
            }

            // we load the commond property file last so that properties from environment specific profile override those in common
            resources[activeProfiles.length] = new ClassPathResource("acceptance-tests.properties");

            propertyPlaceholderConfigurer.setLocations(resources);

            StringBuilder builder = new StringBuilder();
            for (String s : activeProfiles) {
                builder.append(s + ",");
            }
            logger.debug("Overriding default properties, using profile based properties from the following file(s): " + builder.toString());

        } else {
            // we set it to dummy file so that the configurer doesn't complain about unset locations property
            propertyPlaceholderConfigurer.setLocation(new ClassPathResource("NON-EXISTING-FILE.properties"));
        }

        // Allow for other PropertyPlaceholderConfigurer instances.
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        propertyPlaceholderConfigurer.setIgnoreResourceNotFound(true);
        propertyPlaceholderConfigurer.setOrder(100);

        return propertyPlaceholderConfigurer;
    }

}
