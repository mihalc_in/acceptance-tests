package com.caparzo.acceptance.tests.context.sftp;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class SftpRemoteFileRemover {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ServiceActivator
    public Message<?> process(Message<?> message) {
        logger.debug("Received SI message: {}", message);

        Boolean success = (Boolean) message.getPayload();
        logger.trace("Message's payload: {}", success);

        logger.trace("Message's headers:");
        MessageHeaders headers = message.getHeaders();
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            logger.trace("[{}={}]", entry.getKey(), entry.getValue());
        }

        String remoteDirectory = (String) message.getHeaders().get("file_remoteDirectory");
        String remoteFile = (String) message.getHeaders().get("file_remoteFile");

        if (!success) {
            throw new AcceptanceTestException("Remote file '" + remoteDirectory + remoteFile + "' could not be deleted!");
        }

        logger.debug("Remote file '{}' has been deleted successfully.", remoteDirectory + remoteFile);

        return message;
    }
}

