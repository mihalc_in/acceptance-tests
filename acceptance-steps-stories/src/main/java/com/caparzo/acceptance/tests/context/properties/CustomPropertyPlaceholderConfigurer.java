package com.caparzo.acceptance.tests.context.properties;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.Map;
import java.util.Properties;

public class CustomPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer implements InitializingBean {

    @Override
    public void afterPropertiesSet() {
        try {
            Properties loadedProperties = this.mergeProperties();
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<Object, Object> singleProperty : loadedProperties.entrySet()) {
                sb.append(singleProperty.getKey() + "=" + singleProperty.getValue() + "\n");
            }
            logger.trace("Loaded properties:\n" + sb.toString());
        } catch (Exception e) {
            throw new AcceptanceTestException("Problem occured when merging properties!", e);
        }
    }
}