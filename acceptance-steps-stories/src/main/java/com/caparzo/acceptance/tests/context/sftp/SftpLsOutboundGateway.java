package com.caparzo.acceptance.tests.context.sftp;

import org.springframework.integration.file.remote.FileInfo;

import java.util.List;

public interface SftpLsOutboundGateway {

    public List<FileInfo> lsFiles(String dir);
}
