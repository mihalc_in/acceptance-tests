package com.caparzo.acceptance.tests.utils;

import org.apache.commons.lang.Validate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.StringReader;

/**
 * Created by mihalcip on 06/05/2014.
 */
public class XPathUtils {

    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void evaluateXPath(Document xmlDocument, String xPathPath) {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression compiledExp = xPath.compile(xPathPath);
            Node node = (Node) compiledExp.evaluate(xmlDocument, XPathConstants.NODE);
            Validate.notNull(node, "Could not find target node for path - " + xPathPath);
            System.out.println(node.toString());
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><FinMessage xmlns=\"urn:swift:xsd:mtmsg.2013\"> <!--line=0--> <Block1> <ApplicationIdentifier>F</ApplicationIdentifier> <ServiceIdentifier>01</ServiceIdentifier> <LogicalTerminalAddress>BARCJPJ9BXXX</LogicalTerminalAddress> <SessionNumber>8</SessionNumber> <SequenceNumber>8</SequenceNumber> </Block1> <!--line=0--> <Block2> <OutputIdentifier>O</OutputIdentifier> <MessageType>950</MessageType> <InputTime>100100</InputTime> <MessageInputReference> <Date>140305</Date> <LTIdentifier>ESSESESSA</LTIdentifier> <BranchCode>XXX</BranchCode> <SessionNumber>2011</SessionNumber> <ISN>457738</ISN> </MessageInputReference> <Date>140305</Date> <Time>100100</Time> <MessagePriority>N</MessagePriority> </Block2> <!--line=0--> <Block3> <!--line=0--> <F108>TESTMT950/A</F108> </Block3> <!--line=0--> <Block4> <Document xmlns=\"urn:swift:xsd:fin.950.2013\"> <MT950> <F20a> <!--line=1--> <F20>TESTMT950/B</F20> </F20a> <F25a> <!--line=2--> <F25>52018558196</F25> </F25a> <F28a> <!--line=3--> <F28C> <StatementNumber>8</StatementNumber> <SequenceNumber>8</SequenceNumber> </F28C> </F28a> <F60a> <!--line=4--> <F60F> <DCMark>D</DCMark> <Date>140305</Date> <Currency>SEK</Currency> <Amount>96,</Amount> </F60F> </F60a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>D</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F61a> <!--line=7--> <F61> <ValueDate>140305</ValueDate> <EntryDate>0305</EntryDate> <DebitCreditMark>C</DebitCreditMark> <Amount>1500,</Amount> <TransactionType>N</TransactionType> <IdentificationCode>TRF</IdentificationCode> <ReferenceForTheAccountOwner>123456</ReferenceForTheAccountOwner> <ReferenceOfTheAccountServicingInstitution>123456</ReferenceOfTheAccountServicingInstitution> <SupplementaryDetails>INT.TRF.BARCGB95</SupplementaryDetails> </F61> </F61a> <F62a> <!--line=9--> <F62F> <DCMark>D</DCMark> <Date>140305</Date> <Currency>SEK</Currency> <Amount>6596,</Amount> </F62F> </F62a> </MT950> </Document> </Block4> <!--line=10--> <Block5> <!--line=10--> <CHK>TEST_950</CHK> </Block5> </FinMessage>";
        Document document = convertStringToDocument(xml);
        evaluateXPath(document, "FinMessage/Block4/Document/MT950/F61a[1]");
    }

}
