package com.caparzo.acceptance.tests.context.sftp;

public interface SftpRmOutboundGateway {

    public boolean rmFile(String file);

}
