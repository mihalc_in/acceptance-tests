package com.caparzo.acceptance.tests.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * This class contains all relevant timeout constants & methods used for waiting purposes
 *
 * @author mihalcip
 * @since 6.0.0-SNAPSHOT
 */
public class TimeoutUtils {

    private static final Logger logger = LoggerFactory.getLogger(TimeoutUtils.class);

    public static Long ACC_TESTS_OVERALL_TIMEOUT_MS = 10000L;
    public static Long ACC_TESTS_TIMEOUT_MS = 1000L;

    public static void doWait(long millis) {
        try {
            logger.info("Waiting " + millis + " ms...");
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            logger.warn("Wait has been interrupted!");
        }
    }

    public static void setTimeoutConstants(Long accTestsOverallTimeoutMs, Long accTestsTimeoutMs) {
        TimeoutUtils.ACC_TESTS_OVERALL_TIMEOUT_MS = accTestsOverallTimeoutMs;
        TimeoutUtils.ACC_TESTS_TIMEOUT_MS = accTestsTimeoutMs;
    }

}
