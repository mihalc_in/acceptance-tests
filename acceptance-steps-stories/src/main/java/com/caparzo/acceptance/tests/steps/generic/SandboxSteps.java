package com.caparzo.acceptance.tests.steps.generic;

import com.caparzo.acceptance.tests.core.jbehave.annotations.Steps;
import com.caparzo.acceptance.tests.utils.TimeoutUtils;
import org.jbehave.core.annotations.Given;

/**
 * Created by caparzo on 18. 6. 2015.
 */
@Steps
public class SandboxSteps {

    @Given("Hello from JBehave:$greeting")
    public void greet(String greeting) {
        TimeoutUtils.doWait(TimeoutUtils.ACC_TESTS_TIMEOUT_MS);

        System.out.println(greeting);
    }
}
