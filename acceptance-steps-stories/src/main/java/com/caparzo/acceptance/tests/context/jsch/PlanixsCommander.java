package com.caparzo.acceptance.tests.context.jsch;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import com.caparzo.acceptance.tests.utils.TimeoutUtils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Commands for remote control of Planixs Realiti.
 * Supports basic commands such as services status check (lb services status),
 * run/restart/stop of services, removing records from LogicBlox database, etc.
 * <p/>
 * User: ibragiah
 * Date: 27/02/2015
 * Time: 15:21
 */

@Component
public class PlanixsCommander {

    private static final int PLANIXS_DEFAULT_TIMEOUT = 2000;
    private static final int PLANIXS_OVERALL_TIMEOUT = 120000;

    @Value("${planixs.server.username}")
    private String username;

    @Value("${planixs.server.password}")
    private String password;

    @Value("${planixs.server.host}")
    private String host;

    private Logger logger = LoggerFactory.getLogger(PlanixsCommander.class);

    private Session session = null;

    /**
     * Returns a shell channel to run commands in
     *
     * @return Chanel
     */
    private Channel getChannel() {

        try {
            if (session != null) {
                return session.openChannel("shell");
            }

            JSch jsch = new JSch();

            session = jsch.getSession(username, host, 22);
            session.setPassword(password);

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            // preferred way how to authenticate, otherwise it complains about kerberos
            config.put("PreferredAuthentications", "password");
            session.setConfig(config);

            session.connect(30000);   // making a connection with timeout.

            return session.openChannel("shell");

        } catch (JSchException e) {
            throw new AcceptanceTestException("Error during initializing JSch session to Realiti server", e);
        }
    }

    /**
     * Method to load data file into Planixs
     *
     * @param fileToLoad the absolute path to csv file that needs to be loaded
     * @return load success boolean indicator
     */
    public boolean loadData(String fileToLoad) {
//        Path path = Paths.get(dataToLoad);
//        String filename = path.getFileName().toString();

        final String loadCommand = "realiti --import " + fileToLoad;

//        final String loadCommand = "lb web-client import -T 600 -f -i " + dataToLoad + " http://localhost:55183/data/" + filename;

        String output = runCommand(loadCommand);
        logger.debug("Output from command: |{}|", output);

        String[] lines = StringUtils.split(output, "\n");
        for (int i = lines.length - 1; i > 0; i--) { //looping from the end, since in the beginning there are trash data
            if (lines[i].contains("INFO    : Finished")) {
                if (lines[i - 1].contains("CAUSE|CAUSE_CODE")) {

                    return true;

                } else {
                    throw new AcceptanceTestException("Failed to load file '" + fileToLoad + "' into Realiti within the set timeout " + PLANIXS_OVERALL_TIMEOUT + "Ms");
                }
            } else if (lines[i].contains("status: ERROR")) {
                throw new AcceptanceTestException("Error while loading the file " + fileToLoad + ". See the command output above");
            }
        }

        throw new AcceptanceTestException("Failed to load file '" + fileToLoad + "' into Realiti");
    }

    public void clearTransactions() {
        final String deleteCmd = "lb exec /Maize '-Transaction(t) <- Transaction@prev(t).'";

        String output = runCommand(deleteCmd);

        logger.debug("Output from command: |{}|", output);

    }

    public List<String> getLoadedTransactions() {
        final String showTransactionsCmd = "lb print /Maize Transaction";

        Pattern pattern = Pattern.compile("\\[\\d+\\]\\s*\\\".+\\\".", Pattern.DOTALL); // to match: [10000013169] "1525690A-710D-444B-9A08-7B574C14ACC5"\n

        String output = runCommand(showTransactionsCmd);
        List<String> transactions = new ArrayList<>();
        for (String line : StringUtils.split(output, "\n")) {

            Matcher matcher = pattern.matcher(line);
            if (matcher.matches()) {
                String cleanLine = line.replaceAll("\\r", "");
                transactions.add(cleanLine);
            }
        }

        return transactions;
    }

    public Map<String, String> getStatusMap() {

        final String LB_COMPILER_SERVER = "lb-compiler-server";
        final String LB_PAGER = "lb-pager";
        final String LB_SERVER = "lb-server";
        final String LB_WEB_SERVER = "lb-web-server";

        String checkServicesCmd = "lb services status";
        String cmdOutput = runCommand(checkServicesCmd);

        Map<String, String> statusMap = new HashMap<>();
        for (String output : StringUtils.split(cmdOutput, "\n")) {

            String[] outputArr = StringUtils.split(output, ":");
            if (outputArr.length != 2) {
                continue;
            }

            logger.debug(output);

            String serviceName = outputArr[0].trim();
            String status = outputArr[1].trim();

            switch (serviceName) {
                case LB_COMPILER_SERVER:
                    statusMap.put(LB_COMPILER_SERVER, status);
                    break;
                case LB_PAGER:
                    statusMap.put(LB_PAGER, status);
                    break;
                case LB_SERVER:
                    statusMap.put(LB_SERVER, status);
                    break;
                case LB_WEB_SERVER:
                    statusMap.put(LB_WEB_SERVER, status);

            }
        }

        return statusMap;
    }

    public boolean isAllServicesRunning() {
        Map<String, String> statusMap = getStatusMap();

        for (Map.Entry entry : statusMap.entrySet()) {
            String value = (String) entry.getValue();
            if (value.contains("failed")) {
                return false;
            }
        }
        return true;
    }

    private String runCommand(String command) {

        Channel channel = getChannel();

        try {
            command = command.concat("\n"); //new line feed is important to run the command in shell
            InputStream stream = new ByteArrayInputStream(command.getBytes(Charset.defaultCharset()));
            channel.setInputStream(stream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            channel.setOutputStream(outputStream);


            channel.connect(2 * PLANIXS_OVERALL_TIMEOUT);

            int timeSpent = 0;
            String cmdOutput = outputStream.toString(Charset.defaultCharset().name());
            String bashPS1 = username + "@" + host; //sysread1@ldndsr000006034
            while (timeSpent < PLANIXS_OVERALL_TIMEOUT) {
                TimeoutUtils.doWait(PLANIXS_DEFAULT_TIMEOUT);

                int firstIndex = StringUtils.indexOf(cmdOutput, bashPS1);
                int lastIndex = StringUtils.lastIndexOf(cmdOutput, bashPS1);

                if (firstIndex != lastIndex) {
                    break; //commands has finished when there are two or more bashPS1 are found.
                }
                cmdOutput = outputStream.toString(Charset.defaultCharset().name());
                timeSpent += PLANIXS_DEFAULT_TIMEOUT;
            }

            channel.disconnect();

            return cmdOutput;

        } catch (Exception e) {
            throw new AcceptanceTestException("Errors during sending '" + command + "' command", e);
        }
    }

}
