package com.caparzo.acceptance.tests.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Utils for working with Json
 *
 * @author sampsonn
 */
public final class JsonUtils {
    /**
     * Convert any object into a json string
     */
    public static String convertObjectToJsonString(Object object) {
        String text = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            StringWriter stringWriter = new StringWriter();
            objectMapper.writeValue(stringWriter, object);
            text = stringWriter.toString();
            return text;
        } catch (Exception e) {
            throw new RuntimeException("Exception converting object to Json string", e);
        }
    }

    /**
     * Converts json string to object, if compatible
     */
    public static <T> T convertJsonStringToObject(String jsonString, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            T object = objectMapper.readValue(jsonString, clazz);
            return object;
        } catch (Exception e) {
            throw new RuntimeException("Error converting Json string to object", e);
        }
    }

    /**
     * Converts map to string, if compatible
     */
    public static String convertMapToJsonString(Map<String, String> map) {
        return new JSONObject(map).toString();
    }

    /**
     * Convert json string to map.
     * This method converts to Map<String, String> because when using Map<String, ?> json will vary the type depending on the data.
     * For example, it will treat '100' as an Integer and '100.23' as a Double even when coming from the same field.
     * By always converting to String, the client code knows what it's getting and takes care of any subsequent conversion as required.
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> convertJsonStringToMap(String jsonString) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Object object = objectMapper.readValue(jsonString, Object.class);
            Map<String, ?> objectMap = (Map<String, ?>) object;

            Map<String, String> map = new HashMap<String, String>(objectMap.size());
            for (String key : objectMap.keySet()) {
                Object value = objectMap.get(key);
                if (value == null)
                    map.put(key, null);
                else {
                    String strValue = value.toString();
                    map.put(key, strValue);
                }
            }
            return map;
        } catch (Exception e) {
            throw new RuntimeException("Error converting Json string to object map", e);
        }
    }

    public static String prettyPrintJson(String jsonString) throws JSONException {
        return new JSONObject(jsonString).toString(2);
    }

}

