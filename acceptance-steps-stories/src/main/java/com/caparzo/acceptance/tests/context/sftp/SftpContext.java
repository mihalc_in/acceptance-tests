package com.caparzo.acceptance.tests.context.sftp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

import java.util.Properties;

/**
 * Spring configuration related to SFTP.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@Configuration
public class SftpContext {

    @Value("${tes.acctest.sftp.host}")
    private String tesHost;

    @Value("${tes.acctest.sftp.user}")
    private String tesUser;

    @Value("${tes.acctest.sftp.password}")
    private String tesPassword;

    @Value("${planixs.server.host}")
    private String realitiHost;

    @Value("${planixs.server.username}")
    private String realitiUser;

    @Value("${planixs.server.password}")
    private String realitiPassword;

    @Bean
    public SessionFactory sftpSessionFactory() {

        DefaultSftpSessionFactory sessionFactory = new DefaultSftpSessionFactory();

        sessionFactory.setHost(tesHost);
        sessionFactory.setPort(22);
        sessionFactory.setUser(tesUser);
        sessionFactory.setPassword(tesPassword);

        Properties sessionConfig = new Properties();
        String preferredMethod = "password";
        sessionConfig.put("PreferredAuthentications", preferredMethod);
        sessionConfig.setProperty("PreferredAuthentications", preferredMethod);
        sessionFactory.setSessionConfig(sessionConfig);

        CachingSessionFactory cachingSessionFactory = new CachingSessionFactory(sessionFactory);

        return cachingSessionFactory;
    }

    @Bean
    public SessionFactory realitiSftpSessionFactory() {

        DefaultSftpSessionFactory sessionFactory = new DefaultSftpSessionFactory();

        sessionFactory.setHost(realitiHost);
        sessionFactory.setPort(22);
        sessionFactory.setUser(realitiUser);
        sessionFactory.setPassword(realitiPassword);

        Properties sessionConfig = new Properties();
        String preferredMethod = "password";
        sessionConfig.put("PreferredAuthentications", preferredMethod);
        sessionConfig.setProperty("PreferredAuthentications", preferredMethod);
        sessionFactory.setSessionConfig(sessionConfig);

        CachingSessionFactory cachingSessionFactory = new CachingSessionFactory(sessionFactory);

        return cachingSessionFactory;
    }

}
