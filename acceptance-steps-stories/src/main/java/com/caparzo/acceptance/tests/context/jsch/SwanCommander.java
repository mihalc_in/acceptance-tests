package com.caparzo.acceptance.tests.context.jsch;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Properties;

/**
 * Commands for remote control of Swan.
 * Supports clearing and reading transaction logs
 * Please note that this is temporary solution and will be removed once we have integrated solution
 * <p/>
 * User: mihalcip
 * Date: 09/03/2015
 * Time: 15:21
 */

@Component
public class SwanCommander {

    private static final int DEFAULT_WAIT_TIMEOUT = 1000;

    @Value("${tes.acctest.sftp.user}")
    private String username;

    @Value("${tes.acctest.sftp.password}")
    private String password;

    @Value("${tes.acctest.sftp.host}")
    private String host;

    private Logger logger = LoggerFactory.getLogger(SwanCommander.class);

    private Session session = null;

    /**
     * Returns a shell channel to run commands in
     *
     * @return Chanel
     */
    private Channel getChannel() {

        try {
            if (session != null) {
                return session.openChannel("shell");
            }

            JSch jsch = new JSch();

            session = jsch.getSession(username, host, 22);
            session.setPassword(password);

            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            // preferred way how to authenticate, otherwise it complains about kerberos
            config.put("PreferredAuthentications", "password");

            session.setConfig(config);

            session.connect(30000);   // making a connection with timeout.

            return session.openChannel("shell");

        } catch (JSchException e) {
            throw new AcceptanceTestException("Error during initializing JSch session to Realiti server", e);
        }
    }

    public String readTransactionCsvLog(String pathToCsv) {
        final String readCmd = "cat " + pathToCsv;
        String output = runCommand(readCmd, DEFAULT_WAIT_TIMEOUT);
        logger.debug("Output from command: |{}|", output);
        return output;
    }

    public String emptyTransactionCsvLog(String pathToCsv) {
        final String csvHeader = "Account|Amount|Garbage|Id|InsertTime|SwiftId|Timestamp|UsedToProof|Version|ccy|closingBalanceDate|descOfSecurity|drCrMark|extAccId|extAccSdsId|idOfSecurity|inputTime|intPartyAccount|intPartyAddress|intPartyBic|intPartyName|msgId|msgType|orderingPartyAccount|orderingPartyAddress|orderingPartyBic|orderingPartyName|outputTime|processedTs|receiverBic|refAccServicingInst|refForAccOwner|securityPrice|securityQuantity|sendRecInfo|senderBic|type|valueAmount|valueDate";
        final String emptyCmd = "echo \"" + csvHeader + "\" > " + pathToCsv;
        String output = runCommand(emptyCmd, DEFAULT_WAIT_TIMEOUT);
        logger.debug("Output from command: |{}|", output);
        return output;
    }

    private String runCommand(String command, int timeToWait) {

        Channel channel = getChannel();

        try {
            command = command.concat("\n"); //new line feed is important to run the command in shell
            InputStream stream = new ByteArrayInputStream(command.getBytes(Charset.defaultCharset()));
            channel.setInputStream(stream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            channel.setOutputStream(outputStream);


            channel.connect(3 * timeToWait);

            int oldSize = outputStream.size();
            Thread.sleep(timeToWait);
            int newSize = outputStream.size();

            while (oldSize != newSize) {
                oldSize = newSize;
                Thread.sleep(timeToWait);
                newSize = outputStream.size();
            }

            channel.disconnect();

            return outputStream.toString(Charset.defaultCharset().name());
        } catch (Exception e) {
            throw new AcceptanceTestException("Errors during sending '" + command + "' command", e);
        }
    }

}
