package com.caparzo.acceptance.tests;

import com.caparzo.acceptance.tests.apt.jbehave.unittest.GenerateStoryTestMethods;
import org.springframework.test.annotation.IfProfileValue;

import static com.caparzo.acceptance.tests.common.ProfileName.*;

/**
 * Used to execute JBehave story tests.
 * User: mihalcip
 * Date: 25/04/14
 * Time: 13:43
 */
@GenerateStoryTestMethods("stories")
@IfProfileValue(name = STRING_ACTIVE_PROFILES_KEY, values = {dev, env})
public abstract class All extends BaseStoriesTest {

}
