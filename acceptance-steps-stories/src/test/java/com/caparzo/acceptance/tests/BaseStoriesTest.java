package com.caparzo.acceptance.tests;

import com.caparzo.acceptance.tests.core.context.AccTestsAppContextInitializer;
import com.caparzo.acceptance.tests.core.jbehave.JBehaveEmbedder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

/**
 * Contains base functionality for running JBehave story tests.
 * User: stasyukd
 * Date: 06/08/13
 * Time: 11:56
 */
@ContextConfiguration(initializers = {AccTestsAppContextInitializer.class}, locations = "classpath:/META-INF/spring/acceptance-tests-context.xml")
public abstract class BaseStoriesTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    protected JBehaveEmbedder embedder;

    protected void runStoryTest(String storyFile) {
        embedder.runStories(storyFile);
    }

}
