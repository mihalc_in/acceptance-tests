package com.caparzo.acceptance.tests.core.jbehave.parameters;

/**
 * Represents a story parameter which holds a simple string value.
 * User: stasyukd
 * Date: 06/08/13
 * Time: 13:29
 */
public class MultiLineStoryStringParameter {

    private final String[] lines;

    public MultiLineStoryStringParameter(String[] lines) {
        this.lines = lines;
    }

    public String[] getLines() {
        return lines;
    }
}
