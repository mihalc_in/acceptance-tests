package com.caparzo.acceptance.tests.core.jbehave.converters;


import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.apache.commons.lang.Validate;
import org.jbehave.core.steps.ParameterConverters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

/**
 * Facade type ParameterConverter implementation that delegates to either jason converter or a the tabular data converter.
 * So with this converter the user now can enter story parameters in the form of either jason string or using the tabular
 * format typically found in jbehave story files.
 * User: stasyukd
 * Date: 05/09/13
 * Time: 10:42
 */
@Component
public class FilObjectsParameterConverterFacade implements ParameterConverters.ParameterConverter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private JsonStringToObjectParameterConverter jsonConverter;

    private TabularStringToObjectParameterConverter tabularConverter;

    public FilObjectsParameterConverterFacade() {
        jsonConverter = new JsonStringToObjectParameterConverter();
        tabularConverter = new TabularStringToObjectParameterConverter();
    }

    @Override
    public boolean accept(Type type) {

        // does not support enum types
        if (type instanceof Class && ((Class) type).isEnum()) {
            return false;
        }

        boolean accepts = jsonConverter.accept(type);
        if (accepts) {
            return true;
        } else {
            accepts = tabularConverter.accept(type);
            return accepts;
        }
    }

    @Override
    public Object convertValue(String value, Type type) {

        logger.trace("Converting value: " + value + " to type: " + type);

        Validate.notEmpty(value, "Value for type - '" + type + "' was empty");
        value = value.trim();

        /* we delegate to one of the two converters based on the beginning of the string - if it begings with '{' then
        * we delegate to the json one and if it begins with '|' then to the tabular format one
        */
        if (value.startsWith("{")) {
            return jsonConverter.convertValue(value, type);
        } else if (value.startsWith("|")) {
            return tabularConverter.convertValue(value, type);
        } else {
            throw new AcceptanceTestException("Invalid beginning of story parameter found. Parameter must start with either '{' " +
                    "for jason format or '|' for tabular format. But parameter was - " + value);
        }

    }

}
