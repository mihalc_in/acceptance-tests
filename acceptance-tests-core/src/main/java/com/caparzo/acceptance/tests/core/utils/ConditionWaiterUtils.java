package com.caparzo.acceptance.tests.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

import java.util.concurrent.TimeUnit;

/**
 * Use to cause the current thread to wait/block until either the timeout is reached or a condition is true.
 * User: stasyukd
 * Date: 10/10/13
 * Time: 12:25
 */
public class ConditionWaiterUtils {

    private static final Logger logger = LoggerFactory.getLogger(ConditionWaiterUtils.class);

    private ConditionWaiterUtils() {
        throw new UnsupportedOperationException("Not intended for instantiation");
    }

    public static interface ConditionChecker {

        public boolean isMatched();
    }

    /**
     * Handy method for waiting for a number of records in the database.
     *
     * @param repository
     * @param expectedCount
     * @return
     */
    public static boolean awaitForRepoCount(final CrudRepository repository, final int expectedCount, int maxWaitInSeconds) {

        return await(new ConditionChecker() {
            @Override
            public boolean isMatched() {
                return repository.count() == expectedCount;
            }
        }, TimeUnit.SECONDS.toMillis(maxWaitInSeconds));
    }

    /**
     * Handy method for waiting for a number of records in the database.
     *
     * @param repository
     * @param expectedCount
     * @return
     */
    public static boolean awaitForRepoCount(final CrudRepository repository, final int expectedCount) {

        return await(new ConditionChecker() {
            @Override
            public boolean isMatched() {
                return repository.count() == expectedCount;
            }
        });
    }

    /**
     * same as the await with timeout, but uses default time out value of 5 seconds
     *
     * @param cc
     * @return
     */
    public static boolean await(ConditionChecker cc) {
        return await(cc, TimeUnit.SECONDS.toMillis(5));
    }

    /**
     * true if the condition check by the specified checker was matched and false if the waiting time elapsed
     * before the condition could be matched
     *
     * @param timeout - in milliseconds
     * @return
     */
    public static boolean await(ConditionChecker cc, long timeout) {

        long startedAt = System.currentTimeMillis();
        long maxCheckAt = startedAt + timeout;

        logger.debug("Waiting for maximum of - " + TimeUnit.SECONDS.convert(timeout, TimeUnit.MILLISECONDS)
                + " seconds for a condition to be matched...");

        while (System.currentTimeMillis() <= maxCheckAt) {

            boolean matched = cc.isMatched();
            if (matched) {
                logger.debug("Condition was matched before timeout reached.");
                return true;
            } else {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        logger.debug("Condition failed to match before the timeout was reached.");
        return false;
    }

    /**
     * true if the condition check by the specified checker was matched and false if the waiting time elapsed
     * before the condition could be matched
     *
     * @param cc
     * @param timeout
     * @return
     */
    public static boolean await(ConditionChecker cc, Timeout timeout) {

        return await(cc, timeout.unit.toMillis(timeout.timeout));
    }

}
