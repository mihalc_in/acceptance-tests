package com.caparzo.acceptance.tests.core.exceptions;

/**
 * Base class for all runtime exceptions related to running of acceptance tests.
 * User: stasyukd
 * Date: 03/09/13
 * Time: 14:07
 */
public class AcceptanceTestException extends RuntimeException {

    public AcceptanceTestException(String errorMsg) {
        super(errorMsg);
    }

    public AcceptanceTestException(Throwable t) {
        super(t);
    }

    public AcceptanceTestException(String errorMsg, Throwable t) {
        super(errorMsg, t);
    }

}
