package com.caparzo.acceptance.tests.core.jbehave.interceptors;


import com.caparzo.acceptance.tests.common.date.DateUtils;
import com.caparzo.acceptance.tests.core.utils.DateFormats;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used to expand variables specified in story files.
 * User: stasyukd
 * User: mihalcip | Refactored class to use various regexp patterns instead of hardcoded values
 * Date: 23/08/13
 * Time: 10:31
 */
public class StringVariableExpander {

    private static final Logger logger = LoggerFactory.getLogger(StringVariableExpander.class);
    private static int dotAllValue = Pattern.DOTALL;
    private static final Pattern VAR_PATTERN = Pattern.compile("(.*)(<)(.*?)(>)(.*)", dotAllValue);

    private static final Pattern DATE_PATTERN = Pattern.compile("(\\w*)(\\+|-)(\\d*)(d|bd)", dotAllValue);
    private static final Pattern MILLIS_PATTERN = Pattern.compile("currentTimeMillis(\\+|-)(\\d*)(s)", dotAllValue);
    private static final Pattern RANDOM_PATTERN = Pattern.compile("(random_digits|random_string)(\\()(\\d*)(\\))", dotAllValue);

    public StringVariableExpander() {
        throw new IllegalStateException("Not intended for instantiation");
    }

    public static String expand(String parameter) {
        logger.trace("Expanding variable " + parameter);
        String expandedParameter = parameter;
        Matcher matcher = VAR_PATTERN.matcher(parameter);
        while (matcher.matches()) {
            String g1 = matcher.group(1); // before part
            String g2 = matcher.group(2); // <
            String g3 = matcher.group(3); // variable
            String g4 = matcher.group(4); // >
            String g5 = matcher.group(5); // after part

            String expandedVariable = expandVariable(g3);
            expandedParameter = g1 + expandedVariable + g5;
            matcher.reset(expandedParameter);
        }

        if (parameter.startsWith("<") && parameter.endsWith(">")) {
            logger.info("Expanded variable " + parameter + " = " + expandedParameter);
        }

        return expandedParameter;
    }

    private static String expandVariable(String variable) {

        Validate.isTrue(StringUtils.isNotEmpty(variable));

        Matcher dateMatcher = DATE_PATTERN.matcher(variable);
        Matcher millisMatcher = MILLIS_PATTERN.matcher(variable);
        Matcher randomMatcher = RANDOM_PATTERN.matcher(variable);


        if (dateMatcher.matches()) {
            String keyword = dateMatcher.group(1); // keyword, e.g. today_date
            String plusMinus = dateMatcher.group(2); // +/-
            String digits = dateMatcher.group(3); // digits
            int number = Integer.parseInt(plusMinus + digits);
            String unit = dateMatcher.group(4); // d/bd
            return expandDateVariable(keyword, number, unit);
        }

        if (millisMatcher.matches()) {
            String plusMinus = millisMatcher.group(1); // +/-
            String digits = millisMatcher.group(2); // digits
            String unit = millisMatcher.group(3);
            int number = Integer.parseInt(plusMinus + digits);
            return expandMillisVariable(number, unit);
        }

        if (randomMatcher.matches()) {
            String keyword = randomMatcher.group(1); // keyword, either random_digits or random_string
            String digits = randomMatcher.group(3); // number
            int number = Integer.parseInt(digits);
            return expandRandomVariable(keyword, number);
        }
        Calendar c = Calendar.getInstance();
        switch (variable) {
            case "today_timestamp":
                variable = "" + c.getTime().getTime();
                break;
            case "today_date":
                variable = new SimpleDateFormat(DateFormats.YYYY_DASH_MM_DASH_DD).format(c.getTime());
                break;
            case "today_date_YYYYMMDD":
                variable = new SimpleDateFormat(DateFormats.YYYYMMDD).format(c.getTime());
                break;
            case "today_date_DDMMYYYY":
                variable = new SimpleDateFormat(DateFormats.DDMMYYYY).format(c.getTime());
                break;
            case "current_date_KDB":
                variable = new SimpleDateFormat(DateFormats.KDB_DATE_FORMAT).format(c.getTime());
                break;
            case "today_timestamp_string":
                variable = new SimpleDateFormat(DateFormats.KDB_TIMESTAMP_FORMAT).format(c.getTime());
                break;
            case "today_timestamp_kdb":
                variable = new SimpleDateFormat(DateFormats.PAYLOAD_DATETIME_Z).format(c.getTime());
                break;
            case "current_date":
                variable = new SimpleDateFormat(DateFormats.YYYY_DASH_MM_DASH_DD).format(c.getTime());
                break;
            case "swift_long_today_date":
                variable = new SimpleDateFormat(DateFormats.YYMMDD).format(c.getTime());
                break;
            case "swift_short_today_date":
                variable = new SimpleDateFormat(DateFormats.MMDD).format(c.getTime());
                break;
            case "realiti_today_date":
                variable = new SimpleDateFormat(DateFormats.YYYYMMDD).format(c.getTime());
                break;
            case "currentTimeMillis":
                variable = "" + System.currentTimeMillis();
                break;
            case "currentTimeMillisToday":
                Date date = new Date();
                variable = "" + (date.getTime() - DateUtils.truncateTime(date).getTime());
                break;
        }

        return variable;

    }

    private static String expandDateVariable(String keyword, Integer number, String unit) {

        Date futureWeekDay = null;
        Date pastWeekDay = null;
        Date dayFromToday = null;
        String variable = "";

        SimpleDateFormat dateFormat = null;
        String format = getFormat(keyword);

        if (format != null && !format.equalsIgnoreCase(keyword)) {
            dateFormat = new SimpleDateFormat(format);
        }
        Calendar c = Calendar.getInstance();
        switch (unit) {
            case "d":
                dayFromToday = DateUtils.getDayFromToday(number);
                if (format.equalsIgnoreCase(keyword)) {
                    return "" + dayFromToday.getTime();
                }

                variable = dateFormat.format(dayFromToday);
                return variable;
            case "bd":
                if (number > 0) {
                    futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), Math.abs(number));
                    if (format.equalsIgnoreCase(keyword)) {
                        return "" + futureWeekDay.getTime();
                    }
                    variable = dateFormat.format(futureWeekDay);
                    return variable;
                } else {
                    pastWeekDay = DateUtils.getPastWeekDay(c.getTime(), Math.abs(number));
                    if (format.equalsIgnoreCase(keyword)) {
                        return "" + pastWeekDay.getTime();
                    }
                    variable = dateFormat.format(pastWeekDay);
                    return variable;
                }
        }

        return variable;
    }

    private static String getFormat(String keyword) {
        String format = keyword;
        switch (keyword) {
            case "today_timestamp_kdb":
                format = DateFormats.PAYLOAD_DATETIME_Z;
                break;
            case "today_date_YYYYMMDD":
                format = DateFormats.YYYYMMDD;
                break;
            case "swift_long_today_date":
                format = DateFormats.YYMMDD;
                break;
            case "swift_short_today_date":
                format = DateFormats.MMDD;
                break;
            case "current_date":
                format = DateFormats.YYYY_DASH_MM_DASH_DD;
                break;
            case "today_date_DDMMYYYY":
                format = DateFormats.DDMMYYYY;
                break;
            case "current_date_KDB":
                format = DateFormats.KDB_DATE_FORMAT;
                break;
            case "today_timestamp_string":
                format = DateFormats.KDB_TIMESTAMP_FORMAT;
                break;
            case "today_date":
                format = DateFormats.YYYY_DASH_MM_DASH_DD;
        }
        return format;
    }

    private static String expandMillisVariable(Integer number, String unit) {
        if ("s".equalsIgnoreCase(unit)) {
            number = number * 1000;
        }

        return "" + (System.currentTimeMillis() + number);
    }

    private static String expandRandomVariable(String keyword, int number) {
        switch (keyword) {
            case "random_digits":
                String numeric = RandomStringUtils.randomNumeric(number);
                if (numeric.startsWith("0")) {
                    numeric = StringUtils.replaceOnce(numeric, "0", "1"); //changing first 0 to 1
                }
                return numeric;
            case "random_string":
                return RandomStringUtils.randomAlphanumeric(number);
        }

        return keyword;
    }

    public static void main(String[] args) {
        System.out.println(expand("<today_timestamp>"));
        System.out.println(expand("<today_timestamp+1bd>"));
        System.out.println(expand("<today_timestamp+2bd>"));
        System.out.println(expand("<today_timestamp+3bd>"));
        System.out.println(expand("<today_timestamp+4bd>"));
        System.out.println(expand("<today_timestamp+5bd>"));
        System.out.println(expand("<today_timestamp+6bd>"));
        System.out.println(expand("<today_timestamp_kdb-1d>"));
        System.out.println(expand("<today_date>"));
        System.out.println(expand("<today_date+1d>"));
        System.out.println(expand("<today_date+1bd>"));
        System.out.println(expand("<today_date+2bd>"));
        System.out.println(expand("<today_date+3bd>"));
        System.out.println(expand("<today_date+4bd>"));
        System.out.println(expand("<today_date+5bd>"));
        System.out.println(expand("<today_date+6bd>"));
        System.out.println(expand("<today_date+7bd>"));
        System.out.println(expand("<today_date-1bd>"));
        System.out.println(expand("<today_date-30bd>"));
        System.out.println(expand("<current_date+1d>"));
        System.out.println(expand("<current_date+2d>"));
        System.out.println(expand("<current_date+3d>"));
        System.out.println(expand("<current_date+4d>"));
        System.out.println(expand("<current_date-1d>"));
        System.out.println(expand("<today_date-1d>"));
        System.out.println(expand("<current_date-2d>"));
        System.out.println(expand("<current_date-3d>"));
        System.out.println(expand("<current_date-4d>"));
        System.out.println(expand("<current_date-5d>"));
        System.out.println(expand("<current_date-6d>"));
        System.out.println(expand("<current_date-7d>"));
        System.out.println(expand("<current_date-29d>"));
        System.out.println(expand("<current_date-28d>"));
        System.out.println(expand("<current_date-25d>"));
        System.out.println(expand("<current_date-23d>"));
        System.out.println(expand("<current_date+28d>"));
        System.out.println(expand("<current_date+29d>"));
        System.out.println(expand("<current_date-30d>"));
        System.out.println(expand("<current_date+6d>"));
        System.out.println(expand("<current_date+7d>"));
        System.out.println(expand("<current_date-1bd>"));
        System.out.println(expand("<today_date_YYYYMMDD>"));
        System.out.println(expand("<today_date_YYYYMMDD-1d>"));
        System.out.println(expand("<today_date_YYYYMMDD+1d>"));
        System.out.println(expand("<today_date_DDMMYYYY>"));
        System.out.println(expand("<current_date_KDB>"));
        System.out.println(expand("<today_timestamp_string>"));
        System.out.println(expand("<today_timestamp_kdb>"));
        System.out.println(expand("<current_date>"));
        System.out.println(expand("<swift_long_today_date>"));
        System.out.println(expand("<swift_short_today_date>"));
        System.out.println(expand("<swift_long_today_date-1d>"));
        System.out.println(expand("<swift_long_today_date-2d>"));
        System.out.println(expand("<swift_long_today_date-3d>"));
        System.out.println(expand("<swift_long_today_date-4d>"));
        System.out.println(expand("<swift_long_today_date+1d>"));
        System.out.println(expand("<swift_long_today_date+2d>"));
        System.out.println(expand("<swift_long_today_date+3d>"));
        System.out.println(expand("<swift_long_today_date+4d>"));
        System.out.println(expand("<swift_short_today_date-1d>"));
        System.out.println(expand("<swift_short_today_date-2d>"));
        System.out.println(expand("<swift_short_today_date-3d>"));
        System.out.println(expand("<swift_short_today_date-4d>"));
        System.out.println(expand("<swift_short_today_date+1d>"));
        System.out.println(expand("<swift_short_today_date+2d>"));
        System.out.println(expand("<swift_short_today_date+3d>"));
        System.out.println(expand("<swift_short_today_date+4d>"));
        System.out.println(expand("<currentTimeMillis>"));
        System.out.println(expand("<currentTimeMillis+1s>"));
        System.out.println(expand("<currentTimeMillis+2s>"));
        System.out.println(expand("<currentTimeMillis+3s>"));
        System.out.println(expand("<currentTimeMillis+4s>"));
        System.out.println(expand("<currentTimeMillis+5s>"));
        System.out.println(expand("<currentTimeMillis+6s>"));
        System.out.println(expand("<currentTimeMillis+7s>"));
        System.out.println(expand("<currentTimeMillis+8s>"));
        System.out.println(expand("<currentTimeMillis+9s>"));
        System.out.println(expand("<currentTimeMillis+10s>"));
        System.out.println(expand("<currentTimeMillis+11s>"));
        System.out.println(expand("<currentTimeMillis+12s>"));
        System.out.println(expand("<currentTimeMillis+13s>"));
        System.out.println(expand("<currentTimeMillis+14s>"));
        System.out.println(expand("<currentTimeMillis+15s>"));
        System.out.println(expand("<currentTimeMillis+16s>"));
        System.out.println(expand("<currentTimeMillis+17s>"));
        System.out.println(expand("<currentTimeMillis+18s>"));
        System.out.println(expand("<currentTimeMillis+19s>"));
        System.out.println(expand("<currentTimeMillis+20s>"));
        System.out.println(expand("<currentTimeMillis+21s>"));
        System.out.println(expand("<currentTimeMillis+22s>"));
        System.out.println(expand("<currentTimeMillisToday>"));
        System.out.println(expand("<random_digits(9)>"));
        System.out.println(expand("<random_string(15)>"));
    }

}
