package com.caparzo.acceptance.tests.core.jbehave.parameters;

import com.caparzo.acceptance.tests.core.jbehave.annotations.DoNotExpandVariables;

/**
 * Used to pass in a string type parameter that should not have any variables inside it expanded.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@DoNotExpandVariables
public class StringWithoutVariablesParameter {

    private final String value;

    public StringWithoutVariablesParameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
