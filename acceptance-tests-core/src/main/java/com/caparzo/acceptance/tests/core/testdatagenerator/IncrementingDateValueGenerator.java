package com.caparzo.acceptance.tests.core.testdatagenerator;


import com.caparzo.acceptance.tests.common.date.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Generates an Date value incremented by a day each time or after a specified number of steps.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class IncrementingDateValueGenerator implements ValueGenerator<Date> {

    private Date currentValue;
    private int incrementAfterSteps = 1;
    private boolean businessDaysOnly = false;

    private int counter = 0;

    /**
     * Creates a generator that will increment date each time.
     *
     * @param initialValue starting date
     */
    public IncrementingDateValueGenerator(Date initialValue) {
        this.currentValue = initialValue;
    }

    /**
     * Creates a generator that will increment date after a specified number of steps.
     *
     * @param initialValue starting date
     * @param count        number of steps after which the date is increased
     */
    public IncrementingDateValueGenerator(Date initialValue, int count) {
        this(initialValue);
        this.incrementAfterSteps = count;
    }

    /**
     * Creates a generator that will increment date after a specified number of steps.
     * Only business days can be specified.
     *
     * @param initialValue        starting date
     * @param incrementAfterSteps number of steps after which the date is increased
     * @param businessDaysOnly    return only business days
     */
    public IncrementingDateValueGenerator(Date initialValue, int incrementAfterSteps, boolean businessDaysOnly) {
        this.currentValue = initialValue;
        this.incrementAfterSteps = incrementAfterSteps;
        this.businessDaysOnly = businessDaysOnly;
    }

    @Override
    public Date generateValue() {
        Date toReturn = currentValue;
        counter++;
        if (counter == incrementAfterSteps) {
            Calendar c = Calendar.getInstance();
            c.setTime(toReturn);
            if (businessDaysOnly) {
                c.setTime(DateUtils.getFutureWeekDay(c.getTime(), 1));
            } else {
                c.add(Calendar.DAY_OF_YEAR, 1);
            }
            Date incrementedDate = c.getTime();
            counter = 0;
            currentValue = incrementedDate;
        }
        return toReturn;
    }

}
