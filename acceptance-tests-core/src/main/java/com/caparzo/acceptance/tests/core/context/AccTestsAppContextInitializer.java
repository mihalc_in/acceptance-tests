package com.caparzo.acceptance.tests.core.context;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Application context initializer
 *
 * @author mihalcip, created on 29/08/2014 16:23
 */
public class AccTestsAppContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        String[] activeProfiles = applicationContext.getEnvironment().getActiveProfiles();
        String systemProperty = StringUtils.join(activeProfiles, ",");
        System.setProperty("spring.profiles.active", systemProperty);
    }
}
