package com.caparzo.acceptance.tests.core.jbehave.parameters;

import org.jbehave.core.annotations.AsParameters;

/**
 * Parameter for specify value for xml path in Story files.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@AsParameters
public class XmlPathValueParameter {

    private String path;

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPath() {

        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "XmlPathValueParameter{" +
                "path='" + path + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
