package com.caparzo.acceptance.tests.core.testdatagenerator;

import com.caparzo.acceptance.tests.apt.javafieldinfo.JavaFieldInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TestDataGenerator<E> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private Class<E> entityType;

    private static class FieldInfoGeneratorCombo {
        ValueGenerator valueGenerator;
        JavaFieldInfo javaFieldInfo;

        public <T> FieldInfoGeneratorCombo(JavaFieldInfo<T> javaFieldInfo, ValueGenerator valueGenerator) {
            this.javaFieldInfo = javaFieldInfo;
            this.valueGenerator = valueGenerator;
        }
    }

    private Map<String, FieldInfoGeneratorCombo> generators = new HashMap<String, FieldInfoGeneratorCombo>();

    public TestDataGenerator(Class<E> type) {
        this.entityType = type;
    }

    public <T> TestDataGenerator<E> useOneOfIncrementally(JavaFieldInfo<T> propertyInfo, final T... possibleValues) {
        List<T> objectsList = Arrays.asList(possibleValues);
        this.useOneOfIncrementally(propertyInfo, objectsList);
        return this;
    }

    public <T> TestDataGenerator<E> useOneOfIncrementally(JavaFieldInfo<T> propertyInfo, final List<T> possibleValues) {
        ValueGenerator randomValuesGenerator = new OneOfIncrementallyValueGenerator(possibleValues);
        generators.put(propertyInfo.getName(), new FieldInfoGeneratorCombo(propertyInfo, randomValuesGenerator));
        return this;
    }

    public <T> TestDataGenerator<E> useOneOfRandomly(JavaFieldInfo<T> propertyInfo, final T... possibleValues) {
        List<T> objectsList = Arrays.asList(possibleValues);
        this.useOneOfRandomly(propertyInfo, objectsList);
        return this;
    }

    public <T> TestDataGenerator<E> useOneOfRandomly(JavaFieldInfo<T> propertyInfo, final List<T> possibleValues) {
        ValueGenerator randomValuesGenerator = new OneOfRandomlyValueGenerator(possibleValues);
        generators.put(propertyInfo.getName(), new FieldInfoGeneratorCombo(propertyInfo, randomValuesGenerator));
        return this;
    }

    public <T> TestDataGenerator<E> useFixed(JavaFieldInfo<T> propertyInfo, final T fixedValue) {

        ValueGenerator fixedValueGenerator = new FixedValueGenerator(fixedValue);
        generators.put(propertyInfo.getName(), new FieldInfoGeneratorCombo(propertyInfo, fixedValueGenerator));
        return this;
    }

    /**
     * Supports only Integer, Long and Date field types.
     *
     * @param propertyInfo
     * @param initialValue
     */
    public <T> TestDataGenerator<E> useIncrementing(JavaFieldInfo<T> propertyInfo, final T initialValue) {

        ValueGenerator generator;

        Class<?> type = initialValue.getClass();

        if (type == Long.class || type == long.class) {
            generator = new IncrementingLongValueGenerator((Long) initialValue);
        } else if (type == Integer.class || type == int.class) {
            generator = new IncrementingIntValueGenerator(initialValue);
        } else if (type == Date.class) {
            generator = new IncrementingDateValueGenerator((Date) initialValue);
        } else {
            throw new IllegalArgumentException("Unsupported type specified - " + type + ". Only Long, Integer or Date are supported by this generator method.");
        }

        generators.put(propertyInfo.getName(), new FieldInfoGeneratorCombo(propertyInfo, generator));
        return this;
    }

    public <T> TestDataGenerator<E> useGenerator(JavaFieldInfo<T> propertyInfo, ValueGenerator<T> generator) {
        generators.put(propertyInfo.getName(), new FieldInfoGeneratorCombo(propertyInfo, generator));
        return this;
    }

    public <T> ValueGenerator<T> getValueGeneratorFor(JavaFieldInfo<T> propertyInfo) {
        return generators.get(propertyInfo.getName()).valueGenerator;
    }

    public List<E> generate(int count) {

        List<E> results = new ArrayList<E>(count);

        for (int i = 0; i < count; i++) {
            E entity = null;
            try {
                entity = entityType.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            for (String propName : generators.keySet()) {
                FieldInfoGeneratorCombo fieldInfoGeneratorCombo = generators.get(propName);
                ValueGenerator valueGenerator = fieldInfoGeneratorCombo.valueGenerator;
                Object value = valueGenerator.generateValue();
                fieldInfoGeneratorCombo.javaFieldInfo.setOn(entity, value);
            }

            results.add(entity);
        }

        return results;
    }

}
