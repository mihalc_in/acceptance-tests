package com.caparzo.acceptance.tests.core.testdatagenerator;

/**
 * Simple interface for any value generators; used in generation of sample/tests data.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public interface ValueGenerator<T> {

    T generateValue();
}
