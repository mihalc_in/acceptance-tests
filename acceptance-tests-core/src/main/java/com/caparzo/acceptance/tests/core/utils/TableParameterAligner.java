package com.caparzo.acceptance.tests.core.utils;

import org.apache.commons.lang.Validate;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Used to align table columns in story files.
 * User: stasyukd
 * Date: 01/10/13
 * Time: 13:23
 */
public class TableParameterAligner {

    public static void alignTable(File file) {

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {

            File tempFile = new File(file.getAbsolutePath() + ".temp");
            // delete temporary file if it already exists
            if (tempFile.exists()) {
                boolean deleted = tempFile.delete();
                Validate.isTrue(deleted);
            }
            boolean ceated = tempFile.createNewFile();
            Validate.isTrue(ceated);

            br = new BufferedReader(new FileReader(file));
            bw = new BufferedWriter(new FileWriter(tempFile));

            String line;
            List<String> tableLines = new ArrayList<String>();
            while ((line = br.readLine()) != null) {

                if (line.startsWith("|")) {
                    tableLines.add(line);
                } else {

                    if (!tableLines.isEmpty()) {
                        List<String> alignedLines = getAlignedTableLines(tableLines);
                        for (String alignedLine : alignedLines) {
                            bw.write(alignedLine);
                            bw.newLine();
                        }
                        tableLines.clear();
                    }

                    bw.write(line);
                    bw.newLine();
                }

            }

            // call process table again for cases when the last line in the story file was a table
            // line
            if (!tableLines.isEmpty()) {
                List<String> alignedLines = getAlignedTableLines(tableLines);
                for (String alignedLine : alignedLines) {
                    bw.write(alignedLine);
                    bw.newLine();
                }
                tableLines.clear();
            }

            br.close();
            bw.close();

            // replace original file
            boolean deleted = file.delete();
            Validate.isTrue(deleted, "Could not delete original file - " + file);
            boolean renamed = tempFile.renameTo(file);
            Validate.isTrue(renamed, "Could not rename file - " + tempFile);

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (bw != null)
                    bw.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

    }

    /**
     * Aligns the table parameter in the specified string. The input string is expected to have only
     * one table, if more than one is present then the behavior is unpredictable.
     *
     * @param inputString
     * @return
     */
    public static String alignTableInString(String inputString) {

        String[] lines = inputString.split("\n");
        List<String> alignedTableLines = getAlignedTableLines(Arrays.asList(lines));
        Validate.isTrue(lines.length == alignedTableLines.size(),
                "Aligned number of lines was different to that of non aligned lines");

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < alignedTableLines.size(); i++) {
            String alignedLine = alignedTableLines.get(i);
            sb.append(alignedLine);
            if (i != alignedTableLines.size() - 1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * @param tableLines
     * @return
     */
    public static List<String> getAlignedTableLines(List<String> tableLines) {

        List<String> alignedLines = new ArrayList<String>(tableLines.size());

        // work out the max column widths
        int[] maxColumnWidths = null;

        for (String line : tableLines) {

            if (line.startsWith("|--")) {
                // no need to do anything with a comment line
                continue;
            }

            if (!line.startsWith("|")) {
                /*
                 * ignore non table lines to support functionality provided by alignTableInString()
                 * method above
                 */
                continue;
            }

            line = fillEmptyValues(line); // for the split below to work correctly

            String[] split = line.split("\\|");
            split = Arrays.copyOfRange(split, 1, split.length); /*
                                                                 * remove the first element as it is
                                                                 * always empty
                                                                 */
            if (maxColumnWidths == null) {
                maxColumnWidths = new int[split.length];
            }

            for (int i = 0; i < split.length; i++) {
                int fieldLength = split[i].trim().length();
                int maxFieldLength = maxColumnWidths[i];
                if (fieldLength > maxFieldLength) {
                    maxColumnWidths[i] = fieldLength;
                }
            }

        }

        // align lines
        for (String line : tableLines) {

            if (line.startsWith("|--")) {
                // simply add comment line
                alignedLines.add(line);
                continue;
            }

            if (!line.startsWith("|")) {
                /*
                 * simply add non table lines to support functionality provided by
                 * alignTableInString() method above
                 */
                alignedLines.add(line);
                continue;
            }

            StringBuilder alignedLine = new StringBuilder();

            line = fillEmptyValues(line); // for the split below to work correctly

            String[] split = line.trim().split("\\|");
            split = Arrays.copyOfRange(split, 1, split.length); /*
                                                                 * remove the first element as it is
                                                                 * always empty
                                                                 */
            alignedLine.append("|"); // add it back to the aligned line

            for (int i = 0; i < split.length; i++) {
                String field = split[i].trim();
                int maxFieldLength = maxColumnWidths[i];
                int dif = maxFieldLength - field.length();
                if (dif > 0) {
                    char[] padChars = new char[dif];
                    Arrays.fill(padChars, ' ');
                    field = field + new String(padChars);
                }
                alignedLine.append(field);
                alignedLine.append("|");
            }

            alignedLines.add(alignedLine.toString());

        }

        return alignedLines;
    }


    private static String fillEmptyValues(String line) {

        int i = line.indexOf("||");

        while (i != -1) {
            line = line.substring(0, i + 1) + " " + line.substring(i + 1);
            i = line.indexOf("||");
        }

        return line;
    }
}
