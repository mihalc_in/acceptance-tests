package com.caparzo.acceptance.tests.core.jbehave;

import java.util.List;

/**
 * Thread local context used by the parameter converters.
 * User: stasyukd
 * Date: 09/10/13
 * Time: 12:14
 */
public class ConverterContext {

    public static ThreadLocal<ConverterContext> threadLocalContext = new ThreadLocal<ConverterContext>();

    private List<String> tabularParameterFieldNames;

    public List<String> getTabularParameterFieldNames() {
        return tabularParameterFieldNames;
    }

    public void setTabularParameterFieldNames(List<String> tabularParameterFieldNames) {
        this.tabularParameterFieldNames = tabularParameterFieldNames;
    }

    /**
     * Shorthand static method.
     *
     * @return
     */
    public static List<String> getFieldNames() {
        return getContext().getTabularParameterFieldNames();
    }

    public static ConverterContext getContext() {
        ConverterContext context = ConverterContext.threadLocalContext.get();
        if (context == null) {
            context = new ConverterContext();
            ConverterContext.threadLocalContext.set(context);
        }
        return context;
    }
}
