package com.caparzo.acceptance.tests.core.jbehave.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to mark story method parameters to not expand any variables inside them.
 * <p/>
 * TODO - with time this approach should not be needed, i.e. there should not be any raw formats like swift xml, BCS or
 * JSon should be used inside the story tests files.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface DoNotExpandVariables {
}
