package com.caparzo.acceptance.tests.core.utils;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import freemarker.template.utility.StringUtil;
import org.apache.commons.lang.Validate;
import org.javatuples.Pair;
import org.springframework.integration.json.ObjectToJsonTransformer;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Contains utility methods related to conversion.
 * User: stasyukd
 * Date: 05/09/13
 * Time: 10:08
 */
public class ConverterUtils {

    private static Map<Class<?>, ObjectToJsonTransformer> transformersByType = new HashMap<Class<?>, ObjectToJsonTransformer>();


    public static <T> String convertToJsonString(T o) {

        Validate.notNull(o, "Cannot convert null object");

        String text = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        try {
            StringWriter stringWriter = new StringWriter();
            objectMapper.writeValue(stringWriter, o);
            text = stringWriter.toString();
            return text;
        } catch (Exception e) {
            throw new RuntimeException("Exception converting object to Json string", e);
        }
//
//        Message<T> inputMsg = new GenericMessage<>(o);
//        Message<String> transformedMsg = (Message<String>) transformer.transform(o);
//        String payload = transformedMsg.getPayload();
//        return payload;
    }

    /**
     * Parses the string that contains data in CSV format, converts each line to object and returns them in List.
     * <p/>
     * It assumes:
     * * the first line contains field names
     * * all values are within quotes.
     * example:
     * ------
     * Hour        |Min        |Max
     * "2014112000"|"-45.43992"|"463.40710"
     * ------
     *
     * @param csv       String containing the CSV data
     * @param delimiter delimiter used in CSV (e.g. ",")
     * @param clazz     the class associated with objects to be parsed from CSV
     * @param <T>       type of object the CSV to be converted to
     * @return list of objects extracted from CSV string
     */
    public static <T> List<T> convertFromCsvToObjects(String csv, String delimiter, Class<T> clazz) {

        ArrayList<T> resultObjects = new ArrayList<>();

        String[] csvLines = csv.split("\n");

        String headerLine = csvLines[0];
        for (int i = 1; i < csvLines.length; i++) {

            T o = convertFromCsvToObject(headerLine, csvLines[i], clazz, delimiter);

            resultObjects.add(o);

        }

        return resultObjects;
    }

    /**
     * Is used to convert CSV with just one line of data
     *
     * @param headerLine String containing delimited CSV headers
     * @param valuesLine String containing delimited CSV values
     * @param clazz      class used to instantiate objects
     * @param delimiter  CSV delimiter
     * @param <T>        type of object to be extracted from CSV
     * @return the object extracted from CSV
     */
    public static <T> T convertFromCsvToObject(String headerLine, String valuesLine, Class<T> clazz, String delimiter) {
        //receive regex literal - save from cases where delimiter is regex special character (e.g. "|").
        String safeDelimiter = Pattern.quote(delimiter);

        String[] headers = headerLine.split(safeDelimiter);

        for (int i = 0; i < headers.length; i++) {
            headers[i] = Introspector.decapitalize(headers[i]);
        }

        T o;
        try {
            o = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Cannot create new instance of type " + clazz, e);
        }

        String[] values = getCsvValues(valuesLine, safeDelimiter);
        ReflectionUtils.setFieldValues(o, headers, values);

        return o;
    }


    /**
     * returns string array of CSV values with quotes trimmed off
     *
     * @param csvLine
     * @param safeDelimiter
     * @return
     */
    private static String[] getCsvValues(String csvLine, String safeDelimiter) {
        csvLine = StringUtil.replace(csvLine, "\"", "", true, false);

        return csvLine.split(safeDelimiter);

    }


    public static <T> Pair<String, String> convertToTabularFormatString(T o, Collection<String> fieldsToInclude) {
        return toTabularFormatString(o, fieldsToInclude);
    }

    public static <T> Pair<String, String> convertToTabularFormatString(T o) {
        Class<T> type = (Class<T>) o.getClass();
        List<String> allFields = ReflectionUtils.getAllPropertyNames(type);
        return toTabularFormatString(o, allFields);
    }

    /**
     * Returns a Pair instance where the first string is the header row and the second string is the value row of the
     * resulting table.
     *
     * @param o
     * @param fieldsToInclude
     * @return
     */
    private static <T> Pair<String, String> toTabularFormatString(T o, Collection<String> fieldsToInclude) {

        Validate.notNull(o);

        // must not be empty if present
        if (fieldsToInclude != null && fieldsToInclude.isEmpty()) {
            throw new IllegalArgumentException("Fields to include cannot be empty if specified");
        }

        Class<? extends Object> clazz = o.getClass();
        PropertyDescriptor[] pds = null;
        try {
            pds = Introspector.getBeanInfo(clazz, Object.class).getPropertyDescriptors();
        } catch (IntrospectionException e) {
            throw new AcceptanceTestException(e);
        }

        StringBuilder headerRow = new StringBuilder("|");
        StringBuilder valueRow = new StringBuilder("|");

        for (PropertyDescriptor pd : pds) {

            String propName = pd.getName();

            if (fieldsToInclude != null && !fieldsToInclude.contains(propName)) {
                // skip this field
                continue;
            }

            headerRow.append(propName);
            headerRow.append("|");

            Method readMethod = pd.getReadMethod();
            Object propValue = null;
            try {
                propValue = readMethod.invoke(o);
            } catch (IllegalAccessException e) {
                throw new AcceptanceTestException(e);
            } catch (InvocationTargetException e) {
                throw new AcceptanceTestException(e);
            } catch (NullPointerException e) {
                throw new AcceptanceTestException("Read method object for property '" + propName + "' is null!", e);
            }
            valueRow.append(propValue);
            valueRow.append("|");
        }

        return new Pair<String, String>(headerRow.toString(), valueRow.toString());
    }

    private ConverterUtils() {
        throw new IllegalStateException("Not intended for instantiation");
    }

}
