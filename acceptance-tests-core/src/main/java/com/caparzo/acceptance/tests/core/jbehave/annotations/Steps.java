package com.caparzo.acceptance.tests.core.jbehave.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specialized Spring bean annotation aimed at Classes which contains JBehave step method implementations.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Steps {
}
