package com.caparzo.acceptance.tests.core.utils;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.math3.util.Pair;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.jbehave.core.model.OutcomesTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Contains utility methods for performing comparisons of objects used in story tests.
 * Author: stasyukd
 * Author: mihalcip
 * Date: 23/08/13
 * Time: 14:39
 */
public class ComparisonUtils {
    private static Logger logger = LoggerFactory.getLogger(ComparisonUtils.class);

    private ComparisonUtils() {
        throw new IllegalArgumentException("Not intended for instantiation");
    }

    /**
     * Verifies if given 2 objects are the same using all properties
     *
     * @param expectedObject expected object to compare
     * @param actualObject   actual object to compare
     * @param <T>            compared objects must be of the same type
     */
    public static <T> void verifyEqual(T expectedObject, T actualObject) {
        Class<?> type = expectedObject.getClass();
        List<String> allFields = ReflectionUtils.getAllPropertyNames(type);
        verifyEqual(expectedObject, actualObject, allFields);
    }

    /**
     * Verifies if given 2 objects are the same using given properties
     *
     * @param expectedObject  expected object to compare
     * @param actualObject    actual object to compare
     * @param fieldsToCompare fields to use for comparison
     * @param <T>             compared objects must be of the same type
     */
    public static <T> void verifyEqual(T expectedObject, T actualObject, List<String> fieldsToCompare) {
        OutcomesTable outcomes = new OutcomesTable();
        Validate.isTrue(expectedObject.getClass() == actualObject.getClass(), "Compared objects must be of the same type, but were expected - "
                + expectedObject.getClass() + " actual - " + actualObject.getClass());

        Validate.notEmpty(fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(expectedObject.getClass(), fieldsToCompare);

        for (String field : fieldsToCompare) {
            PropertyDescriptor pd = ReflectionUtils.getPropertyDescriptorsForField(expectedObject.getClass(), field);
            final Method readMethod = pd.getReadMethod();

            Object expectedValue = null;
            Object actualValue = null;
            try {
                expectedValue = readMethod.invoke(expectedObject);
                actualValue = readMethod.invoke(actualObject);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }

//            special case which we do not want to compare using hamcrest matchers - expectedObject and actualObject are basically the same
            if (expectedValue == null && actualValue == null) {
                continue;
            }

            Class<?> propertyType = pd.getPropertyType();
            if (propertyType == BigDecimal.class) {
//                special closeTo matcher for BigDecimal
                BigDecimal expectedBD = (BigDecimal) expectedValue;
                BigDecimal actualBD = (BigDecimal) actualValue;
                Matcher<BigDecimal> bigDecimalMatcher = Matchers.closeTo(expectedBD, new BigDecimal("0.00"));
                outcomes.addOutcome("Checking field: " + field, actualBD, bigDecimalMatcher);
            } else if (propertyType == Date.class) {
                compareDates(outcomes, field, (Date) expectedValue, (Date) actualValue);
            } else {
                Matcher<Object> matcher = Matchers.equalTo(expectedValue);
                outcomes.addOutcome("Checking field: " + field, actualValue, matcher);
            }
        }
        outcomes.verify();
    }

    /**
     * Decides whether given 2 collections contain same objects using all properties
     *
     * @param expectedRecords collection of expected objects to compare
     * @param actualRecords   collection of actual objects to compare
     * @param <T>             compared collections must contain objects of the same type
     * @return true or false
     */
    public static <T> boolean hasSameRecords(Collection<? extends T> expectedRecords, Collection<? extends T> actualRecords) {

        if (expectedRecords.isEmpty()) {
            throw new IllegalArgumentException("Expected records collection parameter cannot be empty.");
        }

        Class<T> type = (Class<T>) expectedRecords.iterator().next().getClass();
        List<String> allFields = ReflectionUtils.getAllPropertyNames(type);

        try {
            verifyContainsSameRecords(expectedRecords, actualRecords, allFields);
            return true;
        } catch (AcceptanceTestException e) {
            return false;
        }
    }

    /**
     * Decides whether given 2 collections contain same objects using given properties
     *
     * @param expectedRecords collection of expected objects to compare
     * @param actualRecords   collection of actual objects to compare
     * @param <T>             compared collections must contain objects of the same type
     * @return true or false
     */
    public static <T> boolean hasSameRecords(List<T> expectedRecords, List<T> actualRecords, List<String> fieldsToCompare) {

        try {
            verifyContainSameRecords(expectedRecords, actualRecords, fieldsToCompare);
            return true;
        } catch (RuntimeException e) {
            return false;
        }

    }

    /**
     * Verifies if given 2 collections contain same objects using all properties
     * In case they don't, nicely formatted reason is provided
     *
     * @param expectedRecords collection of expected objects to compare
     * @param actualRecords   collection of actual objects to compare
     * @param <T>             compared collections must contain objects of the same type
     */
    public static <T> void verifyContainSameRecords(Collection<? extends T> expectedRecords, Collection<? extends T> actualRecords) {

        if (expectedRecords.isEmpty()) {
            throw new IllegalArgumentException("Expected records collection parameter cannot be empty.");
        }

        Class<T> type = (Class<T>) expectedRecords.iterator().next().getClass();
        List<String> allFields = ReflectionUtils.getAllPropertyNames(type);
        verifyContainsSameRecords(expectedRecords, actualRecords, allFields);

    }

    /**
     * Verifies if given 2 collections contain same objects using given properties
     * In case they don't, nicely formatted reason is provided
     *
     * @param expectedRecords collection of expected objects to compare
     * @param actualRecords   collection of actual objects to compare
     * @param fieldsToCompare fields to use for comparison
     * @param <T>             compared collections must contain objects of the same type
     */
    public static <T> void verifyContainSameRecords(Collection<? extends T> expectedRecords, Collection<? extends T> actualRecords, List<String> fieldsToCompare) {

        if (expectedRecords.isEmpty()) {
            throw new IllegalArgumentException("Expected records collection parameter cannot be empty.");
        }

        verifyContainsSameRecords(expectedRecords, actualRecords, fieldsToCompare);
    }

    private static <T> void verifyContainsSameRecords(Collection<? extends T> expectedRecords, Collection<? extends T> actualRecords, List<String> fieldsToCompare) {

        if (expectedRecords.size() != actualRecords.size()) {
            StringBuilder sb = new StringBuilder("Expected number of records was - " + expectedRecords.size() + " but actual was - " + actualRecords.size());
            String expectedRecordsTable = TableFormatUtils.asTableString(expectedRecords,
                    "A total of " + expectedRecords.size() + " records were expected with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(expectedRecordsTable);
            String actualRecordsTable = TableFormatUtils.asTableString(actualRecords,
                    "Actual number of records was " + actualRecords.size() + ", with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(actualRecordsTable);
            String errorMsg = sb.toString();
            throw new AcceptanceTestException(errorMsg);
        } else {

            // get subset of fields to ignore
            Class<T> type = (Class<T>) expectedRecords.iterator().next().getClass();
            List<String> fieldsToIgnore = ReflectionUtils.getAllPropertyNames(type);
            fieldsToIgnore.removeAll(fieldsToCompare);

            Collection<T> missingExpectedRecords = new ArrayList<T>();
            for (T expectedRecord : expectedRecords) {
                boolean recordFound = false;
                for (T actualRecord : actualRecords) {
                    /*
                     replaced EqualsBuilder.reflectionEquals with own implementation because of BigDecimal comparison issues
                     e.g. 500.00 equals 500 returns false
                     verifyEqualObjects method uses compareTo workaround instead
                     I keep EqualsBuilder.reflectionEquals in case there will be issues in the future
                    */
                    //***********************************************************************
                    //this block has been added
                    boolean same = true;
                    try {
                        verifyEqualObjects(expectedRecord, actualRecord, fieldsToCompare);
                    } catch (AcceptanceTestException e) {
                        same = false;
                    }
                    //***********************************************************************

                    // boolean same = EqualsBuilder.reflectionEquals(expectedRecord, actualRecord, fieldsToIgnore);
                    if (same) {
                        recordFound = true;
                        break;
                    }
                }
                if (!recordFound) {
                    missingExpectedRecords.add(expectedRecord);
                }
            }
            if (!missingExpectedRecords.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Actual records did not contain at least one of the expected records.\n");
                String missingRecordsTable = TableFormatUtils.asTableString(missingExpectedRecords, "Missing records with the following fields:", fieldsToCompare);
                sb.append(missingRecordsTable);
                sb.append("\n");
                String actualRecordsTable = TableFormatUtils.asTableString(actualRecords, "Actual records had the following fields:", fieldsToCompare);
                sb.append(actualRecordsTable);
                String msg = sb.toString();
                throw new AcceptanceTestException(msg);
            }

        }
    }

    private static void compareDates(OutcomesTable outcomes, String field, Date expectedDate, Date actualDate) {
        long expectedTime = expectedDate.getTime();
        long actualTime = actualDate.getTime();

        TimeZone timeZoneHere = TimeZone.getDefault();
        Long expectedTimeOffset = Long.valueOf(timeZoneHere.getOffset(expectedTime));
        Long actualTimeOffset = Long.valueOf(timeZoneHere.getOffset(actualTime));

        Long diffTimeOffsets = actualTimeOffset - expectedTimeOffset;
        Long diffTimes = actualTime - expectedTime;

        Matcher<Long> matcher = Matchers.equalTo(diffTimes);
        outcomes.addOutcome("Checking field: " + field, diffTimeOffsets, matcher);
    }

    public static <T, U> void verifyHasSameFieldsUsingIntrospection
            (T expectedObject, U actualObject, List<String> fieldsToCompare) {
        OutcomesTable outcomes = new OutcomesTable();

        Validate.notEmpty(fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(expectedObject.getClass(), fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(actualObject.getClass(), fieldsToCompare);

        for (String field : fieldsToCompare) {
            PropertyDescriptor pdExpectedObject = ReflectionUtils.getPropertyDescriptorsForField(expectedObject.getClass(), field);
            final Method readMethodExpectedObject = pdExpectedObject.getReadMethod();

            PropertyDescriptor pdActualObject = ReflectionUtils.getPropertyDescriptorsForField(actualObject.getClass(), field);
            final Method readMethodActualObject = pdActualObject.getReadMethod();

            Object expectedValue = null;
            Object actualValue = null;
            try {
                expectedValue = readMethodExpectedObject.invoke(expectedObject);
                actualValue = readMethodActualObject.invoke(actualObject);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }

//            special case which we do not want to compare using hamcrest matchers - expectedObject and actualObject are basically the same
            if (expectedValue == null && actualValue == null) {
                continue;
            }

            Class<?> propertyType = pdExpectedObject.getPropertyType();
            if (propertyType == BigDecimal.class) {
//                special closeTo matcher for BigDecimal
                BigDecimal expectedBD = (BigDecimal) expectedValue;
                BigDecimal actualBD = (BigDecimal) actualValue;
                Matcher<BigDecimal> bigDecimalMatcher = Matchers.closeTo(expectedBD, new BigDecimal("0.00"));
                outcomes.addOutcome("Checking field: " + field, actualBD, bigDecimalMatcher);
            } else if (propertyType == Date.class) {
                compareDates(outcomes, field, (Date) expectedValue, (Date) actualValue);
            } else {
                Matcher<Object> matcher = Matchers.equalTo(expectedValue);
                outcomes.addOutcome("Checking field: " + field, actualValue, matcher);
            }
        }
        outcomes.verify();
    }

    public static <T, U> void verifyHasSameFieldsUsingReflection(T expectedObject, U actualObject, List<String> fieldsToCompare) {
        OutcomesTable outcomes = new OutcomesTable();
        Validate.notEmpty(fieldsToCompare);

        Class<?> expectedObjectClass = expectedObject.getClass();
        Class<?> actualObjectClass = actualObject.getClass();

        ReflectionUtils.verifyHasPropertiesForFields(expectedObjectClass, fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(actualObjectClass, fieldsToCompare);

        for (String field : fieldsToCompare) {
            Field expectedField = null;
            Field actualField = null;

            try {
                expectedField = expectedObjectClass.getDeclaredField(field);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(expectedObjectClass + " does not contain field " + field);
            }

            try {
                actualField = actualObjectClass.getDeclaredField(field);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(actualObjectClass + " does not contain field " + field);
            }

            expectedField.setAccessible(true);
            actualField.setAccessible(true);

            Object expectedValue = null;
            Object actualValue = null;
            try {
                expectedValue = expectedField.get(expectedObject);
                actualValue = actualField.get(actualObject);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

//            special case which we do not want to compare using hamcrest matchers - expectedObject and actualObject are basically the same
            if (expectedValue == null && actualValue == null) {
                continue;
            }

            Class<?> propertyType = expectedField.getType();
            if (propertyType == BigDecimal.class) {
//                special closeTo matcher for BigDecimal
                BigDecimal expectedBD = (BigDecimal) expectedValue;
                BigDecimal actualBD = (BigDecimal) actualValue;
                Matcher<BigDecimal> bigDecimalMatcher = Matchers.closeTo(expectedBD, new BigDecimal("0.00"));
                outcomes.addOutcome("Checking field: " + field, actualBD, bigDecimalMatcher);
            } else if (propertyType == Date.class) {
                compareDates(outcomes, field, (Date) expectedValue, (Date) actualValue);
            } else {
                Matcher<Object> matcher = Matchers.equalTo(expectedValue);
                outcomes.addOutcome("Checking field: " + field, actualValue, matcher);
            }
        }
        outcomes.verify();
    }

    public static <T, U> void verifyContainsRecordsWithSameFields(Collection<? extends T> expectedRecords, Collection<? extends U> actualRecords, List<String> fieldsToCompare) {

        List<T> actualConvertedRecords = new ArrayList<>(actualRecords.size());

        for (U actualRecord : actualRecords) {
            Class<T> type = (Class<T>) expectedRecords.iterator().next().getClass();
            T newObject = ReflectionUtils.createNewObject(type);

            ReflectionUtils.copyPropertiesUsingReflection(newObject, actualRecord, fieldsToCompare);
            actualConvertedRecords.add(newObject);
        }

        if (expectedRecords.size() != actualConvertedRecords.size()) {
            StringBuilder sb = new StringBuilder("Expected number of records was - " + expectedRecords.size() + " but actual was - " + actualConvertedRecords.size());
            String expectedRecordsTable = TableFormatUtils.asTableString(expectedRecords,
                    "A total of " + expectedRecords.size() + " records were expected with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(expectedRecordsTable);
            String actualRecordsTable = TableFormatUtils.asTableString(actualConvertedRecords,
                    "Actual number of records was " + actualConvertedRecords.size() + ", with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(actualRecordsTable);
            String errorMsg = sb.toString();
            throw new AcceptanceTestException(errorMsg);
        } else {

            // get subset of fields to ignore
            Class<T> type = (Class<T>) expectedRecords.iterator().next().getClass();
            List<String> fieldsToIgnore = ReflectionUtils.getAllPropertyNames(type);
            fieldsToIgnore.removeAll(fieldsToCompare);

            Collection<T> missingExpectedRecords = new ArrayList<T>();
            for (T expectedRecord : expectedRecords) {
                boolean recordFound = false;
                for (T actualRecord : actualConvertedRecords) {
                    boolean same = EqualsBuilder.reflectionEquals(expectedRecord, actualRecord, fieldsToIgnore);
                    if (same) {
                        recordFound = true;
                        break;
                    }
                }
                if (!recordFound) {
                    missingExpectedRecords.add(expectedRecord);
                }
            }
            if (!missingExpectedRecords.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Actual records did not contain at least one of the expected records.\n");
                String missingRecordsTable = TableFormatUtils.asTableString(missingExpectedRecords, "Missing records with the following fields:", fieldsToCompare);
                sb.append(missingRecordsTable);
                sb.append("\n");
                String actualRecordsTable = TableFormatUtils.asTableString(actualConvertedRecords, "Actual records had the following fields:", fieldsToCompare);
                sb.append(actualRecordsTable);
                String msg = sb.toString();
                throw new AcceptanceTestException(msg);
            }
        }
    }

    public static boolean nullSafeEquals(Object object1, Object object2) {
        if (object1 == null && object2 == null) {
            return true;
        }

        //empty string vs null case
        if (object1 instanceof String && object2 == null) {
            String object1String = (String) object1;
            if (object1String.length() == 0) {
                return true;
            }
        }

        //null vs empty string case
        if (object1 == null && object2 instanceof String) {
            String object2String = (String) object2;
            if (object2String.length() == 0) {
                return true;
            }
        }

        if (object1 == null && object2 != null) {
            return false;
        }

        if (object1 != null && object2 == null) {
            return false;
        }

        if (object1 instanceof BigDecimal && object2 instanceof BigDecimal) {
            BigDecimal object1BD = (BigDecimal) object1;
            BigDecimal object2BD = (BigDecimal) object2;
            return object1BD.compareTo(object2BD) == 0;
        }

        if ((object1 instanceof Timestamp || object1 instanceof Date) && (object2 instanceof Timestamp || object2 instanceof Date)) {
            Date object1TS = (Date) object1;
            Date object2TS = (Date) object2;

            long time1 = object1TS.getTime();
            long time2 = object2TS.getTime();

            TimeZone timeZoneHere = TimeZone.getDefault();
            Long rawOffset = Long.valueOf(timeZoneHere.getOffset(time2));
            Long diff = time1 - time2;
            diff = Math.abs(diff);

            return rawOffset.equals(diff);
        }

        return object1.equals(object2);
    }

    public static <T> void verifyEqualObjects(T expected, T actual, List<String> fieldsToCompare) {

        Validate.isTrue(expected.getClass() == actual.getClass(), "Compared objects must be of the same type, but were expected - "
                + expected.getClass() + " actual - " + actual.getClass());

        Validate.notEmpty(fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(expected.getClass(), fieldsToCompare);

        Map<String, Pair> unequalFields = new HashMap<String, Pair>();

        for (String field : fieldsToCompare) {

            PropertyDescriptor pd = ReflectionUtils.getPropertyDescriptorsForField(expected.getClass(), field);
            Method readMethod = pd.getReadMethod();

            Object expectedValue = null;
            Object actualValue = null;

            try {
                expectedValue = readMethod.invoke(expected);
                actualValue = readMethod.invoke(actual);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }

            if (actualValue == null && expectedValue == null) {
                //we skip from comparison this case
                continue;

            } else if (expectedValue != null && actualValue != null) {
                Class<?> propertyType = pd.getPropertyType();
                if (propertyType == BigDecimal.class) {
                    // use compareTo for BigDecimal type to ignore scale in comparison
                    BigDecimal expectedBD = (BigDecimal) expectedValue;
                    BigDecimal actualBD = (BigDecimal) actualValue;
                    if (expectedBD.compareTo(actualBD) != 0) {
                        Pair<Object, Object> pair = new Pair<>(expectedValue, actualValue);
                        unequalFields.put(field, pair);
                    }
                } else {
                    if (!expectedValue.equals(actualValue)) {
                        Pair<Object, Object> pair = new Pair<>(expectedValue, actualValue);
                        unequalFields.put(field, pair);
                    }
                }

            } else {
                Pair<Object, Object> pair = new Pair<>(expectedValue, actualValue);
                unequalFields.put(field, pair);
            }

        }

        if (unequalFields.isEmpty()) {
            // the compared fields were all equal
        } else {
            // need to throw exception
            StringBuilder sb = new StringBuilder("The compared objects were not equal. The following fields were different: ");
            Set<String> fieldNames = unequalFields.keySet();
            int i = 0;
            for (String fieldName : fieldNames) {
                Pair pair = unequalFields.get(fieldName);
                Object expectedValue = pair.getFirst();
                Object actualValue = pair.getSecond();
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append("[field name - '" + fieldName + "' expected - '" + expectedValue + "' but was - '" + actualValue + "']");
                i++;
            }
            throw new AcceptanceTestException(sb.toString());
        }
    }

    /**
     * Method to verify whether one collection of objects (superset) contains other collection of objects (expectedSubset)
     * Note: for higher efficiency use your objects equals method (by setting useEquals=true) and re-write you objects equals method
     *
     * @param isEqualsAllowed indicator whether to use class' equals or ReflectionUtils.isEqual to compare objects
     * @param superset        the collection of objects where search for expected objects
     * @param expectedSubset
     * @param <T>
     */
    public static <T> void verifyIsSubset(Collection<T> superset, Collection<T> expectedSubset, final boolean isEqualsAllowed, List<String> fieldsToCompare) {
        List<T> actualObjects = new ArrayList<>();

        for (T expectedObject : expectedSubset) {
            for (T supersetObject : superset) {
                if (isEqualsAllowed) {
                    if (expectedObject.equals(supersetObject)) {
                        actualObjects.add(expectedObject);
                    }
                } else {
                    if (isEqual(expectedObject, supersetObject, fieldsToCompare)) {
                        actualObjects.add(expectedObject);
                    }
                }
            }
        }

        if (actualObjects.size() != expectedSubset.size()) {
            Class<T> type = (Class<T>) expectedSubset.iterator().next().getClass();

            StringBuilder sb = new StringBuilder("Expected number of records was - " + expectedSubset.size() + " but actual was - " + actualObjects.size());
            String expectedRecordsTable = TableFormatUtils.asTableString(expectedSubset,
                    "A total of " + expectedSubset.size() + " records were expected with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(expectedRecordsTable);
            String actualRecordsTable = TableFormatUtils.asTableString(actualObjects,
                    "Actual number of records was " + actualObjects.size() + ", with the following fields:", fieldsToCompare);
            sb.append("\n");
            sb.append(actualRecordsTable);
            String errorMsg = sb.toString();

            throw new AcceptanceTestException(errorMsg);
        }

    }

    private static <T> boolean isEqual(T object1, T object2) {
        return isEqual(object1, object2, ReflectionUtils.getAllPropertyNames(object1.getClass()));
    }

    private static <T> boolean isEqual(T object1, T object2, List<String> fieldsToCompare) {

        Validate.notEmpty(fieldsToCompare);
        ReflectionUtils.verifyHasPropertiesForFields(object2.getClass(), fieldsToCompare);

        if (object1.getClass() != object2.getClass()) {
            return false;
        }

        PropertyDescriptor[] pds = ReflectionUtils.getPropertyDescriptorFiltered(object1.getClass(), fieldsToCompare);
        for (PropertyDescriptor pd : pds) {
            Method readMethod = pd.getReadMethod();
            Object value1, value2;

            try {
                value1 = readMethod.invoke(object1);
                value2 = readMethod.invoke(object2);
            } catch (InvocationTargetException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            if (!value1.equals(value2)) {
                return false;
            }
        }

        return true;
    }
}
