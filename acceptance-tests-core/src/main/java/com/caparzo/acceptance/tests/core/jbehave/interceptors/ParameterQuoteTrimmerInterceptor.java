package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Used to trim quotes around parameter values before they are passed to the parameter converters e.g. '10' will be
 * converted to simply 10 by this interceptor.
 * User: stasyukd
 * Date: 30/08/13
 * Time: 15:29
 */
@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 11)
public class ParameterQuoteTrimmerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution(public * convertValue(..))")
    public void convertValueMethod() {
    }

    @Pointcut("target(org.jbehave.core.steps.ParameterConverters.ParameterConverter)")
    public void typeParameterConverter() {
    }

    @Around("convertValueMethod() && typeParameterConverter()")
    public Object interceptTransformer(ProceedingJoinPoint pjp) throws Throwable {

        Object[] args = pjp.getArgs();
        logger.trace("Trimming quotes for: " + StringUtils.join(args, ","));
        trimQuotes(args);

        // continue with expanded args
        return pjp.proceed(args);
    }

    private void trimQuotes(Object[] args) {
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof String) {
                String trimmedValue = trimQuotes(((String) arg).trim());
                args[i] = trimmedValue;
            }
        }
    }

    private String trimQuotes(String arg) {
        if (arg.startsWith("'") && arg.endsWith("'")) {
            return getTrimmed(arg);
        } else if (arg.startsWith("\"") && arg.endsWith("\"")) {
            return getTrimmed(arg);
        } else {
            return arg;
        }
    }

    private String getTrimmed(String arg) {
        if (arg.length() == 2) {
            // intention here is to have empty parameter value i.e. '' or "" results in empty string
            return "";
        } else {
            String trimmed = arg.substring(1, arg.length() - 1);
            return trimmed;
        }
    }


}
