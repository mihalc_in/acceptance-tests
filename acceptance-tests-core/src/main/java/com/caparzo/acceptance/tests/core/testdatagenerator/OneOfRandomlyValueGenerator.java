package com.caparzo.acceptance.tests.core.testdatagenerator;

import org.apache.commons.lang.Validate;

import java.util.List;
import java.util.Random;

/**
 * Choose one value from the specified list randomly.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class OneOfRandomlyValueGenerator<T> implements ValueGenerator<T> {

    private final Random random = new Random();

    private final List<T> possibleValues;

    public OneOfRandomlyValueGenerator(List<T> possibleValues) {
        this.possibleValues = possibleValues;
    }

    @Override
    public T generateValue() {
        Validate.notEmpty(possibleValues);
        int i = random.nextInt(possibleValues.size());
        return possibleValues.get(i);
    }

}
