package com.caparzo.acceptance.tests.core.jbehave;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Used to store scenario execution context as thread local variables. This context will be reset before scenario start.
 * User: stasyukd
 * Date: 30/08/13
 * Time: 14:31
 */
public class ScenarioExecutionContext extends ThreadLocal<ScenarioExecutionContext> {

    public static final ScenarioExecutionContext CONTEXT = new ScenarioExecutionContext();

    private static final Logger log = LoggerFactory.getLogger(ScenarioExecutionContext.class);

    private Map<String, Object> contextVariables = new HashMap<>();

    private List<?> cleanupList = new ArrayList<>();

    private List<String> msgTags = new ArrayList<>();

    private List<Tuple> requests = new ArrayList<>();

    private List<Tuple> responses = new ArrayList<>();

    private List<String> restResponses = new ArrayList<>();

    private List<Object> genericObjects = new ArrayList<>();

    public static void addRequest(String action, String requestPayload) {
        CONTEXT.get().getRequests().add(new Tuple(action, requestPayload));
    }

    public static void addResponse(String action, String responsePayload) {
        CONTEXT.get().getResponses().add(new Tuple(action, responsePayload));
    }

    public static void addRestResponse(String restResponsePayload) {
        CONTEXT.get().getRestResponses().add(restResponsePayload);
    }

    public static void addGenenericObject(Object object) {
        CONTEXT.get().getGenericObjects().add(object);
    }

    public static Object getLastGenericObject() {
        List<Object> genericObjects = CONTEXT.get().getGenericObjects();

        if (genericObjects.size() == 0) {
            return null;
        }
        return genericObjects.get(genericObjects.size() - 1);
    }

    public static Object getAllGenericObjects() {
        List<Object> genericObjects = CONTEXT.get().getGenericObjects();
        return genericObjects;
    }

    public static String getLastRequest() {
        List<Tuple> requests = CONTEXT.get().getRequests();
        return requests.get(requests.size() - 1).payload;
    }


    public static String getLastResponse() {
        List<Tuple> responses = CONTEXT.get().getResponses();
        return responses.get(responses.size() - 1).payload;
    }

    public static String getLastRestResponse() {
        List<String> restResponses = CONTEXT.get().getRestResponses();
        return restResponses.get(restResponses.size() - 1);
    }

    public static List<String> getAllResponses() {
        List<Tuple> responses = CONTEXT.get().getResponses();
        List<String> allResponses = new ArrayList<>();
        for (Tuple response : responses) {
            allResponses.add(response.payload);
        }
        return allResponses;
    }

    public static List<String> getAllRequests() {
        List<Tuple> requests = CONTEXT.get().getRequests();
        List<String> allRequests = new ArrayList<>();
        for (Tuple response : requests) {
            allRequests.add(response.payload);
        }
        return allRequests;
    }


    private List<Tuple> getRequests() {
        return requests;
    }

    private List<Tuple> getResponses() {
        return responses;
    }

    private List<String> getRestResponses() {
        return restResponses;
    }

    private List<Object> getGenericObjects() {
        return genericObjects;
    }

    public List<String> getMsgTags() {
        return msgTags;
    }

    public Map<String, Object> getContextVariables() {
        return contextVariables;
    }

    public List<?> getCleanupList() {
        return cleanupList;
    }

    public static void saveRecordToCleanupList(Object recordToBeDeleted) {
        List cleanupList = ScenarioExecutionContext.CONTEXT.get().getCleanupList();
        cleanupList.add(recordToBeDeleted);
    }

    public static void saveRecordsToCleanupList(List<?> recordsToBeDeleted) {
        List cleanupList = ScenarioExecutionContext.CONTEXT.get().getCleanupList();
        cleanupList.addAll(recordsToBeDeleted);
    }

    private static class Tuple {

        final String actionName;

        final String payload;

        private Tuple(String actionName, String payload) {
            this.actionName = actionName;
            this.payload = payload;
        }

    }

    /**
     * Shorthand static methods.
     */
    public static Map<String, Object> getContextVars() {
        return getContext().getContextVariables();
    }

    public static List<String> getMessageTags() {
        return getContext().getMsgTags();
    }

    public static ScenarioExecutionContext getContext() {
        ScenarioExecutionContext context = ScenarioExecutionContext.CONTEXT.get();
        if (context == null) {
            context = new ScenarioExecutionContext();
            ScenarioExecutionContext.CONTEXT.set(context);
        }
        return context;
    }
}
