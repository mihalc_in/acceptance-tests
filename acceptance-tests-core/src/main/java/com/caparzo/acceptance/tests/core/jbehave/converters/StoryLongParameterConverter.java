package com.caparzo.acceptance.tests.core.jbehave.converters;

import org.jbehave.core.steps.ParameterConverters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

/**
 * JBehave parameter converter for string values.
 * User: stasyukd
 * Date: 06/08/13
 * Time: 13:30
 */
@Component
public class StoryLongParameterConverter implements ParameterConverters.ParameterConverter {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean accept(Type type) {
        if (type == Long.class) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object convertValue(String value, Type type) {
        logger.trace("Converting value: " + value + " to type: " + type);
        return Long.valueOf(value);
    }
}
