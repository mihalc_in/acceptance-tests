package com.caparzo.acceptance.tests.core.testdatagenerator;

import org.apache.commons.math3.random.RandomDataGenerator;

import java.math.BigDecimal;

/**
 * Generator of {@code BigDecimal} with specified range and number of decimal places.
 * Default values are:
 * <ul>
 * <li>lowerBound = 0</li>
 * <li>upperBound = 10000</li>
 * <li>numDecimalPlaces = 3</li>
 * </ul>
 *
 * @author pastelio
 */
public class RandomBigDecimalValueGenerator implements ValueGenerator<BigDecimal> {

    private final long lowerBound;
    private final long upperBound;
    private final int numDecimalPlaces;
    private final RandomDataGenerator generator = new RandomDataGenerator();

    /**
     * Creates {@code RandomBigDecimalValueGenerator} with default bounds and decimal places.
     */
    public RandomBigDecimalValueGenerator() {
        lowerBound = 0L;
        upperBound = 10000L;
        numDecimalPlaces = 3;
    }

    /**
     * Creates {@code RandomBigDecimalValueGenerator} with specified bounds and default decimal places.
     *
     * @param lowerBound lower bound of the generated values
     * @param upperBound upper bound of the generated values
     */
    public RandomBigDecimalValueGenerator(long lowerBound, long upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        numDecimalPlaces = 3;
    }

    /**
     * Creates {@code RandomBigDecimalValueGenerator} with specified bounds and number of decimal places.
     *
     * @param lowerBound       lower bound of the generated values
     * @param upperBound       upper bound of the generated values
     * @param numDecimalPlaces number of decimal places
     */
    public RandomBigDecimalValueGenerator(long lowerBound, long upperBound, int numDecimalPlaces) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.numDecimalPlaces = numDecimalPlaces;
    }

    /**
     * Generates {@code BigDecimal}.
     *
     * @return
     */
    @Override
    public BigDecimal generateValue() {
        long integerPart = generator.nextLong(lowerBound, upperBound);
        long decimalPart = generator.nextLong(0, (long) Math.pow(10, (double) numDecimalPlaces));
        BigDecimal number1 = new BigDecimal(integerPart * Math.pow(10, numDecimalPlaces) + decimalPart);
        BigDecimal number2 = number1.movePointLeft(numDecimalPlaces);
//        return (T) new BigDecimal(integerPart*Math.pow(10, numDecimalPlaces) + decimalPart).setScale(numDecimalPlaces);
        return number2;
    }
}
