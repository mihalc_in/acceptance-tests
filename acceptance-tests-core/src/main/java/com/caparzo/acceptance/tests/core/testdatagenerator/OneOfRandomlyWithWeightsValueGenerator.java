package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.util.List;
import java.util.Random;

/**
 * Randomly generates values from the supplied list, using the supplied weights.
 *
 * @author pastelio
 */
public class OneOfRandomlyWithWeightsValueGenerator<T> implements ValueGenerator<T> {

    private final Random random = new Random();

    private final List<T> possibleValues;
    private final int[] weights;
    private final int weightSum;

    /**
     * Creates new {@code OneOfRandomlyWithWeightsValueGenerator} with provided values and weights.
     *
     * @param possibleValues values that can be generated
     * @param weights        weights of the values
     */
    public OneOfRandomlyWithWeightsValueGenerator(List<T> possibleValues, int[] weights) {
        if (possibleValues.size() != weights.length || possibleValues.isEmpty() || weights.length == 0) {
            throw new IllegalArgumentException("Number of weights must be the same as the number of possible values, also must be at least one.");
        }
        this.possibleValues = possibleValues;
        this.weights = weights;
        int sum = 0;
        for (int weight : weights) {
            if (weight < 1) {
                throw new IllegalArgumentException("All weights must be at least equal to one.");
            }
            sum += weight;
        }
        weightSum = sum;
    }

    /**
     * return a random value from the list of values, taking weights into account.
     *
     * @return generated value
     */
    @Override
    public T generateValue() {
        int randomNumber = random.nextInt(weightSum);
        int rsum = 0;
        int index = 0;
        for (int weight : weights) {
            rsum += weight;
            if (randomNumber < rsum) {
                break;
            } else {
                ++index;
            }
        }
        return possibleValues.get(index);
    }
}
