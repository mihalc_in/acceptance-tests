package com.caparzo.acceptance.tests.core.utils;


import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains utility methods which work via java reflection.
 * User: stasyukd
 * Date: 10/09/13
 * Time: 11:48
 */
public class ReflectionUtils {

    private static final String patternValue = "TO_DATE\\(([^)]+)\\)";

    private ReflectionUtils() {
        throw new IllegalArgumentException("Not intended for instantiation");
    }

    public static <T> void truncateTimeOnDateFields(T cashflowProjectionDecorator) {
        truncateTimeOnFields(cashflowProjectionDecorator, null);
    }

    /**
     * @param cashflowProjectionDecorator
     * @param fieldNames                  - list of fields which to truncate
     * @param <T>
     */
    public static <T> void truncateTimeOnDateFields(T cashflowProjectionDecorator, String... fieldNames) {
        // the api method cannot except empty or null fieldNames parameter
        Validate.notEmpty(fieldNames);
        List<String> stringFieldNames = Arrays.asList(fieldNames);
        truncateTimeOnFields(cashflowProjectionDecorator, stringFieldNames);
    }

    /**
     * Returns an array of property descriptors obtained by introspecting the specified class.
     *
     * @param clazz
     * @return
     */
    public static PropertyDescriptor[] getPropertyDescriptors(Class clazz) {
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(clazz, Object.class);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        return beanInfo.getPropertyDescriptors();
    }

    public static PropertyDescriptor getPropertyDescriptorsForField(Class clazz, String fieldName) {

        PropertyDescriptor pd = findPropertyDescriptorsForField(clazz, fieldName);
        if (pd != null) {
            return pd;
        } else {
            throw new RuntimeException("Could not find property descriptor for field name - " + fieldName + " in class - " + clazz);
        }

    }

    /**
     * Same as getPropertyDescriptorsForField but returns null instead of throwing an exception.
     *
     * @param clazz
     * @param fieldName
     * @return
     */
    public static PropertyDescriptor findPropertyDescriptorsForField(Class clazz, String fieldName) {

        PropertyDescriptor[] pds = getPropertyDescriptors(clazz);

        for (PropertyDescriptor pd : pds) {
            String actualName = pd.getName();
            if (actualName.equals(fieldName)) {
                return pd;
            }
        }
        return null;
    }

    private static <T> void truncateTimeOnFields(T cashflowProjectionDecorator, List<String> fieldNames) {
        PropertyDescriptor[] pds;
        try {
            pds = Introspector.getBeanInfo(cashflowProjectionDecorator.getClass()).getPropertyDescriptors();
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        for (PropertyDescriptor pd : pds) {

            String propertyName = pd.getName();
            if (fieldNames != null) {
                boolean contains = fieldNames.contains(propertyName);
                if (!contains) {
                    continue;
                } else {
                    // try the next field
                }
            }

            Class<?> propertyType = pd.getPropertyType();
            Method readMethod = pd.getReadMethod();
            Date beforeValue;
            try {
                // must cast to Date here otherwise an invalid property is specified
                beforeValue = (Date) readMethod.invoke(cashflowProjectionDecorator);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }

            // we do not try to truncate any null date values
            if (beforeValue == null) {
                continue;
            }

            Calendar beforeValueCal = Calendar.getInstance();
            beforeValueCal.setTime(beforeValue);
            beforeValueCal.set(Calendar.HOUR_OF_DAY, 0);
            beforeValueCal.set(Calendar.MINUTE, 0);
            beforeValueCal.set(Calendar.SECOND, 0);
            beforeValueCal.set(Calendar.MILLISECOND, 0);

            Date afterValue = beforeValueCal.getTime();
            Method writeMethod = pd.getWriteMethod();
            try {
                writeMethod.invoke(cashflowProjectionDecorator, afterValue);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void copyProperties(Object target, Object source) {

        Class<? extends Object> targetClass = target.getClass();
        Validate.isTrue(source.getClass().isAssignableFrom(targetClass));

        BeanInfo bi;
        try {
            bi = Introspector.getBeanInfo(targetClass, Object.class);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }

        PropertyDescriptor[] pds = bi.getPropertyDescriptors();
        for (PropertyDescriptor pd : pds) {
            Method readMethod = pd.getReadMethod();
            Method writeMethod = pd.getWriteMethod();
            try {
                Object sourceValue = readMethod.invoke(source);
                if (sourceValue != null) {
                    writeMethod.invoke(target, sourceValue);
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Uses reflection rather than bean introspection API to copy properties. As the result getters and setters need not be implemented in
     * the copy targets. Only properties that are not null will be copied.
     *
     * @param target
     * @param source
     */
    public static <T> void copyNonNullFields(T source, T target) {

        Class<? extends Object> targetClass = target.getClass();

        Field[] fields = targetClass.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                if (Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                Object sourceValue = field.get(source);
                if (sourceValue != null) {
                    field.set(target, sourceValue);
                }
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Error occurred while trying to copy object fields.", e);
        }

    }

    public static void copyPropertiesUsingReflection(Object target, Object source, List<String> fieldNames) {
        Class<? extends Object> targetClass = target.getClass();
        Class<? extends Object> sourceClass = source.getClass();

        for (String fieldName : fieldNames) {
            Field targetField = null;
            Field sourceField = null;

            try {
                targetField = targetClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(targetClass + " does not contain field " + fieldName);
            }

            try {
                sourceField = sourceClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(sourceClass + " does not contain field " + fieldName);
            }

            targetField.setAccessible(true);
            sourceField.setAccessible(true);

            Object sourceValue = null;
            try {
                sourceValue = sourceField.get(source);
                targetField.set(target, sourceValue);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Will populate any mandatory fields for the given type with random values before setting any values provided in
     * fieldValues parameter.
     *
     * @param clazz
     * @return
     */
    public static <T> T createNewObject(Class<T> clazz) {
        T o = null;
        try {

            Constructor<T> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            o = constructor.newInstance();

        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return o;
    }

    public static void setFieldValues(Object instance, String[] fieldNames, String[] fieldValues) {
        BeanInfo beanInfo = null;
        Class<?> type = instance.getClass();
        try {
            beanInfo = Introspector.getBeanInfo(type);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < fieldNames.length; i++) {
            String fieldName = fieldNames[i];
            String fieldValue = fieldValues[i];
            // find the property descriptor for this fieldName
            PropertyDescriptor matchingPd = null;
            for (PropertyDescriptor pd : pds) {
                String propertyName = pd.getName();
                if (propertyName.equals(fieldName)) {
                    matchingPd = pd;
                    break;
                }
            }
            Validate.notNull(matchingPd, "Could not find a property descriptor for field name - " + fieldName + " in class - " + type);
            Method writeMethod = matchingPd.getWriteMethod();
            Validate.notNull(writeMethod, "Could not find a write method for field name - " + fieldName + " in class - " + type);
            Class<?> paramType = getParameterType(writeMethod);
            Object fieldValueTyped;
            try {
                fieldValueTyped = convertToType(fieldValue, paramType);
            } catch (RuntimeException ex) {
                throw new RuntimeException("Exception occurred while trying to covert field value to expected type, fieldName - " + fieldName + ", fieldValue - " + fieldValue + ", expectedType - " + paramType + ".", ex);
            }
            try {
                writeMethod.invoke(instance, fieldValueTyped);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static Object[] getFieldValues(Object instance, String[] fieldNames) {

        Object[] fieldValues = new Object[fieldNames.length];

        BeanInfo beanInfo = null;
        Class<?> type = instance.getClass();
        try {
            beanInfo = Introspector.getBeanInfo(type);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < fieldNames.length; i++) {
            String fieldName = fieldNames[i];

            // find the property descriptor for this fieldName
            PropertyDescriptor matchingPd = null;
            for (PropertyDescriptor pd : pds) {
                String propertyName = pd.getName();
                if (propertyName.equals(fieldName)) {
                    matchingPd = pd;
                    break;
                }
            }
            Validate.notNull(matchingPd, "Could not find a property descriptor for field name - " + fieldName + " in class - " + type);
            Method readMethod = matchingPd.getReadMethod();
            Validate.notNull(readMethod, "Could not find a read method for field name - " + fieldName + " in class - " + type);
            Class<?> returnType = readMethod.getReturnType();

            try {
                Object o = readMethod.invoke(instance);
                fieldValues[i] = o;
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
        return fieldValues;
    }

    public static Field getField(Class type, String fieldName) {

        try {
            Field field = type.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(type + " does not contain field " + fieldName, e);
        }
    }

    public static Object getFieldValue(Object instance, String fieldName) {

        try {
            Field field = getField(instance.getClass(), fieldName);
            Object value = field.get(instance);
            return value;
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Error occurred while trying to read field " + fieldName + " of instance - " + instance, e);
        }
    }

    public static List<Field> getFields(Class type, List<String> fieldNames) {

        List<Field> fields = new ArrayList<>();

        for (String fieldName : fieldNames) {
            try {
                Field field = type.getDeclaredField(fieldName);
                field.setAccessible(true);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(type + " does not contain field " + fieldName, e);
            }
        }

        return fields;
    }

    private static Class<?> getParameterType(Method writeMethod) {

        Class<?>[] methodParamTypes = writeMethod.getParameterTypes();
        Validate.isTrue(methodParamTypes.length == 1, "Write method - " + writeMethod + " must accept only one parameter");

        Class<?> paramType = methodParamTypes[0];

        // check if object, and if so then check if it is generic parameter type
//        if (paramType == Object.class) {
//            Type[] genericParameterTypes = writeMethod.getGenericParameterTypes();
//            TypeVariable<Method>[] typeParameters = writeMethod.getTypeParameters();
//            if (genericParameterTypes.length > 0) {
//                TypeVariableImpl genericParameterType = (TypeVariableImpl) genericParameterTypes[0];
//                Class genericDeclaration = (Class) genericParameterType.getGenericDeclaration();
//                paramType = genericDeclaration;
//            }
//        }

        return paramType;
    }

    public static Type getListType(Type type) {
        ParameterizedType pt = ((ParameterizedType) type);
        Type[] genericTypes = pt.getActualTypeArguments();
        Validate.isTrue(genericTypes.length == 1);
        Type t = genericTypes[0];
        return t;
    }

    public static boolean isListType(Type type) {
        if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            Type rawT = pt.getRawType();
            return rawT.equals(List.class);
        } else {
            return false;
        }
    }

    public static <T> T convertToType(String fieldValue, Class<T> paramType) {

        // support for null values
        if (fieldValue.equals("null")) {
            return null;
        } else if (paramType == String.class) {
            return (T) fieldValue;
        } else if (paramType == boolean.class || paramType == Boolean.class) {
            Boolean typedValue = Boolean.valueOf(fieldValue);
            return (T) typedValue;
        } else if (paramType == int.class || paramType == Integer.class) {
            Integer typedValue = Integer.valueOf(fieldValue);
            return (T) typedValue;
        } else if (paramType == long.class || paramType == Long.class) {
            Long typedValue = Long.parseLong(fieldValue);
            return (T) typedValue;
        } else if (paramType == double.class || paramType == Double.class) {
            Double typedValue = Double.parseDouble(fieldValue);
            return (T) typedValue;
        } else if (paramType == BigDecimal.class) {
            BigDecimal typedValue = null;
            try {
                typedValue = new BigDecimal(fieldValue);
            } catch (NumberFormatException e) {
                throw new RuntimeException("Could not create BigDecimal value from string - " + fieldValue, e);
            }
            return (T) typedValue;
        } else if (paramType == float.class || paramType == Float.class) {
            Float typedValue = Float.parseFloat(fieldValue);
            return (T) typedValue;
        } else if (paramType == Date.class) {

//            special workaround for dates which user wants to format in given format and/or timezone
//            e.g. the user can specify TO_DATE(2014-05-16 10:15, yyyy-MM-dd HH:mm) or TO_DATE(2014-05-16 10:15, yyyy-MM-dd HH:mm, UTC)

            Pattern pattern = Pattern.compile(patternValue);
            Matcher matcher = pattern.matcher(fieldValue);

            String dateValue = "";
            String dateFormat = "";
            String timezoneId = "";
            boolean matched = false;

            if (matcher.matches()) {
                matched = true;

                String value = matcher.group(1); // params of TO_DATE function
                String[] params = value.split(",");

                if (params.length == 2) {
                    dateValue = params[0].trim();
                    dateFormat = params[1].trim();
                    timezoneId = TimeZone.getDefault().getID();
                } else if (params.length == 3) {
                    dateValue = params[0].trim();
                    dateFormat = params[1].trim();
                    timezoneId = params[2].trim();
                }
            }

            Date typedValue;
            if (matched) {

                typedValue = parseDate(dateValue, dateFormat, timezoneId);

            } else {
                String[] fieldValues = StringUtils.split(fieldValue, " ");
                if (fieldValues.length == 2) {  //example: "2015-03-31 21:00:00"
                    if (StringUtils.split(fieldValues[1], ":").length == 3) { //HH:mm:ss

                        typedValue = parseDate(fieldValue, "yyyy-MM-dd HH:mm:ss", TimeZone.getDefault().getID());

                    } else if (StringUtils.split(fieldValues[1], ":").length == 2) { //without seconds (HH:mm)

                        typedValue = parseDate(fieldValue, "yyyy-MM-dd HH:mm", TimeZone.getDefault().getID());

                    } else {

                        throw new AcceptanceTestException("Wrong format of date - " + fieldValue + ". It should be in format \"yyyy-MM-dd HH:mm:ss\", \"yyyy-MM-dd HH:mm\", \"yyyy-MM-dd\"");
                    }
                } else {
                    typedValue = parseDate(fieldValue, "yyyy-MM-dd", TimeZone.getDefault().getID());
                }
            }
            return (T) typedValue;
        } else if (paramType == Timestamp.class) {
            long longType = Long.parseLong(fieldValue);
            Timestamp typedValue = new Timestamp(longType);
            return (T) typedValue;
        } else if (paramType.isEnum()) {
            Enum[] enumConstants = (Enum[]) paramType.getEnumConstants();
            Validate.notEmpty(enumConstants, "Supported enum constants must have at least one enum value");
            Enum enumType = Enum.valueOf(enumConstants[0].getClass(), fieldValue);
            return (T) enumType;
        } else {
            throw new RuntimeException("Unable to convert field value - " + fieldValue + " to parameter type - " + paramType
                    + ". Parameter type is not supported.");
        }
    }

    public static Date parseDate(String fieldValue, String dateFormat, String timezoneId) {

        Date dateValue = null;

        // try to parse the timestamp format i.e. a long compatible value
        try {
            long longValue = Long.parseLong(fieldValue);
            dateValue = new Date(longValue);
            return dateValue;
        } catch (NumberFormatException nfe) {
            // do not do anything as it might be in date format which we will try to parse below
        }

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timezoneId));
            dateValue = simpleDateFormat.parse(fieldValue);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return dateValue;
    }

    public static <T> Class<T> getFieldType(String fieldName, Class<? extends Object> type) {

        PropertyDescriptor pd = getPropertyDescriptorForField(fieldName, type);
        return (Class<T>) pd.getPropertyType();
    }

    private static PropertyDescriptor getPropertyDescriptorForField(String fieldName, Class<? extends Object> type) {
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(type);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        for (PropertyDescriptor pd : pds) {
            if (pd.getName().equals(fieldName)) {
                return pd;
            }
        }
        throw new RuntimeException("Could not find property descriptor for field name - " + fieldName + " in type - "
                + type + ". Does the field with this name exist, is there a getter for this field also?");
    }

    public static PropertyDescriptor[] getPropertyDescriptorFiltered(Class type, List<String> fieldNames) {
        List<PropertyDescriptor> descriptors = new ArrayList();
        for (PropertyDescriptor descriptor : getPropertyDescriptors(type)) {
            if (fieldNames.contains(descriptor.getName()))
                descriptors.add(descriptor);
        }
        return descriptors.toArray(new PropertyDescriptor[descriptors.size()]);
    }

    public static void setField(String fieldName, Object typedValue, Object target) {
        PropertyDescriptor pd = getPropertyDescriptorForField(fieldName, target.getClass());
        Method writeMethod = pd.getWriteMethod();
        Validate.notNull(writeMethod, "Could not find write method for property - " + pd.getName() + " in class - " + target.getClass());
        try {
            writeMethod.invoke(target, typedValue);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<String> getAllPropertyNames(Class<?> type) {
        BeanInfo beanInfo = null;
        try {
            beanInfo = Introspector.getBeanInfo(type, Object.class);
        } catch (IntrospectionException e) {
            throw new RuntimeException(e);
        }
        PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
        List<String> allPropertyNames = new ArrayList<String>(pds.length);
        for (PropertyDescriptor pd : pds) {
            allPropertyNames.add(pd.getName());
        }
        return allPropertyNames;
    }

    public static void verifyHasPropertiesForFields(Class<?> clazz, List<String> fieldsToVerify) {

        List<String> allPropertyNames = ReflectionUtils.getAllPropertyNames(clazz);

        boolean containsAll = allPropertyNames.containsAll(fieldsToVerify);
        if (!containsAll) {
            List<String> missingFields = new ArrayList<String>();
            for (String field : fieldsToVerify) {
                boolean contains = allPropertyNames.contains(field);
                if (!contains) {
                    missingFields.add(field);
                }
            }
            StringBuilder sb = new StringBuilder("Target parameter class - " + clazz + " does not contain the following properties: [");
            for (int i = 0; i < missingFields.size(); i++) {
                String missingField = missingFields.get(i);
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(missingField);
            }
            sb.append("]");
            throw new RuntimeException(sb.toString());
        }
    }
}
