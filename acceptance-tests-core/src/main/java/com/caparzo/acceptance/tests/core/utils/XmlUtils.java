package com.caparzo.acceptance.tests.core.utils;

import com.caparzo.acceptance.tests.core.jbehave.parameters.XmlPathValueParameter;
import org.apache.commons.lang.Validate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

/**
 * Contains utility methods for working/manipulating XML.
 *
 * @author stasyukd
 * @since 5.0.0-SNAPSHOT
 */
public class XmlUtils {

    private XmlUtils() {
        throw new UnsupportedOperationException();
    }

    public static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void setTextContentForPath(Document xmlDocument, String xPathPath, String content) {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression compiledExp = xPath.compile(xPathPath);
            Node node = (Node) compiledExp.evaluate(xmlDocument, XPathConstants.NODE);
            Validate.notNull(node, "Could not find target node for path - " + xPathPath);
            node.setTextContent(content);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getTextContentForPath(Document xmlDocument, String xPathPath) {
        String textContent = "";
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression compiledExp = xPath.compile(xPathPath);
            Node node = (Node) compiledExp.evaluate(xmlDocument, XPathConstants.NODE);
            Validate.notNull(node, "Could not find target node for path - " + xPathPath);
            textContent = node.getTextContent();
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
        return textContent;
    }

    public static void deleteNode(Document xmlDocument, String xPathPath) {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression compiledExp = xPath.compile(xPathPath);
            Node node = (Node) compiledExp.evaluate(xmlDocument, XPathConstants.NODE);
            Validate.notNull(node, "Could not find target node for path - " + xPathPath);

            Node parentNode = node.getParentNode();
            parentNode.removeChild(node);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    public static Document copyDoc(Document originalXmlDocument) {

        Element originalRoot = originalXmlDocument.getDocumentElement();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document copiedDoc = builder.newDocument();
            Node copiedRoot = copiedDoc.importNode(originalRoot, true);
            copiedDoc.appendChild(copiedRoot);
            return copiedDoc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static Document substitutePaths(Document xmlDoc, List<XmlPathValueParameter> pathValues) {
        // store it to temporary reference, because it can happen some tag will be deleted
        Document tempXmlDoc = XmlUtils.copyDoc(xmlDoc);

        for (XmlPathValueParameter pathValue : pathValues) {
            String path = pathValue.getPath();
            Validate.notEmpty(path);
            String value = pathValue.getValue();
            if (value != null && !value.isEmpty()) {
                if (value.equalsIgnoreCase("not_present")) {
                    XmlUtils.deleteNode(tempXmlDoc, path);
                } else {
                    XmlUtils.setTextContentForPath(tempXmlDoc, path, value);
                }
            } else {
                XmlUtils.setTextContentForPath(tempXmlDoc, path, "");
            }
        }
        return tempXmlDoc;
    }
}
