package com.caparzo.acceptance.tests.core.jbehave;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import com.caparzo.acceptance.tests.core.jbehave.reporters.CustomHTMLReporter;
import org.apache.commons.lang.Validate;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.RelativePathCalculator;
import org.jbehave.core.io.StoryLocation;
import org.jbehave.core.reporters.FilePrintStreamFactory;
import org.jbehave.core.reporters.StoryReporter;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.spring.SpringStepsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.*;

/**
 * JBehave embedder for running story tests.
 * User: stasyukd
 * Date: 06/08/13
 * Time: 12:01
 */
@Component
public class JBehaveEmbedder extends Embedder implements ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ApplicationContext applicationContext;

    private SpringStepsFactory stepsFactory;

    @PostConstruct
    public void init() {

        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            logger.trace(beanDefinitionName);
        }

        stepsFactory = new SpringStepsFactory(configuration(), applicationContext);

        // embedder controls
        super.embedderControls()
                .doIgnoreFailureInStories(false)
                .doIgnoreFailureInView(true)
                .useStoryTimeoutInSecs(Long.MAX_VALUE);

        // causes test to fail upon finding a pending story step
        configuration().usePendingStepStrategy(new FailingUponPendingStep());

        // not sure what this one does 100%, possibly skips running the rest of stories/scenarios after first failure is found
        configuration().storyControls().doSkipScenariosAfterFailure(false);

        configuration().useStepPatternParser(new RegExStoryStepParser());

        // custom parameter converters
        Map<String, ParameterConverters.ParameterConverter> converterMap =
                applicationContext.getBeansOfType(ParameterConverters.ParameterConverter.class);
        Collection<ParameterConverters.ParameterConverter> converters = converterMap.values();
        Validate.notEmpty(converters);
        List<ParameterConverters.ParameterConverter> cList = new ArrayList<ParameterConverters.ParameterConverter>(converters);
        configuration().parameterConverters().addConverters(cList);
        configuration().parameterConverters().addConverters(new ParameterConverters.EnumConverter());
        configuration().parameterConverters().addConverters(new ParameterConverters.EnumListConverter());

        // report configuration
//        configuration().storyReporterBuilder().withFormats(
//                StoryReporterBuilder.Format.CONSOLE,
//                StoryReporterBuilder.Format.HTML,
//                StoryReporterBuilder.Format.STATS);

        configuration().useStoryReporterBuilder(new StoryReporterBuilder() {
                    @Override
                    public StoryReporter reporterFor(String storyPath, org.jbehave.core.reporters.Format format) {
                        if (format.equals(org.jbehave.core.reporters.Format.HTML)) {
//                    FilePrintStreamFactory filePrintStreamFactory = filePrintStreamFactory(storyPath);
                            FilePrintStreamFactory filePrintStreamFactory = new FilePrintStreamFactory(new StoryLocation(codeLocation(), storyPath), fileConfiguration("html"));
                            PrintStream printStream = filePrintStreamFactory.createPrintStream();
                            return new CustomHTMLReporter(printStream);
                        } else {
                            return super.reporterFor(storyPath, format);
                        }
                    }
                }
                        .withFailureTrace(true)
                        .withFormats(org.jbehave.core.reporters.Format.CONSOLE, org.jbehave.core.reporters.Format.HTML, org.jbehave.core.reporters.Format.STATS)
        );

        configuration().usePathCalculator(new RelativePathCalculator());

        // setup meta filter to skip scenarios or stories with @skip
        useMetaFilters(Arrays.asList(new String[]{"-skip"}));
    }

    public void runStories(String storiesMask) {

        List<String> paths = storyPaths(storiesMask);
        runStoriesAsPaths(paths);
    }

    private List<String> storyPaths(String storyFileMask) {

        logger.debug("Looking for stories using mask - " + storyFileMask);

        if (storyFileMask.startsWith("classpath*:")) {
            return Collections.singletonList(storyFileMask);
        }

        List<String> foundStoryPaths = new ArrayList<>();

        PathMatchingResourcePatternResolver pr = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());

        Resource[] resources;
        try {
            resources = pr.getResources(storyFileMask);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (Resource resource : resources) {
            if (resource instanceof ClassPathResource) {
                String path = ((ClassPathResource) resource).getPath();
                foundStoryPaths.add(path);
            } else if (resource instanceof UrlResource) {
                UrlResource urlResource = (UrlResource) resource;
                URL url;
                try {
                    url = urlResource.getURL();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                String urlPath = url.getPath();
                if (urlPath.startsWith("/")) {
                    urlPath = urlPath.substring(1);
                }
                foundStoryPaths.add(urlPath);
            } else {
                throw new AcceptanceTestException("Unsupported resource type - " + resource);
            }

        }

        if (foundStoryPaths.isEmpty()) {
            throw new RuntimeException("Failed to find any story files using mask - " + storyFileMask);
        } else {
            logger.debug("Found total of - " + foundStoryPaths.size() + " story files");
            logger.debug("Found paths:\n" + foundStoryPaths);
        }
        return foundStoryPaths;
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return stepsFactory;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
