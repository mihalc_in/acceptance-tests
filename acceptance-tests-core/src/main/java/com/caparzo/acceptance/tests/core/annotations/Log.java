package com.caparzo.acceptance.tests.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to be used on methods execution of which is to be logged by the logging interceptor.
 * User: stasyukd
 * Date: 01/10/13
 * Time: 17:16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Log {

    Log.Level level() default Log.Level.DEBUG;

    public static enum Level {
        INFO, WARN, ERROR, DEBUG, TRACE
    }
}
