package com.caparzo.acceptance.tests.core.jbehave.reporters;

import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.reporters.HtmlOutput;
import org.jbehave.core.steps.StepCreator;

import java.io.PrintStream;
import java.util.Properties;

/**
 * A customized HTML reporter which "works around" the bug/issue in core JBehave when reporting tabular parameters that are not instances of ExamplesTable but
 * classes annotated with @AsParameter - the issue in core JBehave reporter is that those type of tabular parameters are not displayed correctly in HTML report
 * (they are not formatted as HTML table).
 *
 * @author stasyukd
 * @since 6.0.0-SNAPSHOT
 */
public class CustomHTMLReporter extends HtmlOutput {

    public CustomHTMLReporter(PrintStream output) {
        super(output);
    }

    public CustomHTMLReporter(PrintStream output, Properties outputPatterns) {
        super(output, outputPatterns);
    }

    public CustomHTMLReporter(PrintStream output, Keywords keywords) {
        super(output, keywords);
    }

    public CustomHTMLReporter(PrintStream output, Properties outputPatterns, Keywords keywords) {
        super(output, outputPatterns, keywords);
    }

    public CustomHTMLReporter(PrintStream output, Properties outputPatterns, Keywords keywords, boolean reportFailureTrace) {
        super(output, outputPatterns, keywords, reportFailureTrace);
    }

    public CustomHTMLReporter(PrintStream output, Properties outputPatterns, Keywords keywords, boolean reportFailureTrace, boolean compressFailureTrace) {
        super(output, outputPatterns, keywords, reportFailureTrace, compressFailureTrace);
    }

    @Override
    protected void print(String text) {
        text = markTableParamsCorrectly(text);
        super.print(text);
    }

    /**
     * Works around the issue described in the class doc.
     *
     * @param text
     * @return
     */
    private String markTableParamsCorrectly(String text) {

        final String wrongTableMarker = "&#9252;";

        if (text.contains(wrongTableMarker)) {

            StringBuilder sb = new StringBuilder();
            String[] tokens = text.split(wrongTableMarker);

            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i];
                boolean isTableStartLine = false;
                boolean isTableEndLine = false;
                if (token.startsWith("|")) {
                    // table line
                    if (i == 0 || (i != 0 && !tokens[i - 1].startsWith("|"))) {
                        // if previous line was not a table line then open the table tag
                        String tableStartMarker = StepCreator.PARAMETER_TABLE_START;
                        tableStartMarker = (String) escape(Format.HTML, tableStartMarker)[0];
                        sb.append(tableStartMarker);
                        sb.append(token);
                        isTableStartLine = true;
                    }
                    if (i == tokens.length - 1 || !tokens[i + 1].startsWith("|")) {
                        // if this is the last line or the next line is not a table line
                        String beforePart = token.substring(0, token.lastIndexOf("|") + 1);
                        sb.append(beforePart);
                        String tableEndMarker = StepCreator.PARAMETER_TABLE_END;
                        tableEndMarker = (String) escape(Format.HTML, tableEndMarker)[0];
                        sb.append(tableEndMarker);
                        String afterPart = token.substring(token.lastIndexOf("|") + 1);
                        sb.append(afterPart);
                        isTableEndLine = true;
                    }
                    if (!isTableStartLine && !isTableEndLine) {
                        // table line that is not first or last line in that table
                        sb.append(token);
                    }
                    if (i != 0 && i != tokens.length - 1) {
                        sb.append("\n");
                    }
                } else {
                    if (i != 0) {
                        sb.append("\n");
                    }
                    sb.append(token);
                }
            }

            String output = sb.toString();
            return output;

        } else {
            return text;
        }

    }
}
