package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.util.Random;

/**
 * Generates random Strings of a length from the specified range, from a supplied alphabet.
 * If length or alphabet aren't provided in the constructor, the default valued are used:
 * <ul>
 * <li>length = 10 (fixed)</li>
 * <li>alphabet = \" 0123456789ABCDE\"</li>
 * </ul>
 *
 * @author pastelio
 */
public class RandomStringValueGenerator<T> implements ValueGenerator<T> {

    private Random random = new Random(System.currentTimeMillis() + hashCode());
    private int minLength = 10;
    private int maxLength = 10;
    private String alphabet = "0123456789ABCDE";

    /**
     * Creates the {@code RandomStringValueGenerator} with the default fixed length and alphabet values.
     */
    public RandomStringValueGenerator() {
    }

    /**
     * Creates the {@code RandomStringValueGenerator} with the specified fixed length and default alphabet.
     *
     * @param length length of the generated strings
     */
    public RandomStringValueGenerator(int length) {
        this.minLength = length;
        this.maxLength = length;
    }

    /**
     * Creates the {@code RandomStringValueGenerator} with the default fixed length and specified alphabet.
     *
     * @param alphabet characters to be used for string generation
     */
    public RandomStringValueGenerator(String alphabet) {
        this.alphabet = alphabet;
    }

    /**
     * Creates the {@code RandomStringValueGenerator} with specified length and alphabet.
     *
     * @param length   length of the generated strings
     * @param alphabet characters to be used for string generations
     */
    public RandomStringValueGenerator(int length, String alphabet) {
        this.minLength = length;
        this.maxLength = length;
        this.alphabet = alphabet;
    }

    /**
     * Creates the {@code RandomStringValueGenerator} with the specified length range and default alphabet.
     *
     * @param minLength min length of the string
     * @param maxLength max length of the string
     */
    public RandomStringValueGenerator(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    /**
     * Creates the {@code RandomStringValueGenerator} with the specified length range and alphabet.
     *
     * @param minLength min length of the string
     * @param maxLength max length of the string
     * @param alphabet  characters to be used for string generations
     */
    public RandomStringValueGenerator(int minLength, int maxLength, String alphabet) {
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.alphabet = alphabet;
    }

    /**
     * Generates a random string.
     *
     * @return randomly generated string
     */
    @Override
    public T generateValue() {
        final int alphabetLength = alphabet.length();
        StringBuilder sb = new StringBuilder();
        int currentLength = minLength + random.nextInt(maxLength - minLength + 1);
        for (int i = 0; i < currentLength; i++) {
            sb.append(alphabet.charAt(random.nextInt(alphabetLength)));
        }
        return (T) sb.toString();
    }
}
