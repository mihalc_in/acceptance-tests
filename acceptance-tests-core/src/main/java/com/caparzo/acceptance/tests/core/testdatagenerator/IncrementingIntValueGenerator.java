package com.caparzo.acceptance.tests.core.testdatagenerator;

/**
 * Generates an Integer value incremented each time.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class IncrementingIntValueGenerator<T> implements ValueGenerator<T> {

    private T inc;

    private T initialValue;

    public IncrementingIntValueGenerator(T initialValue) {
        this.initialValue = initialValue;
        inc = initialValue;
    }

    @Override
    public T generateValue() {
        Integer intField = (Integer) inc;
        inc = (T) Integer.valueOf(intField + 1);
        return (T) intField;
    }

}
