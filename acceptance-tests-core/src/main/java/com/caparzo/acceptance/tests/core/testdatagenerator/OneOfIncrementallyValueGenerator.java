package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.util.List;

/**
 * Choose one value from the specified list incrementally, i.e. iterating over all values.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class OneOfIncrementallyValueGenerator<T> implements ValueGenerator<T> {

    private List<T> possibleValues;

    private final int lowIndex = 0;

    private final int highIndex;

    private int currentIndex = lowIndex;

    public OneOfIncrementallyValueGenerator(List<T> possibleValues) {
        this.possibleValues = possibleValues;
        highIndex = possibleValues.size() - 1;
    }

    @Override
    public T generateValue() {
        T value = possibleValues.get(currentIndex);
        currentIndex++;
        if (currentIndex > highIndex) {
            currentIndex = lowIndex;
        }
        return value;
    }
}
