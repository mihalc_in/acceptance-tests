package com.caparzo.acceptance.tests.core.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.Validate;

import java.io.*;
import java.util.*;

/**
 * Used to align columns in story table parameters.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class TableParameterInStoryFilesAligner {

    public static void main(String[] args) throws Exception {

        if (args.length == 0) {
            printUsage();
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("--help") || args[0].equalsIgnoreCase("-?") || args[0].equalsIgnoreCase("/?")) {
                printUsage();
            } else if (args[0].equalsIgnoreCase("-prompt")) {
                String path = promptForFilePath();
                Validate.isTrue(path.length() != 0, "Path to the folder with JBehave stories is empty!");
                File file = new File(path);

                if (file.isDirectory()) {
                    alignStoriesInDir(file);
                } else if (file.isFile()) {
                    alignStory(file);
                }
            } else {
                String path = args[0];
                File file = new File(path);
                if (!file.exists()) {
                    throw new IllegalArgumentException("Specified story path - '" + path + "' doesn't exist");
                }

                if (file.isDirectory()) {
                    alignStoriesInDir(file);
                } else if (file.isFile()) {
                    alignStory(file);
                }
            }
        } else {
            printUsage();
        }
    }

    private static void alignStory(File storyFile) {
        TableParameterInStoryFilesAligner aligner = new TableParameterInStoryFilesAligner();
        aligner.alignTable(storyFile);
    }

    private static void alignStoriesInDir(File storyDir) {
        TableParameterInStoryFilesAligner aligner = new TableParameterInStoryFilesAligner();
        Collection<File> files = FileUtils.listFiles(storyDir, new String[]{"story"}, true);
        Validate.isTrue(files.size() != 0, "There are no JBehave stories in given folder!");

        for (File f : files) {
            System.out.println("Aligning table parameter in story file - " + f.getAbsolutePath());
            aligner.alignTable(f);
        }
    }

    public static void printUsage() {
        System.out.println("Usage:");
        System.out.println("\t--help or -? or /?\t\tprints help");
        System.out.println("\t\"input story file path\" \t\tprovide input folder path to story files");
        System.out.println("\t-prompt\t\tprompts for input raw bc message file path and output path for formatted file");
    }

    public static String promptForFilePath() {

        Scanner in = new Scanner(System.in);
        System.out.println("Please provide input path to folder containing JBehave stories:");
        String path = correctPathFormat(in.nextLine());

        in.close();

        return path;
    }

    private static String correctPathFormat(String path) {
        if (path == null) {
            return null;
        }
        if (path.startsWith("\"") && path.endsWith("\"")) {
            return path.substring(1, path.length() - 1);
        }
        return path;
    }

    public void alignTable(File file) {

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {

            File tempFile = new File(file.getAbsolutePath() + ".temp");
            // delete temporary file if it already exists
            if (tempFile.exists()) {
                boolean deleted = tempFile.delete();
                Validate.isTrue(deleted);
            }
            boolean created = tempFile.createNewFile();
            Validate.isTrue(created);

            br = new BufferedReader(new FileReader(file));
            bw = new BufferedWriter(new FileWriter(tempFile));

            String line;
            List<String> tableLines = new ArrayList<String>();
            while ((line = br.readLine()) != null) {

                if (line.startsWith("|")) {
                    tableLines.add(line);
                } else {

                    if (!tableLines.isEmpty()) {

//                        System.out.println("Aligning table lines:");
//                        for (String tableLine : tableLines) {
//                            System.out.println(tableLine);
//                        }

                        List<String> alignedLines = getAlignedTableLines(tableLines);
                        for (String alignedLine : alignedLines) {
                            bw.write(alignedLine);
                            bw.newLine();
                        }
                        tableLines.clear();
                    }

                    bw.write(line);
                    bw.newLine();
                }

            }

            // call process table again for cases when the last line in the story file was a table
            // line
            if (!tableLines.isEmpty()) {
                List<String> alignedLines = getAlignedTableLines(tableLines);
                for (String alignedLine : alignedLines) {
                    bw.write(alignedLine);
                    bw.newLine();
                }
                tableLines.clear();
            }

            br.close();
            bw.close();

            // only override the original file if we have actually changed some lines
            boolean areEqual = areFileContentsEqual(file, tempFile);
            if (!areEqual) {
                // replace original file
                boolean deleted = file.delete();
                Validate.isTrue(deleted, "Could not delete original file - " + file);
                boolean renamed = tempFile.renameTo(file);
                Validate.isTrue(renamed, "Could not rename file - " + tempFile);
            } else {
                // we just delete the temporary file that we created
                boolean deleted = tempFile.delete();
                Validate.isTrue(deleted, "Could not delete the temporary file - " + tempFile);
            }

        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (bw != null)
                    bw.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

    }

    private boolean areFileContentsEqual(File file1, File file2) {

        Scanner sc1;
        Scanner sc2;
        try {
            sc1 = new Scanner(file1);
            sc1.useDelimiter("\\n");
            sc2 = new Scanner(file2);
            sc2.useDelimiter("\\n");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            while (sc1.hasNext() && sc2.hasNext()) {
                String str1 = sc1.next().trim();
                String str2 = sc2.next().trim();
                if (!str1.equals(str2)) {
                    return false;
                }
            }
            while (sc1.hasNext()) {
                return false;
            }
            while (sc2.hasNext()) {
                return false;
            }
        } finally {
            sc1.close();
            sc2.close();
        }

        return true;
    }

    /**
     * Aligns the table parameter in the specified string. The input string is expected to have only
     * one table, if more than one is present then the behavior is unpredictable.
     *
     * @param inputString
     * @return
     */
    private String alignTableInString(String inputString) {

        String[] lines = inputString.split("\n");
        List<String> alignedTableLines = getAlignedTableLines(Arrays.asList(lines));
        Validate.isTrue(lines.length == alignedTableLines.size(),
                "Aligned number of lines was different to that of non aligned lines");

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < alignedTableLines.size(); i++) {
            String alignedLine = alignedTableLines.get(i);
            sb.append(alignedLine);
            if (i != alignedTableLines.size() - 1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * @param tableLines
     * @return
     */
    private List<String> getAlignedTableLines(List<String> tableLines) {

        List<String> alignedLines = new ArrayList<String>(tableLines.size());

        // work out the max column widths
        int[] maxColumnWidths = null;

        for (String line : tableLines) {

            if (line.startsWith("|--")) {
                // no need to do anything with a comment line
                continue;
            }

            if (!line.startsWith("|")) {
                /*
                 * ignore non table lines to support functionality provided by alignTableInString()
                 * method above
                 */
                continue;
            }

            line = fillEmptyValues(line); // for the split below to work correctly

            String[] split = line.trim().split("\\|");
            split = Arrays.copyOfRange(split, 1, split.length); /*
                                                                 * remove the first element as it is
                                                                 * always empty
                                                                 */
            if (maxColumnWidths == null) {
                maxColumnWidths = new int[split.length];
            }

            for (int i = 0; i < split.length; i++) {
                int fieldLength = split[i].trim().length();
                int maxFieldLength = maxColumnWidths[i];
                if (fieldLength > maxFieldLength) {
                    maxColumnWidths[i] = fieldLength;
                }
            }

        }

        // align lines
        for (String line : tableLines) {

            if (line.startsWith("|--")) {
                // simply add comment line
                alignedLines.add(line);
                continue;
            }

            if (!line.startsWith("|")) {
                /*
                 * simply add non table lines to support functionality provided by
                 * alignTableInString() method above
                 */
                alignedLines.add(line);
                continue;
            }

            StringBuilder alignedLine = new StringBuilder();

            line = fillEmptyValues(line); // for the split below to work correctly

            String[] split = line.trim().split("\\|");
            split = Arrays.copyOfRange(split, 1, split.length); /*
                                                                 * remove the first element as it is
                                                                 * always empty
                                                                 */
            alignedLine.append("|"); // add it back to the aligned line

            for (int i = 0; i < split.length; i++) {
                String field = split[i].trim();
                int maxFieldLength = maxColumnWidths[i];
                int dif = maxFieldLength - field.length();
                if (dif > 0) {
                    char[] padChars = new char[dif];
                    Arrays.fill(padChars, ' ');
                    field = field + new String(padChars);
                }
                alignedLine.append(field);
                alignedLine.append("|");
            }

            alignedLines.add(alignedLine.toString());

        }

        return alignedLines;
    }


    private String fillEmptyValues(String line) {

        int i = line.indexOf("||");

        while (i != -1) {
            line = line.substring(0, i + 1) + " " + line.substring(i + 1);
            i = line.indexOf("||");
        }

        return line;
    }

}
