package com.caparzo.acceptance.tests.core.utils;

/**
 * Constants for common date format patterns.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public interface DateFormats {

    public static final String YYYYMMDD = "yyyyMMdd";

    public static final String YYYY_DASH_MM_DASH_DD = "yyyy-MM-dd";

    public static final String YYYYMMDDHHMMSSZ = "yyyyMMdd HH:mm:ss z";

    public static final String YYYYMMDDHHMMSSMMSSS = "yyyy-MM-dd HH:mm:ss.SSS";

    public static final String DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";

    public static final String KDB_DATE_FORMAT = "yyyy.MM.dd";

    public static final String KDB_TIMESTAMP_FORMAT = "yyyy.MM.dd'T'HH:mm:ss.SSS";

    public static final String PAYLOAD_DATETIME_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static final String XLS_EXPORT_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS";

    public static final String MMYYYYDD = "MMddyyyy";

    public static final String DDMMYYYY = "dd/MM/yyyy";

    public static final String DD_DASH_MMM_DASH_YYYY = "dd-MMM-yyyy";

    public static final String DD_DASH_MMM_DASH_YYYY_SPACE_HHMMSS = "dd-MMM-yyyy HH:mm:ss";

    public static final String YYYY_DASH_MM_DASH_DD_SPACE_HHMM = "yyyy-MM-dd HH:mm";

    public static final String YYMMDD = "yyMMdd";

    public static final String MMDD = "MMdd";

}
