package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import com.caparzo.acceptance.tests.core.jbehave.annotations.DoNotExpandVariables;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Used to intercept method calls to parameter converters in order to expand variables contained in string
 * representations of story step parameters.
 * User: stasyukd
 * Date: 30/08/13
 * Time: 15:29
 */
@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
public class VariableExpanderInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution(public * convertValue(..))")
    public void convertValueMethod() {
    }

    @Pointcut("target(org.jbehave.core.steps.ParameterConverters.ParameterConverter)")
    public void typeParameterConverter() {
    }

    @Around("convertValueMethod() && typeParameterConverter()")
    public Object interceptTransformer(ProceedingJoinPoint pjp) throws Throwable {

        Object[] args = pjp.getArgs();

        Class argType;
        if (args[1] instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) args[1];
            Type[] actualTypeArguments = pt.getActualTypeArguments();
            Validate.isTrue(actualTypeArguments.length == 1);
            argType = (Class) actualTypeArguments[0];
        } else {
            argType = (Class) args[1];
        }

        Annotation doNotExpandAnnotation = argType.getAnnotation(DoNotExpandVariables.class);
        if (doNotExpandAnnotation != null) {
            logger.debug("Not expanding variables in story parameter method type - " + argType.getName());
        } else {
            logger.trace("Expanding arguments: " + StringUtils.join(args, ","));
            expandAnyStringArgs(args);
        }

        // continue with expanded args
        Object returnedValue = pjp.proceed(args);
        return returnedValue;
    }

    private void expandAnyStringArgs(Object[] args) {
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof String) {
                String trimmed = ((String) arg).trim();
                String expandedParameter = expandStringVariables(trimmed);
                args[i] = expandedParameter;
            }
        }
    }

    private String expandStringVariables(String arg) {
        String expanded = StringVariableExpander.expand(arg);
        return expanded;
    }


}
