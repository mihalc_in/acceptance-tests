package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of ValueGenerator that repeats the values configured via the repeat method. The repetition of different
 * values configured via the repeat happens in incremental fashion, i.e. the first value is repeated the specified number of
 * times and then once the maxRepeat is reached for that value the repetition moves onto the next value if there is one and
 * if there isn't the repeatCount is simply reset back to zero.
 * <p/>
 * The repeat method is intended to be used in chained fashion.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class RepeatingValueGenerator<T> implements ValueGenerator<T> {

    private class MaxRepeatAndCurrentCount<T> {
        T value;
        int maxRepeat;
    }

    private List<MaxRepeatAndCurrentCount<T>> config = new ArrayList<MaxRepeatAndCurrentCount<T>>();

    private int currentValueIndex = 0;
    private int currentCount = 0;

    public RepeatingValueGenerator<T> repeat(T value, Integer maxRepeat) {

        MaxRepeatAndCurrentCount configEntry = new MaxRepeatAndCurrentCount();
        configEntry.value = value;
        configEntry.maxRepeat = maxRepeat;
        config.add(configEntry);

        return this;
    }

    @Override
    public T generateValue() {

        MaxRepeatAndCurrentCount<T> currentConfig = config.get(currentValueIndex);
        int maxRepeat = currentConfig.maxRepeat;
        if (currentCount == maxRepeat) {
            // move to next value
            if (currentValueIndex == config.size() - 1) {
                currentValueIndex = 0;
            } else {
                currentValueIndex += 1;
            }
            currentConfig = config.get(currentValueIndex);
            currentCount = 0;
        }

        currentCount += 1;
        T value = currentConfig.value;
        return value;
    }
}
