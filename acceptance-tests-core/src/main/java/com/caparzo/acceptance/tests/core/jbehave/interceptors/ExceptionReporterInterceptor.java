package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * An aop based interceptor that catches any exceptions from the jbehave parameter converter's convertValue method and wraps
 * in a RuntimeException instance in which the exception error message contains the full stack trace of the underlying
 * exception, so that this stack trace is reported/visible in the story tests log and report.
 * <p/>
 * This interceptor should be the last one to execute in the interceptor chain.
 * <p/>
 * User: stasyukd
 * Date: 08/10/13
 * Time: 07:33
 */
@Aspect
@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class ExceptionReporterInterceptor {

    @Pointcut("execution(public * convertValue(..))")
    public void convertValueMethod() {
    }

    @Pointcut("target(org.jbehave.core.steps.ParameterConverters.ParameterConverter)")
    public void typeParameterConverter() {
    }

    @Around("convertValueMethod() && typeParameterConverter()")
    public Object interceptTransformer(ProceedingJoinPoint pjp) throws Throwable {

        try {

            Object returnedValue = pjp.proceed();
            return returnedValue;

        } catch (Exception e) {

            StringBuilder sb = new StringBuilder("Exception occurred while trying to convert jbehave story parameter.\n");
            sb.append("Exception stack trace: ");
            StackTraceElement[] stackTrace = e.getStackTrace();

            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append("\n\t" + stackTraceElement.toString());
            }

            sb.append("Underlying exception is - " + e);

            String errorMsg = sb.toString();
            throw new RuntimeException(errorMsg);
        }

    }
}
