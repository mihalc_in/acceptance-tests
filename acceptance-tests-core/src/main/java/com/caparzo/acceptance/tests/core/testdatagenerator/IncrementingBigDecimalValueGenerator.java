package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.math.BigDecimal;

/**
 * @author pastelio
 */
public class IncrementingBigDecimalValueGenerator implements ValueGenerator<BigDecimal> {

    private long currentValue;
    private long inc;

    public IncrementingBigDecimalValueGenerator(long initialValue) {
        this.currentValue = initialValue;
        inc = 1L;
    }

    public IncrementingBigDecimalValueGenerator(long initialValue, long inc) {
        this.currentValue = initialValue;
        this.inc = inc;
    }

    @Override
    public BigDecimal generateValue() {
        long result = currentValue;
        currentValue += inc;
        return BigDecimal.valueOf(result);
    }
}
