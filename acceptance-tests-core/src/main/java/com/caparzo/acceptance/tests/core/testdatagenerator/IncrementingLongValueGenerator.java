package com.caparzo.acceptance.tests.core.testdatagenerator;

/**
 * Generates a Long value incremented each time.
 * The default incrementation step is one.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class IncrementingLongValueGenerator implements ValueGenerator<Long> {

    private Long inc;

    private Long currentValue;

    /**
     * Constructs IncrementingLongValueGenerator with incrementation step = 1.
     *
     * @param initialValue initial value
     */
    public IncrementingLongValueGenerator(Long initialValue) {
        this.currentValue = initialValue;
        inc = 1L;
    }

    /**
     * Constructs IncrementingLongValueGenerator with provided initial value and incrementation step.
     *
     * @param initialValue initial value
     * @param inc          incrementation step
     */
    public IncrementingLongValueGenerator(Long initialValue, Long inc) {
        this.currentValue = initialValue;
        this.inc = inc;
    }

    @Override
    public Long generateValue() {
        Long result = Long.valueOf(currentValue);
        currentValue = currentValue + inc;
        return result;
    }

}
