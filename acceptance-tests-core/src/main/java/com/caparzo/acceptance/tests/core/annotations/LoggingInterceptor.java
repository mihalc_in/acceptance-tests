package com.caparzo.acceptance.tests.core.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;

/**
 * AOP based interceptor that logs any public bean methods annotated with Log annotation.
 * <p/>
 * TODO - Work in progress.
 * <p/>
 * User: stasyukd
 * Date: 01/10/13
 * Time: 17:20
 */
//@Aspect
//@Component
public class LoggingInterceptor {


    @Pointcut("execution(public * *(..))")
    public void anyPublicMethod() {
    }

    @Around("anyPublicMethod() && @annotation(logAnnotation)")
    public Object annotatedWith(ProceedingJoinPoint pjp, Log logAnnotation) throws Throwable {

//        MethodSignature methodSig = (MethodSignature) pjp.getSignature();

        return null;

//        methodSig.get

//        if (log.isTraceEnabled()) {
//
//            MethodSignature methodSig;
//            long timeBefore = -1;
//            long timeAfter = -1;
//
//            // log before
//            try {
//                log.trace("METHOD TRACE BEGIN");
//                methodSig = (MethodSignature) pjp.getSignature();
//                trace("Executing method - {}", methodSig);
//                Object[] actualArgs = pjp.getArgs();
//                trace("with the following arguments: {}", actualArgs);
//                timeBefore = System.currentTimeMillis();
//            } catch (Exception e) {
//                log.error("Exception occurred while trying to trace method call, execution of the method itself is not impacted", e);
//            }
//
//            // execute target
//            Object returnValue = pjp.proceed();
//
//            // log after
//            try {
//                timeAfter = System.currentTimeMillis();
//                methodSig = (MethodSignature) pjp.getSignature();
//                trace("Completed execution of method - {}", methodSig);
//                trace("Method return value was: {}", returnValue);
//            } catch (Exception e) {
//                log.error("Exception occurred while trying to trace method call, execution of the method itself is not impacted", e);
//            }
//
//            // log time
//            try {
//                if (timeBefore != -1 && timeAfter != -1){
//                    long duration = timeAfter - timeBefore;
//                    // for user friendliness we report in seconds for values more than 1000 millis
//                    StringBuilder reportValue = new StringBuilder();
//                    if (duration > 1000) {
//                        reportValue.append(TimeUnit.MILLISECONDS.toSeconds(duration));
//                        reportValue.append(".");
//                        long remainder = duration % 1000;
//                        reportValue.append(remainder);
//                        reportValue.append(" seconds");
//                    } else {
//                        reportValue.append(duration);
//                        reportValue.append(" milliseconds");
//                    }
//                    trace("Method execution took - " + reportValue.toString());
//                } else {
//                    log.error("Could not record method execution time, timeBefore - {}, timeAfter - {}", timeBefore, timeAfter);
//                }
//                log.trace("METHOD TRACE END");
//            } catch(Exception e) {
//                log.error("Exception occurred while trying to trace method call, execution of the method itself is not impacted", e);
//            }
//
//            return returnValue;
//
//        } else {
//            return pjp.proceed();
//        }
    }
}
