package com.caparzo.acceptance.tests.core.testdatagenerator;

import java.util.UUID;

/**
 * Generates random UUID strings.
 * Uses the {@code java.util.UUID} class.
 *
 * @author pastelio
 */
public class RandomUuidStringValueGenerator<T> implements ValueGenerator<T> {

    /**
     * Generates random UUID string.
     *
     * @return generated UUID string.
     */
    @Override
    public T generateValue() {
        return (T) UUID.randomUUID().toString();
    }
}
