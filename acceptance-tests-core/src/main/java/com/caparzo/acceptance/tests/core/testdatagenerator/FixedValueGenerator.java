package com.caparzo.acceptance.tests.core.testdatagenerator;

/**
 * Always generates the same value that is specified via constructor.
 *
 * @author stasyukd
 * @since 2.0.0-SNAPSHOT
 */
public class FixedValueGenerator<T> implements ValueGenerator<T> {

    private final T fixedValue;

    public FixedValueGenerator(T fixedValue) {
        this.fixedValue = fixedValue;
    }

    @Override
    public T generateValue() {
        return fixedValue;
    }

}
