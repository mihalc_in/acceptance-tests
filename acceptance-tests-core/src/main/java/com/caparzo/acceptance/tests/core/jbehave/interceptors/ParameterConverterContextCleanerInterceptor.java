package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import com.caparzo.acceptance.tests.core.jbehave.ConverterContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Used to reset thread local context used by the parameter converters. This should be the first to be executed
 * in the interceptor chain.
 * User: stasyukd
 * Date: 30/08/13
 * Time: 15:29
 */
@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ParameterConverterContextCleanerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(public * convertValue(..))")
    public void convertValueMethod() {
    }

    @Pointcut("target(org.jbehave.core.steps.ParameterConverters.ParameterConverter)")
    public void typeParameterConverter() {
    }

    @Around("convertValueMethod() && typeParameterConverter()")
    public Object interceptTransformer(ProceedingJoinPoint pjp) throws Throwable {

        logger.trace("Resetting converter context...");

        // reset context and proceed
        ConverterContext.threadLocalContext.set(new ConverterContext());

        // continue with expanded args
        return pjp.proceed();
    }

}
