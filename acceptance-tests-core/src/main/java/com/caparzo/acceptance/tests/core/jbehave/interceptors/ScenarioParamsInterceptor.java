package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import com.caparzo.acceptance.tests.core.jbehave.ScenarioExecutionContext;
import com.caparzo.acceptance.tests.core.jbehave.ScenarioParamsContext;
import com.caparzo.acceptance.tests.core.jbehave.annotations.DoNotExpandVariables;
import com.caparzo.acceptance.tests.core.utils.ReflectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used to intercept method calls to parameter converters in order to use variables previously defined in step
 * User: mihalcip
 * Date: 16/06/14
 * Time: 15:35
 */
@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 12)
public class ScenarioParamsInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution(public * convertValue(..))")
    public void convertValueMethod() {
    }

    @Pointcut("target(org.jbehave.core.steps.ParameterConverters.ParameterConverter)")
    public void typeParameterConverter() {
    }

    @Around("convertValueMethod() && typeParameterConverter()")
    public Object interceptTransformer(ProceedingJoinPoint pjp) throws Throwable {

        Object[] args = pjp.getArgs();

        Class argType;
        if (args[1] instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) args[1];
            Type[] actualTypeArguments = pt.getActualTypeArguments();
            Validate.isTrue(actualTypeArguments.length == 1);
            argType = (Class) actualTypeArguments[0];
        } else {
            argType = (Class) args[1];
        }

        Annotation doNotExpandAnnotation = argType.getAnnotation(DoNotExpandVariables.class);
        if (doNotExpandAnnotation != null) {
            logger.debug("Not expanding variables in story parameter method type - " + argType.getName());
        } else {
            logger.trace("Expanding scenario parameters: " + StringUtils.join(args, ","));
            expandScenarioParams(args);
        }

        // continue with expanded args
        Object returnedValue = pjp.proceed(args);
        return returnedValue;
    }

    private void expandScenarioParams(Object[] args) {
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof String) {
                String trimmed = ((String) arg).trim();
                String expandedParameter = expandScenarioParam(trimmed);
                args[i] = expandedParameter;
            }
        }
    }

    private static final Pattern VAR_PATTERN = Pattern.compile("(.*)(\\$\\{)(.*)(\\})(.*)", Pattern.DOTALL);

    private String expandScenarioParam(String arg) {
        String expandedParameter = arg;

        Matcher matcher = VAR_PATTERN.matcher(arg);

        // match every occurence of pattern, replace it with scenario param value, put it together and return
        while (matcher.matches()) {
            String g1 = matcher.group(1); // before part
            String g2 = matcher.group(2); // <
            String variable = matcher.group(3); // variable
            String g4 = matcher.group(4); // >
            String g5 = matcher.group(5); // after part

            String expandedVariable = "";
            Map<String, String> scenarioParams = ScenarioParamsContext.CONTEXT.get().getScenarioParams();
            if (scenarioParams.containsKey(variable)) {
                expandedVariable = scenarioParams.get(variable);
            } else {

                // same syntax is used for scenario context objects, so see if the reference is to a context object rather then parameter
                // variable might be a complex type i.e. the reference can be to a specific field to that object
                String[] tokens = variable.split("\\.");
                String key = tokens[0];

                Map<String, Object> contextVars = ScenarioExecutionContext.getContextVars();
                Object contextObject = contextVars.get(key);
                if (contextObject != null) {
                    if (tokens.length > 1) {
                        String fieldName = tokens[1];
                        expandedVariable = String.valueOf(ReflectionUtils.getFieldValue(contextObject, fieldName));
                    } else {
                        expandedVariable = contextObject.toString();
                    }
                } else {
                    throw new RuntimeException("Scenario parameter '" + variable + "' has not been set in previous steps!");
                }
            }

            expandedParameter = g1 + expandedVariable + g5;
            matcher.reset(expandedParameter);
        }
        return expandedParameter;
    }
}
