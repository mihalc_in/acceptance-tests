package com.caparzo.acceptance.tests.core.jbehave.converters;

import com.caparzo.acceptance.tests.core.exceptions.AcceptanceTestException;
import com.caparzo.acceptance.tests.core.jbehave.ConverterContext;
import com.caparzo.acceptance.tests.core.utils.ReflectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Converts tabular format string representations of objects into instances of those objects.
 * Note this implementation supports individual objects as well as lists of those objects as supported parameters
 * (see overridden implementation of accept method below for details).
 * User: stasyukd
 * Date: 29/08/13
 * Time: 13:35
 */
class TabularStringToObjectParameterConverter extends BaseFilObjectsParameterConverter {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object convertValue(String value, Type type) {

        logger.trace("Converting value: " + value + " to type: " + type);

        boolean listType = ReflectionUtils.isListType(type);
        if (listType) {
            type = ReflectionUtils.getListType(type);
        }

        value = value.trim();
        // must cast to Class here
        List<Object> objectValues = convertToObjectParameters(value, (Class) type);

        if (listType) {
            return objectValues;
        } else {
            return objectValues.iterator().next();
        }
    }

    List<Object> convertToObjectParameters(String value, Class clazz) {

        List<Object> objectValues = new ArrayList<Object>();
        objectValues = Collections.checkedList(objectValues, clazz);

        logger.trace("Splitting tabular parameter by lines, parameter value = '" + value + "'");
        String[] lines = getLines(value);
        int numberOfLinesBefore = lines.length;
        logger.trace("parameter was split into " + lines.length + "  lines, lines: '" + Arrays.toString(lines) + "'");

        // first lineWithPipeSeparators is the field names
        String headerLine = lines[0];
        String[] fieldNames = getFieldNames(headerLine);

        // records start from lineWithPipeSeparators two
        for (int i = 1; i < lines.length; i++) {
            String recordLine = lines[i];
            String[] fieldValues = getTrimmedTokens(recordLine);
            verifyEqualNumFieldNamesAndFieldValues(fieldNames, fieldValues);
            Object o = ReflectionUtils.createNewObject(clazz);
            ReflectionUtils.setFieldValues(o, fieldNames, fieldValues);
            objectValues.add(o);
        }

        // temporary returning only one value for now instead of the whole list
        Validate.notEmpty(objectValues, "Failed to create any object parameters from tabular parameter. Does tabular parameter contain any rows?");
        // number of objects should be the same as number of lines -1 for the header line
        int expectedNumberOfObjects = numberOfLinesBefore - 1;
        Validate.isTrue(objectValues.size() == expectedNumberOfObjects, "Failed to split tabular parameter into expected " +
                "number of objects, expected - " + expectedNumberOfObjects + " but was - " + objectValues.size());

        // update put field names in context
        List<String> fieldNamesList = Arrays.asList(fieldNames);
        logger.trace(clazz + " - setting field names " + fieldNamesList);
        ConverterContext.getContext().setTabularParameterFieldNames(fieldNamesList);

        return objectValues;
    }

    private void verifyEqualNumFieldNamesAndFieldValues(String[] fieldNames, String[] fieldValues) {
        if (fieldNames.length != fieldValues.length) {
            StringBuilder errMsg = new StringBuilder("Number of field names (" + fieldNames.length
                    + ") doesn't match the number of field values (" + fieldValues.length + ").");
            errMsg.append("\nField names: ");
            int index = 0;
            for (String fieldName : fieldNames) {
                if (index != 0) {
                    errMsg.append(", ");
                }
                errMsg.append(fieldName);
                index++;
            }
            errMsg.append(".");
            errMsg.append("\nField values: ");
            index = 0;
            for (String fieldValue : fieldValues) {
                if (index != 0) {
                    errMsg.append(", ");
                }
                errMsg.append(fieldValue);
                index++;
            }
            errMsg.append(".");
            throw new AcceptanceTestException(errMsg.toString());
        }
    }

    /**
     * extracted to separate method as is used by other classes
     *
     * @param headerLine
     * @return
     */
    String[] getFieldNames(String headerLine) {
        String[] trimmedTokens = getTrimmedTokens(headerLine);
        String[] normalizedFieldNames = normalizeFieldNames(trimmedTokens);
        return normalizedFieldNames;
    }

    /**
     * extracted to separate method as is used by other classes
     *
     * @param value
     * @return
     */
    String[] getLines(String value) {
        String[] split = value.split("(\\n|(\\r\\n))");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].trim();
        }

        return split;
    }

    /**
     * Will convert user friendly field names like "Agent Bank Name" into java property names like "agentBankName".
     *
     * @param userFriendlyFieldNames
     * @return
     */
    private String[] normalizeFieldNames(String[] userFriendlyFieldNames) {

        String[] normalizedFieldNames = new String[userFriendlyFieldNames.length];

        for (int i = 0; i < userFriendlyFieldNames.length; i++) {
            String userFriendlyFieldName = userFriendlyFieldNames[i];
            String normalizedFieldName = normalizeFieldName(userFriendlyFieldName);
            normalizedFieldNames[i] = normalizedFieldName;
        }

        return normalizedFieldNames;
    }

    private String normalizeFieldName(String userFriendlyFieldName) {

        String trimmed = userFriendlyFieldName.trim();
        String[] splitBySpace = trimmed.split("\\s");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splitBySpace.length; i++) {
            String part = splitBySpace[i];
            // we capitalize every word apart from the first
            if (i != 0) {
                part = StringUtils.capitalize(part);
            } else {
                part = StringUtils.uncapitalize(part);
            }
            sb.append(part);
        }
        String normalizedFieldName = sb.toString();
        return normalizedFieldName;
    }

    private static String[] getTrimmedTokens(String lineWithPipeSeparators) {
        String[] tokens = lineWithPipeSeparators.split("\\|");
        String[] trimmedTokens = new String[tokens.length - 1];
        // we start copying from index 1 as the first token is the first occurrence of '|' character that gets matched
        // by the split method
        for (int i = 1; i < tokens.length; i++) {
            String value = tokens[i].trim();
            trimmedTokens[i - 1] = value;
        }
        return trimmedTokens;
    }

    @Override
    public boolean accept(Type type) {
        boolean isList = ReflectionUtils.isListType(type);
        if (isList) {
            type = ReflectionUtils.getListType(type);
        }
        return super.accept(type);
    }

}
