package com.caparzo.acceptance.tests.core.jbehave;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.Validate;
import org.jbehave.core.io.StoryLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLClassLoader;

/**
 * Decorator type implementation of StoryLoader that is able to load stories from the classpath if the story path starts
 * with "classpath*:".
 *
 * @author stasyukd
 * @since 7.0.0-SNAPSHOT
 */
public class ClasspathEnabledStoryLoader implements StoryLoader {

    private StoryLoader decoratedLoader;

    public ClasspathEnabledStoryLoader(StoryLoader decoratedLoader) {
        this.decoratedLoader = decoratedLoader;
    }

    @Override
    public String loadStoryAsText(String storyPath) {

        if (storyPath.startsWith("classpath*:")) {
            return loadStoryFromClasspath(storyPath);
        } else {
            return this.decoratedLoader.loadStoryAsText(storyPath);
        }
    }

    private String loadStoryFromClasspath(String storyPath) {

        URLClassLoader classLoader = (URLClassLoader) getClass().getClassLoader();
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        Resource[] resources;
        try {
            resources = resolver.getResources(storyPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Validate.notEmpty(resources);
        Resource resource = resources[0];
        Validate.notNull(resource);

        StringWriter writer = new StringWriter();
        try {
            InputStream inputStream = resource.getInputStream();
            IOUtils.copy(inputStream, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String storyAsString = writer.toString();
        return storyAsString;
    }

}
