package com.caparzo.acceptance.tests.core.utils;

import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Contains utility methods for obtaining string representations of objects in string format with tables similar to the
 * ones in story files. Intended to be used for formatting logging output.
 * User: stasyukd
 * Date: 01/10/13
 * Time: 15:48
 */
public class TableFormatUtils {

    private TableFormatUtils() {
        throw new UnsupportedOperationException("Not intended for instantiation");
    }

    // objects
    public static <T> String asTableString(Collection<T> objects, String title) {
        return formatToString(objects, title, null);
    }

    public static <T> String asTableString(T object, String title) {
        Class<T> type = (Class<T>) object.getClass();
        List<String> allFields = ReflectionUtils.getAllPropertyNames(type);

        return asTableString(object, title, allFields);
    }

    public static <T> String asTableString(T object, String title, List<String> fieldsToInclude) {
        List<T> objects = new ArrayList<>();
        objects.add(object);
        return formatToString(objects, title, fieldsToInclude);
    }

    // collections of objects
    public static <T> String asTableString(Collection<T> objects, String title, List<String> fieldsToInclude) {
        return formatToString(objects, title, fieldsToInclude);
    }

    private static <T> String formatToString(Collection<T> objects, String title, Collection<String> fieldsToInclude) {
        StringBuilder tableString = new StringBuilder();

        tableString.append("=================================================================================================\n");
        tableString.append(title + "\n");
        tableString.append("=================================================================================================\n");

        String asTable = formatToString(objects, fieldsToInclude);
        tableString.append(asTable);
        return tableString.toString();
    }

    private static <T> String formatToString(Collection<T> objects, Collection<String> fieldsToInclude) {

        StringBuilder tableString = new StringBuilder();

        String headerRow = null;
        for (T o : objects) {
            Pair<String, String> tabularRep = ConverterUtils.convertToTabularFormatString(o, fieldsToInclude);
            if (headerRow == null) {
                headerRow = tabularRep.getValue0();
                tableString.append(headerRow + "\n");
            }
            String valueRow = tabularRep.getValue1();
            tableString.append(valueRow + "\n");
        }

        String result = tableString.toString();
        String alignedTableString = TableParameterAligner.alignTableInString(result);
        return alignedTableString;
    }
}
