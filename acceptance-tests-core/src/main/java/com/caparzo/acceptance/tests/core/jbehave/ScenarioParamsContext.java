package com.caparzo.acceptance.tests.core.jbehave;

import java.util.HashMap;
import java.util.Map;

/**
 * Thread local context used by the parameter converters.
 * User: mihalcip
 * Date: 16/06/14
 * Time: 15:29
 */
public class ScenarioParamsContext extends ThreadLocal<ScenarioParamsContext> {

    public static ThreadLocal<ScenarioParamsContext> CONTEXT = new ThreadLocal<ScenarioParamsContext>();

    private Map<String, String> scenarioParams = new HashMap<>();

    public static String getScenarioParam(String paramName) {
        ScenarioParamsContext scenarioParamsContext = CONTEXT.get();
        Map<String, String> params = scenarioParamsContext.getScenarioParams();
        return params.get(paramName);
    }

    public static void addScenarioParam(String name, String value) {
        ScenarioParamsContext scenarioParamsContext = CONTEXT.get();
        scenarioParamsContext.getScenarioParams().put(name, value);
    }

    public Map<String, String> getScenarioParams() {
        return scenarioParams;
    }
}
