package com.caparzo.acceptance.tests.core.testdatagenerator;

import com.caparzo.acceptance.tests.common.date.DateUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Generates random dates in a specified date range.
 * The generated dates are always truncated to the {@code DAY_OF_YEAR}, meaning that hours, minutes and all subsequent values are set to zero.
 * If no dates are specified in the constructor parameters, the date range is from yesterday to tomorrow.
 *
 * @author pastelio
 */
public class RandomDateValueGenerator<T> implements ValueGenerator<T> {

    private final long earliestTimestamp;
    private final long latestTimestamp;
    private final RandomDataGenerator randomGenerator = new RandomDataGenerator();

    /**
     * Creates a new {@code RandomDateValueGenerator} with the date range from yesterday to tomorrow.
     */
    public RandomDateValueGenerator() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        earliestTimestamp = DateUtils.truncateTime(calendar).getTimeInMillis();
        calendar.add(Calendar.DAY_OF_YEAR, 3);
        calendar.add(Calendar.MILLISECOND, -1);
        latestTimestamp = calendar.getTimeInMillis();
    }

    /**
     * Creates a new {@code RandomDateValueGenerator} with the specified date range.
     * Hours, minutes and subsequent values of the provided dates are not used.
     *
     * @param earliestDate lower bound of the range
     * @param latestDate   upper bound of the range
     */
    public RandomDateValueGenerator(Date earliestDate, Date latestDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(earliestDate);
        earliestTimestamp = DateUtils.truncateTime(calendar).getTimeInMillis();
        calendar.setTime(latestDate);
        DateUtils.truncateTime(calendar);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        latestTimestamp = calendar.getTimeInMillis();
    }

    public RandomDateValueGenerator(String earliestDate, String latestDate) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        earliestTimestamp = dateFormat.parse(earliestDate).getTime();
        latestTimestamp = dateFormat.parse(latestDate).getTime();
    }

    /**
     * Generates a random {@code Date} with hours, minutes and subsequent values equal to zero.
     *
     * @return
     */
    @Override
    public T generateValue() {
        return (T) DateUtils.truncateTime(new Date(randomGenerator.nextLong(earliestTimestamp, latestTimestamp)));
    }
}
