package com.caparzo.acceptance.tests.core.utils;

import java.util.concurrent.TimeUnit;

/**
 * Utility pojo for passing around time digits and time units.
 *
 * @author stasyukd
 * @since 7.0.0-SNAPSHOT
 */
public class Timeout {

    public final long timeout;

    public final TimeUnit unit;

    public Timeout(long timeout, TimeUnit unit) {
        this.timeout = timeout;
        this.unit = unit;
    }

//    public static Timeout create(long timeout, TimeUnit unit) {
//        return new Timeout(timeout, unit);
//    }

    @Override
    public String toString() {
        return "Timeout{" +
                "unit=" + unit +
                ", timeout=" + timeout +
                '}';
    }
}
