package com.caparzo.acceptance.tests.core.jbehave.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.lang.reflect.Type;

/**
 * Converts jason string representations of objects into instances of those objects.
 * User: stasyukd
 * Date: 29/08/13
 * Time: 13:35
 */
class JsonStringToObjectParameterConverter extends BaseFilObjectsParameterConverter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Object convertValue(String value, Type type) {

        logger.trace("Converting value: " + value + " to type: " + type);

        JsonToObjectTransformer transformer = new JsonToObjectTransformer((Class) type);

        Message<String> inputMsg = new GenericMessage<>(value);
        Message<?> transformedMsg = transformer.transform(inputMsg);
        Object payload = transformedMsg.getPayload();
        return payload;
    }

}
