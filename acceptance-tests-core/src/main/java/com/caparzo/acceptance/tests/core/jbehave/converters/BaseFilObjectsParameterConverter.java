package com.caparzo.acceptance.tests.core.jbehave.converters;

import org.jbehave.core.steps.ParameterConverters;

import java.lang.reflect.Type;

/**
 * Contains base functionality for FIL objects parameter converters.
 * User: stasyukd
 * Date: 05/09/13
 * Time: 11:03
 */
abstract class BaseFilObjectsParameterConverter implements ParameterConverters.ParameterConverter {

    @Override
    public boolean accept(Type type) {
        // we support any type which is in package or subpackage of 'com.caparzo.acceptance.tests'
        if (type instanceof Class<?>) {
            String className = ((Class<?>) type).getName();
            if (className.startsWith("com.caparzo.acceptance.tests")) {
                return true;
            } else {
                return false;
            }
        } else {
            // base implementation only supports singular parameter classes
            return false;
        }
    }

}
