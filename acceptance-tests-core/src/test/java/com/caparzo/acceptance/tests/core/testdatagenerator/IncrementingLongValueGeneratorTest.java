package com.caparzo.acceptance.tests.core.testdatagenerator;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author pastelio
 */
public class IncrementingLongValueGeneratorTest {

    @Test
    public void defaultIncrementingTest() {
        IncrementingLongValueGenerator generator = new IncrementingLongValueGenerator(1000L);

        long result1 = generator.generateValue();
        long result2 = generator.generateValue();
        long result3 = generator.generateValue();

        Assert.assertEquals("The first generated number wasn't as expected.", 1000L, result1);
        Assert.assertEquals("The second generated number wasn't as expected.", 1001L, result2);
        Assert.assertEquals("The third generated number wasn't as expected.", 1002L, result3);
    }

    @Test
    public void userSetIncrementingTest() {
        IncrementingLongValueGenerator generator = new IncrementingLongValueGenerator(1000L, 1000L);

        long result1 = generator.generateValue();
        long result2 = generator.generateValue();
        long result3 = generator.generateValue();

        Assert.assertEquals("The first generated number wasn't as expected.", 1000L, result1);
        Assert.assertEquals("The second generated number wasn't as expected.", 2000, result2);
        Assert.assertEquals("The third generated number wasn't as expected.", 3000L, result3);
    }
}
