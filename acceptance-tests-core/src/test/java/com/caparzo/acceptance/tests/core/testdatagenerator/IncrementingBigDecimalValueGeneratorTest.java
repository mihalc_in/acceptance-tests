package com.caparzo.acceptance.tests.core.testdatagenerator;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author pastelio
 */
public class IncrementingBigDecimalValueGeneratorTest {

    @Test
    public void defaultIncrementingTest() {
        IncrementingBigDecimalValueGenerator generator = new IncrementingBigDecimalValueGenerator(1000L);

        BigDecimal result1 = generator.generateValue();
        BigDecimal result2 = generator.generateValue();
        BigDecimal result3 = generator.generateValue();

        Assert.assertEquals("The first generated number wasn't as expected.", BigDecimal.valueOf(1000L), result1);
        Assert.assertEquals("The second generated number wasn't as expected.", BigDecimal.valueOf(1001L), result2);
        Assert.assertEquals("The third generated number wasn't as expected.", BigDecimal.valueOf(1002L), result3);
    }

    @Test
    public void userSetIncrementingTest() {
        IncrementingBigDecimalValueGenerator generator = new IncrementingBigDecimalValueGenerator(1000L, 1000L);

        BigDecimal result1 = generator.generateValue();
        BigDecimal result2 = generator.generateValue();
        BigDecimal result3 = generator.generateValue();

        Assert.assertEquals("The first generated number wasn't as expected.", BigDecimal.valueOf(1000L), result1);
        Assert.assertEquals("The second generated number wasn't as expected.", BigDecimal.valueOf(2000L), result2);
        Assert.assertEquals("The third generated number wasn't as expected.", BigDecimal.valueOf(3000L), result3);
    }
}
