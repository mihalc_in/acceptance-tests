package com.caparzo.acceptance.tests.core.testdatagenerator;

import com.caparzo.acceptance.tests.common.date.DateFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * @author pastelio
 */
public class IncrementingDateValueGeneratorTest {

    DateFormat dateFormat = DateFormatter.YYYYMMDD.get();

    @Test
    public void defaultIncrementingTest() throws ParseException {
        IncrementingDateValueGenerator generator = new IncrementingDateValueGenerator(dateFormat.parse("20131231"));

        Date date1 = generator.generateValue();
        Date date2 = generator.generateValue();
        Date date3 = generator.generateValue();

        Assert.assertEquals("The first generated date wasn't correct.", dateFormat.parse("20131231"), date1);
        Assert.assertEquals("The second generated date wasn't correct.", dateFormat.parse("20140101"), date2);
        Assert.assertEquals("The third generated date wasn't correct.", dateFormat.parse("20140102"), date3);
    }

    @Test
    public void incrementAfterTwoTest() throws ParseException {
        IncrementingDateValueGenerator generator = new IncrementingDateValueGenerator(dateFormat.parse("20131231"), 2);

        Date date1 = generator.generateValue();
        Date date2 = generator.generateValue();
        Date date3 = generator.generateValue();

        Assert.assertEquals("The first generated date wasn't correct.", dateFormat.parse("20131231"), date1);
        Assert.assertEquals("The second generated date wasn't correct.", dateFormat.parse("20131231"), date2);
        Assert.assertEquals("The third generated date wasn't correct.", dateFormat.parse("20140101"), date3);
    }

    @Test
    public void onlyBusinessDaysTest() throws ParseException {
        IncrementingDateValueGenerator generator = new IncrementingDateValueGenerator(dateFormat.parse("20140411"), 1, true);

        Date date1 = generator.generateValue();
        Date date2 = generator.generateValue();
        Date date3 = generator.generateValue();

        Assert.assertEquals("The first generated date wasn't correct.", dateFormat.parse("20140411"), date1);
        Assert.assertEquals("The second generated date wasn't correct.", dateFormat.parse("20140414"), date2);
        Assert.assertEquals("The third generated date wasn't correct.", dateFormat.parse("20140415"), date3);
    }
}
