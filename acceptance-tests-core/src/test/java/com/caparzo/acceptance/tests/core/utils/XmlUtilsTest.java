package com.caparzo.acceptance.tests.core.utils;

import com.caparzo.acceptance.tests.core.jbehave.parameters.XmlPathValueParameter;
import junit.framework.Assert;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yadrajee
 */
public class XmlUtilsTest {

    private String xmlString = "<msg><uuid>12345</uuid></msg>";
    private Document expectedDoc = null;

    @Before
    public void createDoc() throws Exception {
        expectedDoc = XMLUnit.buildDocument(XMLUnit.newControlParser(), new FileReader("src/test/resources/xmlForJunitTest.xml"));
        XMLUnit.setIgnoreWhitespace(true);
    }

    @Test
    public void convertStringToDocumentTest() {
        Document actualDoc = XmlUtils.convertStringToDocument(xmlString);
        Diff myDiff = new Diff(expectedDoc, actualDoc);
        assert (myDiff.similar());
    }

    @Test
    public void convertDocumentToStringTest() throws Exception {
        String expectedXMLString = xmlString;
        String actualXMLString = XmlUtils.convertDocumentToString(expectedDoc);

        Diff myDiff = new Diff(expectedXMLString, actualXMLString);
        assert (myDiff.similar());
    }

    @Test
    public void textContentForPathTest() {
        String xmlStr = "<msg><uuid>12345</uuid></msg>";
        Document xmlDoc = XmlUtils.convertStringToDocument(xmlStr);
        String path = "msg/uuid";
        String actualVal = "54321";
        XmlUtils.setTextContentForPath(xmlDoc, path, actualVal);
        String expectedVal = XmlUtils.getTextContentForPath(xmlDoc, path);
        Assert.assertEquals(expectedVal, actualVal);
    }

    @Test
    public void deleteNodeTest() throws Exception {
        String xmlStr = "<msg><uuid>12345</uuid><name>Tom</name></msg>";
        Document xmlDoc = XmlUtils.convertStringToDocument(xmlStr);
        String path = "msg/name";
        XmlUtils.deleteNode(xmlDoc, path);

        String actualXml = XmlUtils.convertDocumentToString(xmlDoc);
        String expectedXml = "<msg><uuid>12345</uuid></msg>";
        Diff myDiff = new Diff(expectedXml, actualXml);
        assert (myDiff.similar());
    }

    @Test
    public void copyDocTest() {
        String xmlStr = "<msg><uuid>12345</uuid></msg>";
        Document expectedXmlDoc = XmlUtils.convertStringToDocument(xmlStr);
        Document actualDoc = XmlUtils.copyDoc(expectedXmlDoc);
        Diff myDiff = new Diff(expectedXmlDoc, actualDoc);
        assert (myDiff.similar());
    }

    @Test
    public void substitutePathsTest() {
        String expectedXml = "<msg><uuid>12345</uuid></msg>";
        String inputXml = "<msg><uuid>12345</uuid><name>not_present</name></msg>";
        Document inputDoc = XmlUtils.convertStringToDocument(inputXml);
        List<XmlPathValueParameter> pathValList = new ArrayList<XmlPathValueParameter>();

        XmlPathValueParameter objXmlPathValueParameter1 = new XmlPathValueParameter();
        objXmlPathValueParameter1.setPath("msg/uuid");
        objXmlPathValueParameter1.setValue("12345");
        pathValList.add(objXmlPathValueParameter1);

        XmlPathValueParameter objXmlPathValueParameter2 = new XmlPathValueParameter();
        objXmlPathValueParameter2.setPath("msg/name");
        objXmlPathValueParameter2.setValue("not_present");
        pathValList.add(objXmlPathValueParameter2);

        Document actualDoc = XmlUtils.substitutePaths(inputDoc, pathValList);
        Diff myDiff = new Diff(XmlUtils.convertStringToDocument(expectedXml), actualDoc);
        assert (myDiff.similar());
    }

}
