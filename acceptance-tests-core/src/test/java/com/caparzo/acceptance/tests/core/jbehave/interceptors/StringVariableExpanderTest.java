package com.caparzo.acceptance.tests.core.jbehave.interceptors;

import com.caparzo.acceptance.tests.common.date.DateUtils;
import com.caparzo.acceptance.tests.core.utils.DateFormats;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by yadrajee
 */
public class StringVariableExpanderTest {

    @Test
    public void today_date_YYYYMMDD() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.YYYYMMDD);
        String variable = "";

        variable = sdf.format(c.getTime());
        Assert.assertEquals("today_date_YYYYMMDD", variable, StringVariableExpander.expand("<today_date_YYYYMMDD>"));

        variable = sdf.format(DateUtils.getDayFromToday(-1));
        Assert.assertEquals("today_date_YYYYMMDD-1d", variable, StringVariableExpander.expand("<today_date_YYYYMMDD-1d>"));

        variable = sdf.format(DateUtils.getDayFromToday(1));
        Assert.assertEquals("today_date_YYYYMMDD+1d", variable, StringVariableExpander.expand("<today_date_YYYYMMDD+1d>"));
    }

    @Test
    public void swift_long_today_date() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.YYMMDD);
        String variable = "";

        variable = sdf.format(c.getTime());
        Assert.assertEquals("swift_long_today_date", variable, StringVariableExpander.expand("<swift_long_today_date>"));

        variable = sdf.format(DateUtils.getDayFromToday(-1));
        Assert.assertEquals("swift_long_today_date-1d", variable, StringVariableExpander.expand("<swift_long_today_date-1d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-2));
        Assert.assertEquals("swift_long_today_date-2d", variable, StringVariableExpander.expand("<swift_long_today_date-2d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-3));
        Assert.assertEquals("swift_long_today_date-3d", variable, StringVariableExpander.expand("<swift_long_today_date-3d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-4));
        Assert.assertEquals("swift_long_today_date-4d", variable, StringVariableExpander.expand("<swift_long_today_date-4d>"));

        variable = sdf.format(DateUtils.getDayFromToday(1));
        Assert.assertEquals("swift_long_today_date+1d", variable, StringVariableExpander.expand("<swift_long_today_date+1d>"));

        variable = sdf.format(DateUtils.getDayFromToday(2));
        Assert.assertEquals("swift_long_today_date+2d", variable, StringVariableExpander.expand("<swift_long_today_date+2d>"));

        variable = sdf.format(DateUtils.getDayFromToday(3));
        Assert.assertEquals("swift_long_today_date+3d", variable, StringVariableExpander.expand("<swift_long_today_date+3d>"));

        variable = sdf.format(DateUtils.getDayFromToday(4));
        Assert.assertEquals("swift_long_today_date+4d", variable, StringVariableExpander.expand("<swift_long_today_date+4d>"));
    }

    @Test
    public void swift_short_today_date() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.MMDD);
        String variable = "";

        variable = sdf.format(c.getTime());
        Assert.assertEquals("swift_short_today_date", variable, StringVariableExpander.expand("<swift_short_today_date>"));

        variable = sdf.format(DateUtils.getDayFromToday(-1));
        Assert.assertEquals("swift_short_today_date-1d", variable, StringVariableExpander.expand("<swift_short_today_date-1d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-2));
        Assert.assertEquals("swift_short_today_date-2d", variable, StringVariableExpander.expand("<swift_short_today_date-2d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-3));
        Assert.assertEquals("swift_short_today_date-3d", variable, StringVariableExpander.expand("<swift_short_today_date-3d>"));

        variable = sdf.format(DateUtils.getDayFromToday(-4));
        Assert.assertEquals("swift_short_today_date-4d", variable, StringVariableExpander.expand("<swift_short_today_date-4d>"));

        variable = sdf.format(DateUtils.getDayFromToday(1));
        Assert.assertEquals("swift_short_today_date+1d", variable, StringVariableExpander.expand("<swift_short_today_date+1d>"));

        variable = sdf.format(DateUtils.getDayFromToday(2));
        Assert.assertEquals("swift_short_today_date+2d", variable, StringVariableExpander.expand("<swift_short_today_date+2d>"));

        variable = sdf.format(DateUtils.getDayFromToday(3));
        Assert.assertEquals("swift_short_today_date+3d", variable, StringVariableExpander.expand("<swift_short_today_date+3d>"));

        variable = sdf.format(DateUtils.getDayFromToday(4));
        Assert.assertEquals("swift_short_today_date+4d", variable, StringVariableExpander.expand("<swift_short_today_date+4d>"));
    }

    @Test
    public void current_date() {
        Date pastWeekDay = null;
        Calendar c = null;
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.YYYY_DASH_MM_DASH_DD);

        c = Calendar.getInstance();
        Assert.assertEquals("current_date", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        Assert.assertEquals("current_date+1d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+1d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 2);
        Assert.assertEquals("current_date+2d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+2d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 3);
        Assert.assertEquals("current_date+3d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+3d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 4);
        Assert.assertEquals("current_date+4d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+4d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 6);
        Assert.assertEquals("current_date+6d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+6d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 7);
        Assert.assertEquals("current_date+7d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+7d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 28);
        Assert.assertEquals("current_date+28d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+28d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, 29);
        Assert.assertEquals("current_date+29d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date+29d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        Assert.assertEquals("current_date-1d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-1d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -2);
        Assert.assertEquals("current_date-2d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-2d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -3);
        Assert.assertEquals("current_date-3d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-3d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -4);
        Assert.assertEquals("current_date-4d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-4d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -5);
        Assert.assertEquals("current_date-5d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-5d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -6);
        Assert.assertEquals("current_date-6d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-6d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -7);
        Assert.assertEquals("current_date-7d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-7d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -29);
        Assert.assertEquals("current_date-29d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-29d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -28);
        Assert.assertEquals("current_date-28d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-28d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -25);
        Assert.assertEquals("current_date-25d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-25d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -23);
        Assert.assertEquals("current_date-23d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-23d>"));

        c = Calendar.getInstance();
        c.add(Calendar.DATE, -30);
        Assert.assertEquals("current_date-30d", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<current_date-30d>"));

        c = Calendar.getInstance();
        pastWeekDay = DateUtils.getPastWeekDay(c.getTime(), 1);
        Assert.assertEquals("current_date-1bd", String.valueOf(sdf.format(pastWeekDay)), StringVariableExpander.expand("<current_date-1bd>"));
    }

    @Test
    public void today_timestamp() throws Exception {

        Long today_timestamp;

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+1bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+2bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+3bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+4bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+5bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

        today_timestamp = Long.parseLong(StringVariableExpander.expand("<today_timestamp+6bd>"));
        assert (today_timestamp >= (today_timestamp - 1000) && today_timestamp <= (today_timestamp + 1000));

    }


    @Test
    public void today_date() {
        Calendar c = Calendar.getInstance();
        Date pastWeekDay = null;
        Date futureWeekDay = null;
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.YYYY_DASH_MM_DASH_DD);

        Assert.assertEquals("today_date", String.valueOf(sdf.format(c.getTime())), StringVariableExpander.expand("<today_date>"));
        Assert.assertEquals("today_date+1d", String.valueOf(sdf.format(DateUtils.getDayFromToday(1))), StringVariableExpander.expand("<today_date+1d>"));
        Assert.assertEquals("today_date-1d", String.valueOf(sdf.format(DateUtils.getDayFromToday(-1))), StringVariableExpander.expand("<today_date-1d>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 1);
        Assert.assertEquals("<today_date+1bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+1bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 2);
        Assert.assertEquals("<today_date+2bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+2bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 3);
        Assert.assertEquals("<today_date+3bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+3bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 4);
        Assert.assertEquals("<today_date+4bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+4bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 5);
        Assert.assertEquals("<today_date+5bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+5bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 6);
        Assert.assertEquals("<today_date+6bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+6bd>"));

        futureWeekDay = DateUtils.getFutureWeekDay(c.getTime(), 7);
        Assert.assertEquals("<today_date+7bd>", String.valueOf(sdf.format(futureWeekDay)), StringVariableExpander.expand("<today_date+7bd>"));

        pastWeekDay = DateUtils.getPastWeekDay(c.getTime(), 1);
        Assert.assertEquals("today_date-1bd", String.valueOf(sdf.format(pastWeekDay)), StringVariableExpander.expand("<today_date-1bd>"));

        pastWeekDay = DateUtils.getPastWeekDay(c.getTime(), 30);
        Assert.assertEquals("today_date-30bd", String.valueOf(sdf.format(pastWeekDay)), StringVariableExpander.expand("<today_date-30bd>"));
    }

    @Test
    public void today_date_DDMMYYYY() {
        Calendar c = Calendar.getInstance();
        String variable = new SimpleDateFormat(DateFormats.DDMMYYYY).format(c.getTime());
        Assert.assertEquals("today_date_DDMMYYYY", variable, StringVariableExpander.expand("<today_date_DDMMYYYY>"));
    }

    @Test
    public void current_date_KDB() {
        Calendar c = Calendar.getInstance();
        String variable = new SimpleDateFormat(DateFormats.KDB_DATE_FORMAT).format(c.getTime());
        Assert.assertEquals("current_date_KDB", variable, StringVariableExpander.expand("<current_date_KDB>"));
    }

    @Test
    public void today_timestamp_string() throws Exception {

        Date currentDate = null;
        String time = null;
        Calendar beforeTime = null;
        Calendar afterTime = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.KDB_TIMESTAMP_FORMAT);


        time = StringVariableExpander.expand("<today_timestamp_string>");
        c.setTime(sdf.parse(time));
        beforeTime = Calendar.getInstance();
        beforeTime.setTime(c.getTime());
        beforeTime.add(Calendar.MILLISECOND, -1000);
        afterTime = Calendar.getInstance();
        afterTime.setTime(c.getTime());
        afterTime.add(Calendar.MILLISECOND, 1000);
        currentDate = c.getTime();
        assert (currentDate.after(beforeTime.getTime()) && currentDate.before(afterTime.getTime()));

    }

    @Test
    @Ignore
    public void currentTimeMillisToday() {
        Date date = new Date();
        String variable = "" + (date.getTime() - DateUtils.truncateTime(date).getTime());
        Assert.assertEquals("currentTimeMillisToday", variable, StringVariableExpander.expand("<currentTimeMillisToday>"));
    }

    @Test
    public void today_timestamp_kdb() throws Exception {

        Date currentDate = null;
        String time = null;
        Calendar beforeTime = null;
        Calendar afterTime = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormats.PAYLOAD_DATETIME_Z);

        time = StringVariableExpander.expand("<today_timestamp_kdb>");
        c.setTime(sdf.parse(time));
        beforeTime = Calendar.getInstance();
        beforeTime.setTime(c.getTime());
        beforeTime.add(Calendar.MILLISECOND, -1000);
        afterTime = Calendar.getInstance();
        afterTime.setTime(c.getTime());
        afterTime.add(Calendar.MILLISECOND, 1000);
        currentDate = c.getTime();
        assert (currentDate.after(beforeTime.getTime()) && currentDate.before(afterTime.getTime()));

        time = StringVariableExpander.expand("<today_timestamp_kdb-1d>");
        c.setTime(sdf.parse(time));
        beforeTime = Calendar.getInstance();
        beforeTime.setTime(c.getTime());
        beforeTime.add(Calendar.MILLISECOND, -1000);
        afterTime = Calendar.getInstance();
        afterTime.setTime(c.getTime());
        afterTime.add(Calendar.MILLISECOND, 1000);
        currentDate = c.getTime();
        assert (currentDate.after(beforeTime.getTime()) && currentDate.before(afterTime.getTime()));
    }

    @Test
    @Ignore
    public void currentTimeMillis() {

        Assert.assertEquals("currentTimeMillis", "" + System.currentTimeMillis(), StringVariableExpander.expand("<currentTimeMillis>"));
        Assert.assertEquals("currentTimeMillis+1s", "" + (System.currentTimeMillis() + 1000), StringVariableExpander.expand("<currentTimeMillis+1s>"));
        Assert.assertEquals("currentTimeMillis+2s", "" + (System.currentTimeMillis() + 2000), StringVariableExpander.expand("<currentTimeMillis+2s>"));
        Assert.assertEquals("currentTimeMillis+3s", "" + (System.currentTimeMillis() + 3000), StringVariableExpander.expand("<currentTimeMillis+3s>"));
        Assert.assertEquals("currentTimeMillis+4s", "" + (System.currentTimeMillis() + 4000), StringVariableExpander.expand("<currentTimeMillis+4s>"));
        Assert.assertEquals("currentTimeMillis+5s", "" + (System.currentTimeMillis() + 5000), StringVariableExpander.expand("<currentTimeMillis+5s>"));
        Assert.assertEquals("currentTimeMillis+6s", "" + (System.currentTimeMillis() + 6000), StringVariableExpander.expand("<currentTimeMillis+6s>"));
        Assert.assertEquals("currentTimeMillis+7s", "" + (System.currentTimeMillis() + 7000), StringVariableExpander.expand("<currentTimeMillis+7s>"));
        Assert.assertEquals("currentTimeMillis+8s", "" + (System.currentTimeMillis() + 8000), StringVariableExpander.expand("<currentTimeMillis+8s>"));
        Assert.assertEquals("currentTimeMillis+9s", "" + (System.currentTimeMillis() + 9000), StringVariableExpander.expand("<currentTimeMillis+9s>"));
        Assert.assertEquals("currentTimeMillis+10s", "" + (System.currentTimeMillis() + 10000), StringVariableExpander.expand("<currentTimeMillis+10s>"));
        Assert.assertEquals("currentTimeMillis+11s", "" + (System.currentTimeMillis() + 11000), StringVariableExpander.expand("<currentTimeMillis+11s>"));
        Assert.assertEquals("currentTimeMillis+12s", "" + (System.currentTimeMillis() + 12000), StringVariableExpander.expand("<currentTimeMillis+12s>"));
        Assert.assertEquals("currentTimeMillis+13s", "" + (System.currentTimeMillis() + 13000), StringVariableExpander.expand("<currentTimeMillis+13s>"));
        Assert.assertEquals("currentTimeMillis+14s", "" + (System.currentTimeMillis() + 14000), StringVariableExpander.expand("<currentTimeMillis+14s>"));
        Assert.assertEquals("currentTimeMillis+15s", "" + (System.currentTimeMillis() + 15000), StringVariableExpander.expand("<currentTimeMillis+15s>"));
        Assert.assertEquals("currentTimeMillis+16s", "" + (System.currentTimeMillis() + 16000), StringVariableExpander.expand("<currentTimeMillis+16s>"));
        Assert.assertEquals("currentTimeMillis+17s", "" + (System.currentTimeMillis() + 17000), StringVariableExpander.expand("<currentTimeMillis+17s>"));
        Assert.assertEquals("currentTimeMillis+18s", "" + (System.currentTimeMillis() + 18000), StringVariableExpander.expand("<currentTimeMillis+18s>"));
        Assert.assertEquals("currentTimeMillis+19s", "" + (System.currentTimeMillis() + 19000), StringVariableExpander.expand("<currentTimeMillis+19s>"));
        Assert.assertEquals("currentTimeMillis+20s", "" + (System.currentTimeMillis() + 20000), StringVariableExpander.expand("<currentTimeMillis+20s>"));
        Assert.assertEquals("currentTimeMillis+21s", "" + (System.currentTimeMillis() + 21000), StringVariableExpander.expand("<currentTimeMillis+21s>"));
        Assert.assertEquals("currentTimeMillis+22s", "" + (System.currentTimeMillis() + 22000), StringVariableExpander.expand("<currentTimeMillis+22s>"));
    }

    @Test
    @Ignore
    public void random_digits() {
//        Assert.assertEquals("random_digits(9)", expand("<random_digits(9)>"), StringVariableExpander.expand("<random_digits(9)>"));
    }

    @Test
    @Ignore
    public void random_string() {
//        Assert.assertEquals("random_string()", expand("<random_string(11)>"), StringVariableExpander.expand("<random_string(11)>"));
    }


}
