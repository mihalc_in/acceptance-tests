package com.caparzo.acceptance.tests.common.date;

/**
 * Date formatter util class.
 *
 * @author savarapa
 */
public final class DateFormatter {

    private DateFormatter() {
        //hide constructor for utility class
    }

    /**
     * Format date into yyyytMMdd format eg. 20110830
     */
    public static final ThreadLocalDateFormat YYYYMMDD = new ThreadLocalDateFormat("yyyyMMdd");
    public static final ThreadLocalDateFormat YYMMDD = new ThreadLocalDateFormat("yyMMdd");
    public static final ThreadLocalDateFormat YYYY_DASH_MM_DASH_DD = new ThreadLocalDateFormat("yyyy-MM-dd");  //2015-06-20
    public static final ThreadLocalDateFormat YYYYMMDDHHMMSSZ = new ThreadLocalDateFormat("yyyyMMdd HH:mm:ss z");
    public static final ThreadLocalDateFormat YYYYMMDDHHMMSSMMSSS = new ThreadLocalDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static final ThreadLocalDateFormat DDMMYYYYHHMMSS = new ThreadLocalDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final ThreadLocalDateFormat KDB_DATE_FORMAT = new ThreadLocalDateFormat("yyyy.MM.dd");
    public static final ThreadLocalDateFormat KDB_TIMESTAMP_FORMAT = new ThreadLocalDateFormat("yyyy.MM.dd'T'HH:mm:ss.SSS");
    public static final ThreadLocalDateFormat PAYLOAD_DATETIME_Z = new ThreadLocalDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    public static final ThreadLocalDateFormat UI_PAYLOAD = new ThreadLocalDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final ThreadLocalDateFormat XLS_EXPORT_FORMAT = new ThreadLocalDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
    /**
     * Format date into MMddyyyy format eg. 08302011
     */
    public static final ThreadLocalDateFormat MMYYYYDD = new ThreadLocalDateFormat("MMddyyyy");
    public static final ThreadLocalDateFormat DDMMYYYY = new ThreadLocalDateFormat("dd/MM/yyyy");
    public static final ThreadLocalDateFormat DD_DASH_MMM_DASH_YYYY = new ThreadLocalDateFormat("dd-MMM-yyyy");
    public static final ThreadLocalDateFormat DD_DASH_MMM_DASH_YYYY_SPACE_HHMMSS = new ThreadLocalDateFormat("dd-MMM-yyyy HH:mm:ss");
    public static final ThreadLocalDateFormat YYYY_DASH_MM_DASH_DD_SPACE_HHMM = new ThreadLocalDateFormat("yyyy-MM-dd HH:mm");
}
