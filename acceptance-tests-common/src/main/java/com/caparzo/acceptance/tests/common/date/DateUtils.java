package com.caparzo.acceptance.tests.common.date;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Handy Date utils
 *
 * @author savarapa
 */
public final class DateUtils {

    private DateUtils() {
    }

    public static boolean isWeekend(Date d) {
        return isWeekend(getCalendar(d));
    }

    public static boolean isWeekDay(Date d) {
        return !isWeekend(d);
    }

    public static Date rollToNextWeekday(Date d) {
        return rollToNextWeekday(getCalendar(d)).getTime();
    }

    public static Date rollToPrevWeekday(Date d) {
        return rollToPrevWeekday(getCalendar(d)).getTime();
    }

    public static Date truncateTime(Date d) {
        return truncateTime(getCalendar(d)).getTime();
    }

    public static List<Date> getNextWeekDays(Date d, int numberOfWeekDays) {
        Calendar c = getCalendar(d);
        truncateTime(c);
        List<Date> dates = new ArrayList<Date>(numberOfWeekDays);
        dates.add(c.getTime());
        for (int i = 1; i < numberOfWeekDays; i++) {
            rollToNextWeekday(c);
            dates.add(c.getTime());
        }
        return dates;
    }

    public static Date getFutureWeekDay(Date d, int numberOfWeekDays) {
        Calendar c = getCalendar(d);
        truncateTime(c);
        for (int i = 0; i < numberOfWeekDays; i++) {
            rollToNextWeekday(c);
        }
        return c.getTime();

    }

    /**
     * Returns date which is n weekdays in the past from the provided date.
     * Only weekends are considered.
     *
     * @param d                counting starts from this date
     * @param numberOfWeekDays number of weekdays into the past
     * @return date n weekdays in the past
     */
    public static Date getPastWeekDay(Date d, int numberOfWeekDays) {
        Calendar c = getCalendar(d);
        truncateTime(c);
        for (int i = 0; i > -numberOfWeekDays; i--) {
            rollToPrevWeekday(c);
        }
        return c.getTime();

    }


    public static boolean isWeekend(Calendar c) {
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;
    }

    public static Calendar addDays(Calendar c, int days) {
        if (days > 0) {
            for (int i = 0; i < days; i++) {
                rollToNextWeekday(c);
            }
        } else if (days < 0) {
            for (int i = 0; i < -1 * days; i++) {
                rollToPrevWeekday(c);
            }
        }
        return c;
    }

    public static Date addDays(Date d, int days) {
        if (days > 0) {
            for (int i = 0; i < days; i++) {
                d = rollToNextWeekday(d);
            }
        } else if (days < 0) {
            for (int i = 0; i < -1 * days; i++) {
                d = rollToPrevWeekday(d);
            }
        }
        return d;
    }

    public static Date addHours(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }

    private static Calendar rollToNextWeekday(Calendar c) {
        c.add(Calendar.DATE, 1);
        while (isWeekend(c)) {
            c.add(Calendar.DATE, 1);
        }
        return c;
    }

    private static Calendar rollToPrevWeekday(Calendar c) {
        c.add(Calendar.DATE, -1);
        while (isWeekend(c)) {
            c.add(Calendar.DATE, -1);
        }
        return c;
    }

    private static Calendar getCalendar(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return c;
    }

    public static Date getToday() {
        return getDayFromToday(0);
    }

    public static Date getTomorrow() {
        return getDayFromToday(1);
    }

    public static Date getDayFromToday(int offset) {
        Calendar cal = getCalendar(new Date());
        if (offset != 0) {
            cal.add(Calendar.DAY_OF_YEAR, offset);
        }
        return truncateTime(cal.getTime());
    }

    public static Calendar truncateTime(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    /**
     * Returns an integer representation of timezone hour offset.
     *
     * @param timezone a timezone, e.g. "GMT -05:00"
     * @return timezone hour offset, e.g. -5
     */
    public static int getTimezoneDifference(String timezone) {
        char sign = timezone.charAt(4);
        int hour = Integer.valueOf(timezone.charAt(5) + "" + timezone.charAt(6));
        if (sign == '-') {
            return -1 * hour;

        } else if (sign == '+') {
            return hour;

        } else {
            throw new IllegalStateException("Wrong timezone: " + timezone);
        }
    }


    public static Date addMinutes(Date d, int minutes) {
        Calendar cal = getCalendar(d);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static long getTimeDifferenceInMinutes(Date date1, Date date2) {
        return TimeUnit.MILLISECONDS.toMinutes(date1.getTime() - date2.getTime());
    }

}

