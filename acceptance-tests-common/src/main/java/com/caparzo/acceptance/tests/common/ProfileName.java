package com.caparzo.acceptance.tests.common;

/**
 * Constants holder for different spring profile names used in fil projects.
 *
 * @author stasyukd
 * @since 3.0.0-SNAPSHOT
 */
public interface ProfileName {

    public static final String STRING_ACTIVE_PROFILES_KEY = "spring.profiles.active";

    /**
     * Profile for running tests against dev environment (localhost).
     */
    public static final String dev = "dev";

    /**
     * Profile for running tests which do factory reset for internal data
     */
    public static final String env = "env";


}
