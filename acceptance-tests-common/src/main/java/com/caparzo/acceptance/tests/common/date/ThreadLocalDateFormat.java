package com.caparzo.acceptance.tests.common.date;

import java.text.SimpleDateFormat;

/**
 * Thread Local date format class.
 *
 * @author savarapa
 */
public class ThreadLocalDateFormat extends ThreadLocal<SimpleDateFormat> {

    private final String format;

    /**
     * Thread Local date format.
     *
     * @param dateFormat date format for Simple Date Formatter
     */
    public ThreadLocalDateFormat(String dateFormat) {
        super();
        format = dateFormat;
    }

    @Override
    protected SimpleDateFormat initialValue() {
        return new SimpleDateFormat(format);
    }

    public String getFormat() {
        return format;
    }
}