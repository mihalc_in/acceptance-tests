package com.caparzo.acceptance.tests.common.utils;

import com.caparzo.acceptance.tests.common.date.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author pastelio
 */
public class DateUtilsTest {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    @Test
    public void getNextWeekDaysTest() throws ParseException {
        Date date = dateFormat.parse("20131226");
        List<Date> dates = DateUtils.getNextWeekDays(date, 6);
        Assert.assertEquals("The size of the returned list is incorrect.", 6, dates.size());
        Assert.assertEquals("First date in the returned list is incorrect.", dateFormat.parse("20131226"), dates.get(0));
        Assert.assertEquals("Second date in the returned list is incorrect.", dateFormat.parse("20131227"), dates.get(1));
        Assert.assertEquals("Third date in the returned list is incorrect.", dateFormat.parse("20131230"), dates.get(2));
        Assert.assertEquals("Fourth date in the returned list is incorrect.", dateFormat.parse("20131231"), dates.get(3));
        Assert.assertEquals("Fifth date in the returned list is incorrect.", dateFormat.parse("20140101"), dates.get(4));
        Assert.assertEquals("Last date in the returned list is incorrect.", dateFormat.parse("20140102"), dates.get(5));
    }

    @Test
    public void isWeekendTest() throws ParseException {
        Date date = dateFormat.parse("20140329");
        Assert.assertTrue("The date is weekend.", DateUtils.isWeekend(date));

        date = dateFormat.parse("20140328");
        Assert.assertEquals("The date is not weekend.", false, DateUtils.isWeekend(date));
    }

    @Test
    public void isWeekDayTest() throws ParseException {
        Date date = dateFormat.parse("20140329");
        Assert.assertEquals("The date is weekend.", false, DateUtils.isWeekDay(date));

        date = dateFormat.parse("20140328");
        Assert.assertEquals("The date is weekday.", true, DateUtils.isWeekDay(date));
    }

    @Test
    public void rollToNextWeekdayTest() throws ParseException {
        Date date = dateFormat.parse("20131226");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131227"), DateUtils.rollToNextWeekday(date));

        date = dateFormat.parse("20131231");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20140101"), DateUtils.rollToNextWeekday(date));

        date = dateFormat.parse("20131227");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131230"), DateUtils.rollToNextWeekday(date));

        date = dateFormat.parse("20131228");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131230"), DateUtils.rollToNextWeekday(date));
    }

    @Test
    public void rollToPrevWeekdayTest() throws ParseException {
        Date date = dateFormat.parse("20131226");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131225"), DateUtils.rollToPrevWeekday(date));

        date = dateFormat.parse("20140101");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131231"), DateUtils.rollToPrevWeekday(date));

        date = dateFormat.parse("20131230");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131227"), DateUtils.rollToPrevWeekday(date));

        date = dateFormat.parse("20131229");
        Assert.assertEquals("The returned date is incorrect", dateFormat.parse("20131227"), DateUtils.rollToPrevWeekday(date));
    }

    @Test
    public void truncateTimeTest() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = df.parse("2014-01-01 08:08:08");

        Assert.assertEquals("The returned date is not truncated.", dateFormat.parse("20140101"), DateUtils.truncateTime(date));
    }

    @Test
    public void getFutureWeekDayTest() throws ParseException {
        Date date = dateFormat.parse("20131226");
        Assert.assertEquals("The returned weekday is incorrect.", dateFormat.parse("20140103"), DateUtils.getFutureWeekDay(date, 6));
    }

    @Test
    public void getPastWeekDayTest() throws ParseException {
        Date date = dateFormat.parse("20140101");
        Assert.assertEquals("The returned weekday is incorrect.", dateFormat.parse("20131227"), DateUtils.getPastWeekDay(date, 3));
    }

    @Test
    public void isWeekendCalendarInputTest() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse("20140329"));

        Assert.assertEquals("The provided Calendar has the weekend date set.", true, DateUtils.isWeekend(calendar));

        calendar.setTime(dateFormat.parse("20140328"));

        Assert.assertEquals("The provided Calendar has the weekday date set.", false, DateUtils.isWeekend(calendar));
    }

    @Test
    public void calendarAddDaysTest() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse("20131226"));
        Calendar expected = Calendar.getInstance();
        expected.setTime(dateFormat.parse("20140102"));

        Assert.assertEquals("The calendar was rolled forward incorrectly.", expected, DateUtils.addDays(calendar, 5));

        calendar.setTime(dateFormat.parse("20140102"));
        expected.setTime(dateFormat.parse("20131226"));

        Assert.assertEquals("The calendar was rolled backwards incorrectly.", expected, DateUtils.addDays(calendar, -5));
    }

    @Test
    public void dateAddDaysTest() throws ParseException {
        Date date = dateFormat.parse("20131226");
        Date expected = dateFormat.parse("20140102");

        Assert.assertEquals("The date was rolled forward incorrectly.", expected, DateUtils.addDays(date, 5));

        date = dateFormat.parse("20140102");
        expected = dateFormat.parse("20131226");

        Assert.assertEquals("The date was rolled backwards incorrectly.", expected, DateUtils.addDays(date, -5));
    }

    @Test
    public void getTodayTest() {
        Assert.assertEquals("The date returned is not today.", DateUtils.truncateTime(new Date()), DateUtils.getToday());
    }

    @Test
    public void getTomorrowTest() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        Assert.assertEquals("The date returned is not tomorrow.", DateUtils.truncateTime(calendar.getTime()), DateUtils.getTomorrow());
    }

    @Test
    public void getDayFromTodayTest() {
        Calendar calendar = Calendar.getInstance();
        Assert.assertEquals("The date returned is not today.", DateUtils.truncateTime(calendar.getTime()), DateUtils.getDayFromToday(0));

        calendar.add(Calendar.DAY_OF_YEAR, 3);
        Assert.assertEquals("The date returned is not 3 days from today.", DateUtils.truncateTime(calendar.getTime()), DateUtils.getDayFromToday(3));

        calendar.add(Calendar.DAY_OF_YEAR, -6);
        Assert.assertEquals("The date returned is not 3 days from today.", DateUtils.truncateTime(calendar.getTime()), DateUtils.getDayFromToday(-3));
    }

    @Test
    public void calendarTruncateTimeTest() {
        Calendar expected = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expected.getTime());

        expected.set(Calendar.HOUR_OF_DAY, 0);
        expected.set(Calendar.MINUTE, 0);
        expected.set(Calendar.SECOND, 0);
        expected.set(Calendar.MILLISECOND, 0);

        Assert.assertEquals("The time was not truncated correctly.", expected, DateUtils.truncateTime(calendar));
    }

    @Test
    public void getTimezoneDifferenceTest() {
        Assert.assertEquals("The timezone difference is incorrect.", -5, DateUtils.getTimezoneDifference("GMT -05:00"));
        Assert.assertEquals("The timezone difference is incorrect.", 5, DateUtils.getTimezoneDifference("GMT +05:00"));
    }

    @Test
    public void calendarAddHoursTest() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse("20131226"));
        Date date = DateUtils.addHours(calendar.getTime(), 2);
        calendar.setTime(date);
        Assert.assertEquals(2013, calendar.get(Calendar.YEAR));
        Assert.assertEquals(12, calendar.get(Calendar.MONTH) + 1);
        Assert.assertEquals(26, calendar.get(Calendar.DATE));
        Assert.assertEquals(2, calendar.get(Calendar.HOUR_OF_DAY));

    }

    @Test
    public void calendarAddMinutesTest() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateFormat.parse("20131226"));
        Date date = DateUtils.addMinutes(calendar.getTime(), 25);
        calendar.setTime(date);
        Assert.assertEquals(2013, calendar.get(Calendar.YEAR));
        Assert.assertEquals(12, calendar.get(Calendar.MONTH) + 1);
        Assert.assertEquals(26, calendar.get(Calendar.DATE));
        Assert.assertEquals(0, calendar.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals(25, calendar.get(Calendar.MINUTE));


    }
}
